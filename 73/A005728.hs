{-
A005728
Number of fractions in Farey series of order n.
1, 2, 3, 5, 7, ...
a(15) = 73
-}

import Data.List
import Data.Ratio
import Numeric

main = do
  putStrLn "<svg version='1.1' baseProfile='full' xmlns='http://www.w3.org/2000/svg' width='7000' height='5000'>"
  putStrLn "<rect width='100%' height='100%' stroke='none' fill='white' />"
  putStrLn "<g stroke-width='0.1' stroke='black' fill='red' transform='translate(400,640)'>"
  putStrLn "<g transform='scale(120)'>"
  putStrLn "<path d='M 0.5 15 L 50.5 15' stroke-width='0.1' stroke='black' />"
  putStr $ unlines
    [ "<g transform='translate(" ++ showFFloat Nothing (fromRational (50 * r) :: Double) "" ++ ",0)'>\
      \<path d='M 0.5 " ++ show (15 + q) ++ " L 0.5 " ++ show (15 - p) ++ "' />\
      \</g>"
    | r <- nub $ sort [ p % q | q <- [1..15], p <- [0..q] ]
    , let p = fromIntegral (numerator r)
    , let q = fromIntegral (denominator r)
    ]
  putStrLn "<g fill='black' stroke='none'>"
  putStr $ unlines
    [ "<g transform='translate(" ++ showFFloat Nothing (fromRational (50 * r) :: Double) "" ++ ",0)'>\
      \<circle cy='" ++ show (15 + q) ++ "' cx='0.5' r='0.4' />\
      \<circle cy='" ++ show (15 - p) ++ "' cx='0.5' r='0.4' />\
      \</g>"
    | r <- nub $ sort [ p % q | q <- [1..15], p <- [0..q] ]
    , let p = fromIntegral (numerator r)
    , let q = fromIntegral (denominator r)
    ]
  putStrLn "</g><g fill='red' stroke='none'>"
  putStr $ unlines
    [ "<g transform='translate(" ++ showFFloat Nothing (fromRational (50 * r) :: Double) "" ++ ",0)'>\
      \<circle cy='" ++ show (15 + q) ++ "' cx='0.5' r='0.3' />\
      \<circle cy='" ++ show (15 - p) ++ "' cx='0.5' r='0.3' />\
      \</g>"
    | r <- nub $ sort [ p % q | q <- [1..15], p <- [0..q] ]
    , let p = fromIntegral (numerator r)
    , let q = fromIntegral (denominator r)
    ]
  putStrLn "</g>"
  putStrLn "</g>"
  putStrLn "</g>"
  putStrLn "</svg>"
