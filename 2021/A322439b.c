/*
A322439
Number of ordered pairs of integer partitions of n where no part of the first is greater than any part of the second.
1, 1, 3, 5, 11, 15, 33, 42, 82, 114, 195, 258, 466, ...
a(16) = 2021
*/

#include <math.h>
#include <stdio.h>

#define P 16
#define N 32
#define M 2021

int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  fprintf(stdout, "<svg version='1.1' baseProfile='full' xmlns='http://www.w3.org/2000/svg' width='6664' height='5176'>\n");
  fprintf(stdout, "<rect width='100%%' height='100%%' stroke='none' fill='#fff' />\n");
  fprintf(stdout, "<g transform='translate(747.0,532.4813172579466) scale(110)' stroke-width='0.02' stroke='#000'>\n");
  for (int j = 0; j < 43; ++j)
  {
    for (int i = 0; i < 47; ++i)
    {
      // read input
      int nparts1 = 0;
      fscanf(stdin, "%d", &nparts1);
      int parts1[P];
      for (int p = 0; p < nparts1; ++p)
      {
        fscanf(stdin, "%d", &parts1[p]);
      }
      int nparts2 = 0;
      fscanf(stdin, "%d", &nparts2);
      int parts2[P];
      for (int p = 0; p < nparts2; ++p)
      {
        fscanf(stdin, "%d", &parts2[p]);
      }
      // draw pie charts
      fprintf(stdout, " <g transform='translate(%f,%f) rotate(-90) scale(0.4) '>\n", i + (j & 1) * 0.5 + 0.5, j * sqrt(3) / 2);
      int part1 = parts1[nparts1 - 1];
      int part2 = parts2[0];
      int t = 0;
      for (int p = 0; p < nparts1; ++p)
      {
        const int r = parts1[p] == part1 ? 0x88 : 0xcc;
        const int g = parts1[p] == part1 ? 0x00 : 0xcc;
        const int b = parts1[p] == part1 ? 0xff : 0xcc;
        double x0 = cos(2 * M_PI * t / N);
        double y0 = sin(2 * M_PI * t / N);
        t += parts1[p];
        double x1 = cos(2 * M_PI * t / N);
        double y1 = sin(2 * M_PI * t / N);
        int large = parts1[p] > N * 0.5;
        int sweep = 1;
        fprintf(stdout, "  <path fill='#%02x%02x%02x' d='M 0 0 L %f %f A 1 1 0 %d %d %f %f L 0 0 Z' />\n", r, g, b, x0, y0, large, sweep, x1, y1);
      }
      for (int p = 0; p < nparts2; ++p)
      {
        const int r = parts2[p] == part2 ? 0xff : 0xcc;
        const int g = parts2[p] == part2 ? 0x00 : 0xcc;
        const int b = parts2[p] == part2 ? 0x88 : 0xcc;
        double x0 = cos(2 * M_PI * t / N);
        double y0 = sin(2 * M_PI * t / N);
        t += parts2[p];
        double x1 = cos(2 * M_PI * t / N);
        double y1 = sin(2 * M_PI * t / N);
        int large = parts2[p] > N * 0.5;
        int sweep = 1;
        fprintf(stdout, "  <path fill='#%02x%02x%02x' d='M 0 0 L %f %f A 1 1 0 %d %d %f %f L 0 0 Z' />\n", r, g, b, x0, y0, large, sweep, x1, y1);
      }
      fprintf(stdout, " </g>\n");
    }
  }
  fprintf(stdout, "</g>\n");
  fprintf(stdout, "</svg>\n");
  return 0;
}
