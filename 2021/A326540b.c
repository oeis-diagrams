/*
A326540
Sum of all the parts in the partitions of n into 9 primes.
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 18, 19, 20, 42, 44, ...
a(43) = 2021
*/

#include "common.c"

#define P 9
#define N 43
#define M 47

const int colours[24] =
{ 0, 0, 1, 2, 0, 3, 0, 4, 0, 0
, 0, 5, 0, 6, 0, 0, 0, 7, 0, 8
, 0, 0, 0, 9
};

int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  fprintf(stdout, "<svg version='1.1' baseProfile='full' xmlns='http://www.w3.org/2000/svg' width='6664' height='5176'>\n");
  fprintf(stdout, "<rect width='100%%' height='100%%' stroke='none' fill='#fff' />\n");
  fprintf(stdout, "<g transform='translate(532,708) scale(80)' stroke-width='0.05' stroke='#000'>\n");
  double x = 0;
  for (int m = 0; m < M; ++m)
  {
    // read input
    int parts[P];
    for (int p = 0; p < P; ++p)
    {
      fscanf(stdin, "%d", &parts[p]);
    }
    // draw rectangles
    double y = 0;
    for (int p = 0; p < P; ++p)
    {
      const int q = colours[parts[p]] - 1;
      const double f = q / 8.0;
      char fill[8];
      colour(fill, f);
      fprintf(stdout, "  <rect x='%f' y='%f' width='1' height='%d' fill='%s' />\n", x, y, parts[p], fill);
      y += parts[p] + 0.5;
    }
    x += 1 + 0.5;
  }
  fprintf(stdout, "</g>\n");
  fprintf(stdout, "</svg>\n");
  return 0;
}
