{-
A309692
Sum of the odd parts appearing among the largest parts of the partitions of n into 3 parts.
0, 0, 0, 1, 0, 3, 3, 11, 8, 20, 17, 38, 33, 60, ...
a(44) = 2021 (161 partitions)
-}

import Data.List (tails)

partitions target parts count
  | count == 0,
    target /= 0 = []
  | count == 0,
    target == 0 = return []
  | target <= 0 = []
  | target >  0 = do
      nexts@(part:_) <- tails parts
      rest <- partitions (target - part) nexts (count - 1)
      return (part : rest)

n = 44

ps = [1..n]

an = partitions n ps 3

s = unlines . map (unwords . map show) $ an

main = putStr s
