{-
A283528
The number of phi-partitions of n.
The number of partitions n = a1+a2+...+ak which have at least two parts and obey phi(n) = phi(a1)+phi(a2)+...+phi(ak). phi is Euler's totient.
0, 0, 1, 1, 2, 0, 3, 4, 8, 2, 4, 1, 5, 8, 24, 24, 6, 2, 7, ...
a(49) = 2021
-}

import Data.List (tails)
import qualified Data.Map as M

phi0 m = length [ () | k <- [1 .. m], gcd k m == 1 ]

phim = M.fromList [ (m, phi0 m) | m <- [1 .. n] ]

phi = (phim M.!)

partitions target parts
  | target == 0 = return []
  | target <= 0 = []
  | target >  0 = do
      nexts@(part:_) <- tails parts
      rest <- partitions (target - part) nexts
      return (part : rest)

n = 49

ps = [1..n]

an = filter ((phi n ==) . sum . map phi)
   . filter ((2 <=) . length)
   $ partitions n ps

s = unlines . map (\l -> unwords . map show . (length l :) $ l) $ an

main = putStr s
