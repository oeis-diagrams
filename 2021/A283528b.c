/*
A283528
The number of phi-partitions of n.
The number of partitions n = a1+a2+...+ak which have at least two parts and obey phi(n) = phi(a1)+phi(a2)+...+phi(ak). phi is Euler's totient.
0, 0, 1, 1, 2, 0, 3, 4, 8, 2, 4, 1, 5, 8, 24, 24, 6, 2, 7, ...
a(49) = 2021
*/

#include "common.c"

#define P 42
#define N 49
#define M 2021

int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  int Q = phi(N);
  fprintf(stdout, "<svg version='1.1' baseProfile='full' xmlns='http://www.w3.org/2000/svg' width='6664' height='5176'>\n");
  fprintf(stdout, "<rect width='100%%' height='100%%' stroke='none' fill='#fff' />\n");
  fprintf(stdout, "<g transform='translate(747.0,532.4813172579466) scale(110)' stroke-width='0.02' stroke='#000'>\n");
  for (int j = 0; j < 43; ++j)
  {
    for (int i = 0; i < 47; ++i)
    {
      // read input
      int nparts = 0;
      fscanf(stdin, "%d", &nparts);
      int parts[P];
      for (int p = 0; p < nparts; ++p)
      {
        fscanf(stdin, "%d", &parts[p]);
      }
      // draw pie charts
      fprintf(stdout, " <g transform='translate(%f,%f) rotate(-90) scale(0.4) '>\n", i + (j & 1) * 0.5 + 0.5, j * sqrt(3) / 2);
      double t = 0;
      for (int p = 0; p < nparts; ++p)
      {
        int q = phi(parts[p]);
        double f = pow((q - 1) / (double) (Q - 1), 0.5);
        char fill[8] = { '#', 'c', 'c', 'c', 'c', 'c', 'c', 0 };
        if (q != 0)
        {
          colour(fill, f);
        }
        double x0 = cos(2 * M_PI * t / N);
        double y0 = sin(2 * M_PI * t / N);
        t += parts[p];
        double x1 = cos(2 * M_PI * t / N);
        double y1 = sin(2 * M_PI * t / N);
        int large = parts[p] > N * 0.5;
        int sweep = 1;
        fprintf(stdout, "  <path fill='%s' d='M 0 0 L %f %f A 1 1 0 %d %d %f %f L 0 0 Z' />\n", fill, x0, y0, large, sweep, x1, y1);
      }
      fprintf(stdout, " </g>\n");
    }
  }
  fprintf(stdout, "</g>\n");
  fprintf(stdout, "</svg>\n");
  return 0;
}
