/*
A309692
Sum of the odd parts appearing among the largest parts of the partitions of n into 3 parts.
0, 0, 0, 1, 0, 3, 3, 11, 8, 20, 17, 38, 33, 60, ...
a(44) = 2021 (161 partitions)
*/

#include <stdio.h>

#define P 3
#define N 44
#define M 161

int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  fprintf(stdout, "<svg version='1.1' baseProfile='full' xmlns='http://www.w3.org/2000/svg' width='6664' height='5176'>\n");
  fprintf(stdout, "<rect width='100%%' height='100%%' stroke='none' fill='#fff' />\n");
  fprintf(stdout, "<g transform='translate(465.5,518.0) scale(45)' stroke-width='0.0225' stroke='#000'>\n");
  for (int j = 0; j < 2; ++j)
  {
    double x = j * 0.8;
    for (int i = 0; i < (M + 1 - j)/2; ++i)
    {
      // read input
      int parts[P];
      for (int p = 0; p < P; ++p)
      {
        fscanf(stdin, "%d", &parts[p]);
      }
      // draw rectangles
      double y = j * (N + 2 + (P - 1) * 0.5);
      for (int p = 0; p < P; ++p)
      {
        const char *fill = p == P - 1 && (parts[p] & 1) ? "#f08" : "#ccc";
        fprintf(stdout, "  <rect x='%f' y='%f' width='1' height='%d' fill='%s' />\n", x, y, parts[p], fill);
        y += parts[p] + 0.5;
      }
      x += 1 + 0.6;
    }
  }
  fprintf(stdout, "</g>\n");
  fprintf(stdout, "</svg>\n");
  return 0;
}
