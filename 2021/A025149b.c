/*
A326540
Sum of all the parts in the partitions of n into 9 primes.
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 18, 19, 20, 42, 44, ...
a(43) = 2021
*/

#include "common.c"

#define P 5
#define N 36
#define M 120

int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  fprintf(stdout, "<svg version='1.1' baseProfile='full' xmlns='http://www.w3.org/2000/svg' width='6664' height='5176'>\n");
  fprintf(stdout, "<rect width='100%%' height='100%%' stroke='none' fill='#fff' />\n");
  fprintf(stdout, "<g transform='translate(947,638) scale(50)' stroke-width='0.025' stroke='#000'>\n");
  for (int j = 0; j < 2; ++j)
  {
    double x = 0;
    for (int i = 0; i < M/2; ++i)
    {
      // read input
      int nparts = 0;
      fscanf(stdin, "%d", &nparts);
      int parts[P];
      for (int p = 0; p < nparts; ++p)
      {
        fscanf(stdin, "%d", &parts[p]);
      }
      // draw rectangles
      double y = j * 40;
      for (int p = 0; p < nparts; ++p)
      {
        const double f = parts[p] / (double) N;
        char fill[8];
        colour(fill, f);
        double adj = 0;
        if (p == nparts - 1)
        {
          adj = 4 * 0.5 - (nparts - 1) * 0.5;
        }
        fprintf(stdout, "  <rect x='%f' y='%f' width='1' height='%f' fill='%s' />\n", x, y, parts[p] + adj, fill);
        y += parts[p] + 0.5;
      }
      x += 1 + 0.6;
    }
  }
  fprintf(stdout, "</g>\n");
  fprintf(stdout, "</svg>\n");
  return 0;
}
