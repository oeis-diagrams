/*
A005816
Number of 4-valent labeled graphs with n nodes where multiple edges and loops are allowed.
1, 1, 3, 15, 138, 2021, ...
a(5) = 2021
*/

#include <stdio.h>
#include <stdlib.h>

#define N 4

struct rep
{
  int r[N * 4][2];
};

int consistent(struct rep r)
{
  int counts[N];
  for (int i = 0; i < N; ++i)
  {
    counts[i] = 0;
  }
  for (int l = 0; l < N * 4; ++l)
  {
    if (r.r[l][0] < 0)
    {
      continue;
    }
    for (int c = 0; c < 2; ++c)
    {
      counts[r.r[l][c]]++;
    }
  }
  for (int i = 0; i < N; ++i)
  {
    if (counts[i] != 4)
    {
      return 0;
    }
  }
  return 1;
}

int cmp_int2(const void *x, const void *y)
{
  const int *p = x;
  const int *q = y;
  if (p[0] == q[0])
  {
    return p[1] - q[1];
  }
  return p[0] - q[0];
}

struct rep sort(struct rep a)
{
  struct rep b;
  for (int l = 0; l < N * 4; ++l)
  {
    if (a.r[l][0] < a.r[l][1])
    {
      b.r[l][0] = a.r[l][0];
      b.r[l][1] = a.r[l][1];
    }
    else
    {
      b.r[l][0] = a.r[l][1];
      b.r[l][1] = a.r[l][0];
    }
  }
  qsort(&b.r[0][0], N * 4, sizeof(int) * 2, cmp_int2);
  return b;
}

void output(int matrix[N][N])
{
  struct rep r;
  for (int j = 0; j < N * 4; ++j)
  {
    for (int k = 0; k < 2; ++k)
    {
      r.r[j][k] = -1;
    }
  }
  int l = 0;
  for (int i = 0; i < N; ++i)
  {
    for (int j = i; j < N; ++j)
    {
      for (int k = 0; k < matrix[i][j]; k += 1 + (i == j))
      {
        r.r[l][0] = i;
        r.r[l][1] = j;
        ++l;
      }
    }
  }
  r = sort(r);
  if (! consistent(r))
  {
    return;
  }
  for (int l = 0; l < N * 4; ++l)
  {
    if (r.r[l][0] < 0)
    {
      continue;
    }
    fprintf(stdout, "%c%c", 'A' + r.r[l][0], 'A' + r.r[l][1]);
  }
  fprintf(stdout, "\n");
}

void A005816(int matrix[N][N], int i, int j)
{
  
  if (j >= N)
  {
    i += 1;
    j = i;
  }
  if (i >= N)
  {
    output(matrix);
  }
  else
  {
    for (int k = 0; k <= N; ++k)
    {
      matrix[i][j] = k;
      matrix[j][i] = k;
      A005816(matrix, i, j + 1);
    }
  }
}

int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  int matrix[N][N];
  for (int i = 0; i < N; ++i)
  {
    for (int j = 0; j < N; ++j)
    {
      matrix[i][j] = 0;
    }
  }
  A005816(matrix, 0, 0);
  return 0;
}
