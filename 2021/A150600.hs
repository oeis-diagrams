{-
A150600
Number of walks within N^3 (the first octant of Z^3) starting at (0,0,0)
and consisting of n steps taken from {(-1, -1, 1), (-1, 0, 0), (0, 1, 1),
(1, 0, -1), (1, 0, 1)}.
1, 2, 7, 27, 109, 462, 2021, ...
a(6) = 2021
-}

start = (0, 0, 0)
steps = [(-1, -1, 1), (-1, 0, 0), (0, 1, 1), (1, 0, -1), (1, 0, 1)]
ipd = 0.1

a150600 = go start
  where
    go (x,y,z) _ | x < 0 || y < 0 || z < 0 = fail "out of bounds"
    go pos 0 = pure [pos]
    go pos@(x,y,z) n = do
      (dx, dy, dz) <- steps
      rest <- go (x + dx, y + dy, z + dz) (n - 1)
      pure (pos : rest)

project eye (x, y, z) =
  ( 100 * ((y - z) * (sqrt 3 / 2) + eye * l) / (1 + l)
  , 100 * (x - (y + z) / 2) / (1 + l)
  )
  where l = sqrt (x^2 + y^2 + z^2)

draw offset (p:rest@(q:_)) = do
  line "#f08" offset (project (-ipd/2) p) (project (-ipd/2) q)
  line "#80f" offset (project ( ipd/2) p) (project ( ipd/2) q)
  draw offset rest
draw _ _ = return ()

line colour (a, b) (u, v) (x, y) = do
  putStrLn $ "<path d='M " ++ show (a + s * u) ++ "," ++ show (b + s * v) ++
                     " L " ++ show (a + s * x) ++ "," ++ show (b + s * y) ++
                     "' stroke='" ++ colour ++ "' />"
  where
    s = 0.5

offsets =
  [ (55 * (fromIntegral i + if odd j then 0 else 0.5), fromIntegral j * 55 * sqrt 3 / 2)
  | j <- [ 1 .. 43 ], i <- [ 1 .. 47 ]
  ]

main = do
  putStr "<svg version='1.1' baseProfile='full' xmlns='http://www.w3.org/2000/svg' width='6664' height='5176'>\n"
  putStr "<rect width='100%' height='100%' stroke='none' fill='#fff' />\n"
  putStr$"<g transform='translate(" ++ show x ++ "," ++ show y ++ ") scale(" ++ show s ++ ")' stroke-width='2' stroke-linecap='round'>\n"
  sequence_ (zipWith draw offsets (a150600 6))
  putStr "</g>\n</svg>\n"
  where
    x = (6664 - w) / 2
    y = (5176 - h) / 2
    w = 47 * 55 * s
    h = (42 * sqrt 3 / 2 + 1) * 55 * s
    s = 2
