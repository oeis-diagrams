{-
A325250
Number of integer partitions of n whose omega-sequence is strict (no repeated parts).  The omega-sequence of an integer partition is the sequence of lengths of the multisets obtained by repeatedly taking the multiset of multiplicities until a singleton is reached.
1, 1, 2, 2, 3, 2, 5, 2, 5, 4, 6, 2, 11, 3, 10, 12, 17, ...
a(43) = 2021
-}

import Data.List (group, nub, sort, tails)

n = 43

partitions target parts
  | target == 0 = return []
  | target <= 0 = []
  | target >  0 = do
      nexts@(part:_) <- tails parts
      rest <- partitions (target - part) nexts
      return (part : rest)

omega = map length . group . sort

strict xs = xs == nub xs

omegaStrict = strict . takeWhile (1 <) . map length . iterate omega

an = filter omegaStrict $ partitions n [1..n]

s = unlines . map (\l -> unwords . map show . (length l :) $ l) $ an

main = putStr s
