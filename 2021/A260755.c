/*
A260744
Number of prime juggling patterns of period n using 2 balls.
1, 2, 5, 10, 23, 48, 105, 216, 467, 958, 2021, ...
a(11) = 2021
*/

#include "common.c"

#define N 11

#define I 47
#define J 43

int k = 0;
void output(int siteswap[N])
{
  int i0 = k % I;
  int j0 = k / I;
  double x = i0 + (j0 & 1) * 0.5 + 0.5;
  double y = j0 * sqrt(3) / 2;
  fprintf(stdout, "<g transform='translate(%f,%f) scale(0.4)'>\n", x, y);
  int balls[2 * N];
  for (int i = 0; i < 2 * N; ++i)
  {
    balls[i] = 0;
  }
  int next_ball = 0;
  for (int pass = 0; pass < 4; ++pass)
  {
    for (int i = 0; i < 2 * N; ++i)
    {
      int b = balls[0];
      for (int j = 1; j < 2 * N; ++j)
      {
        balls[j - 1] = balls[j];
      }
      balls[2 * N - 1] = b;
      if (siteswap[i % N])
      {
        if (! b)
        {
          b = ++next_ball;
        }
        balls[siteswap[i % N] - 1] = b;
      }
    }
  }
  const int o = 16;
  for (int hand = 0; hand < 2; ++hand)
  {
    for (int i = 0; i < N; ++i)
    {
      int t = hand + 2 * i;
      int s = siteswap[t % N];
      if (s)
      {
        double r0 = hand ? 1 : 0.5;
        double r1 = hand ? (s & 1) ? 0.5 : 1 : (s & 1) ? 1 : 0.5;
        int k = t * o;
        double a = 2 * M_PI * k / (2 * N * o);
        double u = r0 * cos(a);
        double v = r0 * sin(a);
        const char *fill = balls[t] == 1 ? "#f08" : "#80f";
        fprintf(stdout, "<path stroke='%s' d='M %f %f ", fill, u, v);
        for (k = t * o + 1; k <= (t + s) * o; ++k)
        {
          double f = (k - t * o) / (double) (s * o);
          double r = r0 + f * (r1 - r0);
          a = 2 * M_PI * k / (2 * N * o);
          u = r * cos(a);
          v = r * sin(a);
          fprintf(stdout, "L %f %f ", u, v);
        }
        fprintf(stdout, "' />\n");
      }
    }
  }
  for (int hand = 0; hand < 2; ++hand)
  {
    for (int i = 0; i < N; ++i)
    {
      int t = hand + 2 * i;
      int s = siteswap[t % N];
      if (s)
      {
        double r0 = hand ? 1 : 0.5;
        int k = t * o;
        double a = 2 * M_PI * k / (2 * N * o);
        double u = r0 * cos(a);
        double v = r0 * sin(a);
        const char *fill = balls[t] == 1 ? "#f08" : "#80f";
        fprintf(stdout, "<circle cx='%f' cy='%f' r='0.15' stroke='none' fill='%s' />\n", u, v, fill);
      }
    }
  }
  fprintf(stdout, "</g>\n");
  ++k;
}

int sum(int siteswap[N])
{
  int s = 0;
  for (int i = 0; i < N; ++i)
  {
    s += siteswap[i];
  }
  return s;
}

void recurse(int minstate, int state0, int state, int visited[N], int siteswap[N], int period)
{
  if (minstate != state0)
  {
    return;
  }
  if (period == N)
  {
    if (state == state0 && sum(siteswap) == 2 * N)
    {
      output(siteswap);
    }
    return;
  }
  for (int i = 0; i < period; ++i)
  {
    if (visited[i] == state)
    {
      return;
    }
  }
  visited[period] = state;
  if (state & 1)
  {
    state >>= 1;
    for (int bit = 0; bit < 2 * N; ++bit)
    {
      if ((state & (1 << bit)) == 0)
      {
        int next = state | (1 << bit);
        siteswap[period] = bit + 1;
        recurse(min(minstate, next), state0, next, visited, siteswap, period + 1);
      }
    }
  }
  else
  {
    state >>= 1;
    int next = state;
    siteswap[period] = 0;
    recurse(min(minstate, next), state0, next, visited, siteswap, period + 1);
  }
}

int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  fprintf(stdout, "<svg version='1.1' baseProfile='full' xmlns='http://www.w3.org/2000/svg' width='6664' height='5176'>\n");
  fprintf(stdout, "<rect width='100%%' height='100%%' stroke='none' fill='#fff' />\n");
  fprintf(stdout, "<g transform='translate(747.0,532.4813172579466) scale(110)' fill='none' stroke-width='0.2' stroke='#000' stroke-linecap='round' stroke-linejoin='round'>\n");
  int visited[N], siteswap[N];
  for (int bit1 = 0; bit1 < 2 * N; ++bit1)
  {
    for (int bit2 = 0; bit2 < bit1; ++bit2)
    {
      int state = (1 << bit1) | (1 << bit2);
      recurse(state, state, state, visited, siteswap, 0);
    }
  }
  fprintf(stdout, "</g>\n");
  fprintf(stdout, "</svg>\n");
  return 0;
}
