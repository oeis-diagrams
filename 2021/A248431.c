/*
A248431
Number of length n+2 0..6 arrays with every three consecutive terms having the sum of some two elements equal to twice the third.
61, 105, 185, 327, 601, 1105, 2021, ...
a(7) = 2021
*/

#include <stdio.h>

int consistent(int a, int b, int c)
{
  return a + b == 2 * c || b + c == 2 * a || c + a == 2 * b;
}

int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  int I = 43 * 10;
  int J = 47 * 7;
  double s = 13;
  double x = (6664 - s * I) / 2;
  double y = (5176 - s * J) / 2;
  fprintf(stdout, "<svg version='1.1' baseProfile='full' xmlns='http://www.w3.org/2000/svg' width='6664' height='5176'>\n");
  fprintf(stdout, "<rect width='100%%' height='100%%' stroke='none' fill='#fff' />\n");
  fprintf(stdout, "<g transform='translate(%f,%f) scale(%f)' stroke-width='0.2' stroke='#000' stroke-linecap='round' >\n", x, y, s);
  int k = 0;
  for (int n0 = 0; n0 <= 6; ++n0)
  {
    for (int n1 = 0; n1 <= 6; ++n1)
    {
      for (int n2 = 0; n2 <= 6; ++n2)
      {
        if (! consistent(n0, n1, n2))
        {
          continue;
        }
        for (int n3 = 0; n3 <= 6; ++n3)
        {
          if (! consistent(n1, n2, n3))
          {
            continue;
          }
          for (int n4 = 0; n4 <= 6; ++n4)
          {
            if (! consistent(n2, n3, n4))
            {
              continue;
            }
            for (int n5 = 0; n5 <= 6; ++n5)
            {
              if (! consistent(n3, n4, n5))
              {
                continue;
              }
              for (int n6 = 0; n6 <= 6; ++n6)
              {
                if (! consistent(n4, n5, n6))
                {
                  continue;
                }
                for (int n7 = 0; n7 <= 6; ++n7)
                {
                  if (! consistent(n5, n6, n7))
                  {
                    continue;
                  }
                  for (int n8 = 0; n8 <= 6; ++n8)
                  {
                    if (! consistent(n6, n7, n8))
                    {
                      continue;
                    }
                    int n[9] = { n0, n1, n2, n3, n4, n5, n6, n7, n8 };
                    int i = k % 43;
                    int j = k / 43;
                    fprintf(stdout, "<path transform='translate(%d,%d)' d='M 0 0 L 9 0 ", 10 * i, 7 * j);
                    for (int x = 0; x < 9; ++x)
                    {
                      if (n[x] > 0)
                      {
                        fprintf(stdout, "M %d.5 0 L %d.5 %d M %d.25 %d.5 L %d.5 %d L %d.75 %d.5 ", x, x, n[x], x, n[x] - 1, x, n[x], x, n[x] - 1);
                      }
                    }
                    fprintf(stdout, "' />\n");
                    ++k;
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  fprintf(stdout, "</g>\n");
  fprintf(stdout, "</svg>\n");
  return 0;
}
