/*
A266543
Number of n X 4 binary arrays with rows and columns lexicographically nondecreasing and row and column sums nonincreasing.
2, 4, 6, 12, 16, 27, 36, 57, 76, 114, 149, 213, 276, 379, ...
a(21) = 2021
*/

#include <stdio.h>

#define M 4
#define N 21
#define I 107
#define J 19

int compare_rows(int matrix[N][M], int n1, int n2)
{
  for (int m = 0; m < M; ++m)
  {
    if (matrix[n1][m] < matrix[n2][m]) return -1;
    if (matrix[n1][m] > matrix[n2][m]) return  1;
  }
  return 0;
}

int compare_columns(int matrix[N][M], int m1, int m2)
{
  for (int n = 0; n < N; ++n)
  {
    if (matrix[n][m1] < matrix[n][m2]) return -1;
    if (matrix[n][m1] > matrix[n][m2]) return  1;
  }
  return 0;
}

int compare_row_sums(int matrix[N][M], int n1, int n2)
{
  int s1 = 0, s2 = 0;
  for (int m = 0; m < M; ++m)
  {
    s1 += matrix[n1][m];
    s2 += matrix[n2][m];
  }
  if (s1 < s2) return -1;
  if (s1 > s2) return  1;
  return 0;
}

int compare_column_sums(int matrix[N][M], int m1, int m2)
{
  int s1 = 0, s2 = 0;
  for (int n = 0; n < N; ++n)
  {
    s1 += matrix[n][m1];
    s2 += matrix[n][m2];
  }
  if (s1 < s2) return -1;
  if (s1 > s2) return  1;
  return 0;
}

int consistent(int matrix[N][M], int n)
{
  int n1 = n - 1;
  if (1 <= n1)
  {
    if (compare_rows(matrix, n1 - 1, n1) > 0) return 0;
    if (compare_row_sums(matrix, n1 - 1, n1) < 0) return 0;
  }
  if (n == N)
  {
    for (int m1 = 1; m1 < 4; ++m1)
    {
      if (compare_columns(matrix, m1 - 1, m1) > 0) return 0;
      if (compare_column_sums(matrix, m1 - 1, m1) < 0) return 0;
    }
  }
  return 1;
}

int i = 0;
int j = 0;

void output(int matrix[N][M])
{
  int o = !!(j % 3);
  for (int n = 0; n < N; ++n)
  {
    for (int m = 0; m < M; ++m)
    {
      int x = i * (M + 1) + m;
      int y = j * (N + 1) + n;
      fprintf(stdout, "  <rect x='%f' y='%d' width='1' height='1' fill='%s' />\n", x + (M + 1) * 0.5 * o, y, matrix[n][m] ? "#f08" : "#ccc");
    }
  }
  ++i;
  if (i == I - o)
  {
    i = 0;
    ++j;
  }
}

void recurse(int matrix[N][M], int m, int n)
{
  if (m == M)
  {
    m = 0;
    ++n;
    if (! consistent(matrix, n))
    {
      return;
    }
  }
  if (n == N)
  {
    output(matrix);
  }
  else
  {
    for (int bit = 0; bit < 2; ++bit)
    {
      matrix[n][m] = bit;
      recurse(matrix, m + 1, n);
    }
  }
}

int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  double s = 10;
  double x = (6664 - s * (I * M + (I - 1) * 1)) / 2;
  double y = (5176 - s * (J * N + (J - 1) * 1)) / 2;
  fprintf(stdout, "<svg version='1.1' baseProfile='full' xmlns='http://www.w3.org/2000/svg' width='6664' height='5176'>\n");
  fprintf(stdout, "<rect width='100%%' height='100%%' stroke='none' fill='#fff' />\n");
  fprintf(stdout, "<g transform='translate(%f,%f) scale(%f)' stroke-width='0.05' stroke='#000' >\n", x, y, s);
  int matrix[N][M];
  for (int n = 0; n < N; ++n)
  {
    for (int m = 0; m < M; ++m)
    {
      matrix[n][m] = 0;
    }
  }
  recurse(matrix, 0, 0);
  fprintf(stdout, "</g>\n");
  fprintf(stdout, "</svg>\n");
  return 0;  
}
