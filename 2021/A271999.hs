{-
A271999
Halogen sequence (the atomic numbers of the elements in group 17 in the periodic table).
1, 11, 17, 35, 53, 85, 117
a(21) = 2021
-}

import Data.List (sort)

n = 21

shells = take n . drop 1 . sort . concat $
  [[2 * (m - 1)^2, 2 * (m + 1)^2] | m <- [1..n]]

shell r m =
  "<circle cx='0' cy='0' r='" ++ show (r^2) ++"' fill='none' stroke-width='" ++ show (fromIntegral (r^2) / fromIntegral m * 0.1) ++ "' />\n"
  ++ unlines
      [ "<circle cx='" ++ show x ++ "' cy='" ++ show y ++ "' r='" ++ show (fromIntegral (r^2) / fromIntegral m * if r == n && i == m then 4 else 2) ++ "' fill='" ++ fill ++ "' />"
      | i <- [1 .. m]
      , let t = 2 * pi * ((fromIntegral i + (if odd r then 0.5 else 0)) / fromIntegral m + 2/3)
      , let x = fromIntegral (r^2) * cos t
      , let y = fromIntegral (r^2) * sin t
      , let fill = if r == n && i == m then "#80f" else "#f08"
      ]

an = concat $ zipWith shell [1..] shells

main = do
  putStr "<svg version='1.1' baseProfile='full' xmlns='http://www.w3.org/2000/svg' width='6664' height='5176'>\n"
  putStr "<rect width='100%' height='100%' stroke='none' fill='#fff' />\n"
  putStr$"<g transform='translate(" ++ show x ++ "," ++ show y ++ ") scale(" ++ show s ++ ")' stroke='#000' stroke-width='0.1'>\n"
  putStr an
  putStr "</g>\n"
  putStr$"<g transform='translate(6200," ++ show y ++") rotate(-90)' stroke='none' fill='#000'>\n"
  putStr "<text style='font-family: \"Latin Modern Sans\"; font-size: 300px; text-anchor: middle;'>mathr.co.uk/calendar/2021</text>\n"
  putStr "</g>\n"
  putStr "</svg>\n"
  where
    x = 6664 / 2
    y = 5176 / 2
    s = 5
