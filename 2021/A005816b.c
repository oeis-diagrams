/*
A005816
Number of 4-valent labeled graphs with n nodes where multiple edges and loops are allowed.
1, 1, 3, 15, 138, 2021, ...
a(5) = 2021
*/

#include "common.c"

#define N 4

int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  char fill[8];
  colour(fill, 1);
  int l = 0;
  fprintf(stdout, "<svg version='1.1' baseProfile='full' xmlns='http://www.w3.org/2000/svg' width='6664' height='5176'>\n");
  fprintf(stdout, "<rect width='100%%' height='100%%' stroke='none' fill='#fff' />\n");
  fprintf(stdout, "<g transform='translate(732,388) scale(200)' stroke-width='0.03' stroke='#000' fill='none'>\n");
  for (int j = 0; j < 11; ++j)
  {
    for (int i = 0; i < 13 - (j & 1); ++i)
    {
      // initialize matrix
      int matrix[N][N];
      for (int a = 0; a < N; ++a)
      {
        for (int b = 0; b < N; ++b)
        {
          matrix[a][b] = 0;
        }
      }
      // read graph
      {
        int a = -1, b = -1;
        while ((a = fgetc(stdin)) != '\n')
        {
          b = fgetc(stdin);
          matrix[a - 'A'][b - 'A']++;
          matrix[b - 'A'][a - 'A']++;
        }
      }
      // draw graph
      double xs[N] = { 0.1, 0.9, 0.9, 0.1 };
      double ys[N] = { 0.1, 0.1, 0.9, 0.9 };
      fprintf(stdout, " <g transform='translate(%f,%f)'>\n", i * 2 + 0.5 + (j & 1), j * 2 + 0.5);
      for (int a = 0; a < N; ++a)
      {
        int k = matrix[a][a] / 2;
        double dx = xs[a] - 0.5;
        double dy = ys[a] - 0.5;
        double d = sqrt(dx * dx + dy * dy);
        double nx = dx / d;
        double ny = dy / d;
        for (int c = 0; c < k; ++c)
        {
          double f = (c + 1) * (k == 2 ? 0.15 : 0.2);
          fprintf(stdout, "  <circle cx='%f' cy='%f' r='%f' />\n", xs[a] + f * nx, ys[a] + f * ny, f);
        }
      }
      fprintf(stdout, "  <path d='");
      for (int a = 0; a < N; ++a)
      {
        for (int b = a + 1; b < N; ++b)
        {
          double dx = xs[b] - xs[a];
          double dy = ys[b] - ys[a];
          double d = sqrt(dx * dx + dy * dy);
          double nx = -dy / d;
          double ny =  dx / d;
          double mx = (xs[b] + xs[a]) * 0.5;
          double my = (ys[b] + ys[a]) * 0.5;
          int k = matrix[a][b];
          for (int c = 0; c < k; ++c)
          {
            double f = (c + 0.5) / k - 0.5;
            fprintf(stdout, "M %f %f Q %f %f %f %f ", xs[a], ys[a], mx + f * nx, my + f * ny, xs[b], ys[b]);
          }
        }
      }
      fprintf(stdout, "' />\n");
      fprintf(stdout, "  <g stroke='none' fill='%s'>\n", fill);
      fprintf(stdout, "    <circle cx='0.1' cy='0.1' r='0.15' />\n");
      fprintf(stdout, "    <circle cx='0.9' cy='0.1' r='0.15' />\n");
      fprintf(stdout, "    <circle cx='0.9' cy='0.9' r='0.15' />\n");
      fprintf(stdout, "    <circle cx='0.1' cy='0.9' r='0.15' />\n");
      fprintf(stdout, "  </g>\n");
      fprintf(stdout, " </g>\n");
      
      // next
      l++;
      if (l >= 138)
      {
        break;
      }
    }
    if (l >= 138)
    {
      break;
    }
  }
  fprintf(stdout, "</g>\n");
  fprintf(stdout, "</svg>\n");
  return 0;
}
