{-
A238220
The total number of 5's in all partitions of n into an even number of distinct parts.
0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 2, 2, 2, 3, 3, 5, 6, 7, 8, 9, 12, ...
a(60) = 2021 (5440 partitions)
a(46) = 410 (1152 partitions)
-}

import Data.List (tails)

partitions target parts
  | target <  0 = []
  | target == 0 = return []
  | target >  0 = do
      part:nexts <- tails parts
      rest <- partitions (target - part) nexts
      return (part : rest)

n = 46

ps = [1 .. n]

an = filter (even . length) $ partitions n ps

s = unlines . map (\l -> unwords . map show . (length l :) $ l) $ an

main = putStr s
