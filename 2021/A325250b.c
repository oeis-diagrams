/*
A325250
Number of integer partitions of n whose omega-sequence is strict (no repeated parts).  The omega-sequence of an integer partition is the sequence of lengths of the multisets obtained by repeatedly taking the multiset of multiplicities until a singleton is reached.
1, 1, 2, 2, 3, 2, 5, 2, 5, 4, 6, 2, 11, 3, 10, 12, 17, ...
a(43) = 2021
*/

#include "common.c"

#define P 43
#define N 43
#define M 2021

int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  fprintf(stdout, "<svg version='1.1' baseProfile='full' xmlns='http://www.w3.org/2000/svg' width='6664' height='5176'>\n");
  fprintf(stdout, "<rect width='100%%' height='100%%' stroke='none' fill='#fff' />\n");
  fprintf(stdout, "<g transform='translate(747.0,532.4813172579466) scale(110)' stroke-width='0.02' stroke='#000'>\n");
  for (int j = 0; j < 43; ++j)
  {
    for (int i = 0; i < 47; ++i)
    {
      // read input
      int nparts = 0;
      fscanf(stdin, "%d", &nparts);
      int parts[P];
      for (int p = 0; p < nparts; ++p)
      {
        fscanf(stdin, "%d", &parts[p]);
      }
      // draw pie charts
      fprintf(stdout, " <g transform='translate(%f,%f) rotate(-90) scale(0.4) '>\n", i + (j & 1) * 0.5 + 0.5, j * sqrt(3) / 2);
      if (nparts == 1)
      {
        double f = pow((parts[0] - 1) / (double) (N - 1), 0.5);
        char fill[8];
        colour(fill, f);
        fprintf(stdout, "  <circle fill='%s' cx='0' cy='0' r='1' />\n", fill);
      }
      else
      {
        double t = 0;
        for (int p = 0; p < nparts; ++p)
        {
          double f = pow((parts[p] - 1) / (double) (N - 1), 0.5);
          char fill[8];
          colour(fill, f);
          double x0 = cos(2 * M_PI * t / N);
          double y0 = sin(2 * M_PI * t / N);
          t += parts[p];
          double x1 = cos(2 * M_PI * t / N);
          double y1 = sin(2 * M_PI * t / N);
          int large = parts[p] > N * 0.5;
          int sweep = 1;
          fprintf(stdout, "  <path fill='%s' d='M 0 0 L %f %f A 1 1 0 %d %d %f %f L 0 0 Z' />\n", fill, x0, y0, large, sweep, x1, y1);
        }
      }
      fprintf(stdout, " </g>\n");
    }
  }
  fprintf(stdout, "</g>\n");
  fprintf(stdout, "</svg>\n");
  return 0;
}
