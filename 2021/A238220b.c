/*
A238220
The total number of 5's in all partitions of n into an even number of distinct parts.
0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 2, 2, 2, 3, 3, 5, 6, 7, 8, 9, 12, ...
a(60) = 2021 (5440 partitions)
a(46) = 410 (1152 partitions)
cf A067661
*/

#include <math.h>
#include <stdio.h>

#define P 46
#define N 46

int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  int I = 36;
  int J = 32;
  double s = 160;
  double x = (6664 - s * (I + 0.5)) / 2;
  double y = (5176 - s * ((J - 1) * sqrt(3)/2 + 0.5)) / 2;
  fprintf(stdout, "<svg version='1.1' baseProfile='full' xmlns='http://www.w3.org/2000/svg' width='6664' height='5176'>\n");
  fprintf(stdout, "<rect width='100%%' height='100%%' stroke='none' fill='#fff' />\n");
  fprintf(stdout, "<g transform='translate(%f,%f) scale(%f)' stroke-width='0.01666' stroke='#000'>\n", x, y, s);
  for (int j = 0; j < J; ++j)
  {
    for (int i = 0; i < I; ++i)
    {
      // read input
      int nparts = 0;
      fscanf(stdin, "%d", &nparts);
      int parts[P];
      for (int p = 0; p < nparts; ++p)
      {
        fscanf(stdin, "%d", &parts[p]);
      }
      // draw pie charts
      fprintf(stdout, " <g transform='translate(%f,%f) rotate(-90) scale(0.4) '>\n", i + (j & 1) * 0.5 + 0.5, j * sqrt(3) / 2);
      double t = 0;
      for (int p = 0; p < nparts; ++p)
      {
        const char  *fill = parts[p] == 5 ? "#f08" : "#ccc";
        double x0 = cos(2 * M_PI * t / N);
        double y0 = sin(2 * M_PI * t / N);
        t += parts[p];
        double x1 = cos(2 * M_PI * t / N);
        double y1 = sin(2 * M_PI * t / N);
        int large = parts[p] > N * 0.5;
        int sweep = 1;
        fprintf(stdout, "  <path fill='%s' d='M 0 0 L %f %f A 1 1 0 %d %d %f %f L 0 0 Z' />\n", fill, x0, y0, large, sweep, x1, y1);
      }
      fprintf(stdout, " </g>\n");
    }
  }
  fprintf(stdout, "</g>\n");
  fprintf(stdout, "</svg>\n");
  return 0;
}
