{-
A326540
Sum of all the parts in the partitions of n into 9 primes.
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 18, 19, 20, 42, 44, ...
a(43) = 2021
-}

import Data.List (intersperse, tails, transpose)
import Data.Maybe (catMaybes)

primes = filterPrime [2..]
  where filterPrime (p:xs) =
          p : filterPrime [x | x <- xs, x `mod` p /= 0]

partitions target parts count
  | count == 0,
    target /= 0 = []
  | count == 0,
    target == 0 = return []
  | target <= 0 = []
  | target >  0 = do
      nexts@(part:_) <- tails parts
      rest <- partitions (target - part) nexts (count - 1)
      return (part : rest)

n = 43

ps = takeWhile (<= n) primes

m = ps `zip` ['A'..]

an = partitions n ps $ 9

s = unlines . map (unwords . map show) $ an

main = putStr s
