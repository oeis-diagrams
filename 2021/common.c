#include <math.h>
#include <stdio.h>

// srgb in 0..1

struct srgb
{
  float r, g, b;
};

// srgb in 0..255

struct srgb8
{
  unsigned char r, g, b;
};

struct srgb8 srgb_to_srgb8(struct srgb s)
{
  struct srgb8 o;
  o.r = fminf(fmaxf(roundf(255.0f * s.r), 0), 255);
  o.g = fminf(fmaxf(roundf(255.0f * s.g), 0), 255);
  o.b = fminf(fmaxf(roundf(255.0f * s.b), 0), 255);
  return o;
}

// hsv in 0..1

struct hsv
{
  float h, s, v;
};

// HSV is a bit of a hack, assume it defines srgb for simplicity

struct srgb hsv2rgb(struct hsv a)
{
  float hue = a.h * 6.0f;
  float sat = a.s;
  float bri = a.v;
  int i = (int) floorf(hue);
  float f = hue - i;
  if (! (i & 1))
    f = 1.0f - f;
  float m = bri * (1.0f - sat);
  float n = bri * (1.0f - sat * f);
  struct srgb cPos;
  switch (i)
  {
  case 6:
  case 0: cPos.b = bri;
    cPos.g = n;
    cPos.r = m;
    break;
  case 1: cPos.b = n;
    cPos.g = bri;
    cPos.r = m;
    break;
  case 2: cPos.b = m;
    cPos.g = bri;
    cPos.r = n;
    break;
  case 3: cPos.b = m;
    cPos.g = n;
    cPos.r = bri;
    break;
  case 4: cPos.b = n;
    cPos.g = m;
    cPos.r = bri;
    break;
  case 5: cPos.b = bri;
    cPos.g = m;
    cPos.r = n;
  }
  return cPos;
}

void colour(char *buffer, float x)
{
  float hue0 = 330 / 360.0;
  float hue1 = (270 + 360) / 360.0;
  float hue = hue0 + x * (hue1 - hue0);
  hue -= floorf(hue);
  struct hsv h = { hue, 0.9, 0.9 };
  struct srgb8 s = srgb_to_srgb8(hsv2rgb(h));
  snprintf(buffer, 8, "#%02x%02x%02x", s.r, s.g, s.b);
}

int min(int m, int n)
{
  return m < n ? m : n;
}

int gcd(int m, int n)
{
  int d = 0;
  for (int i = 1; i < min(m, n); ++i)
  {
    if (m % i == 0 && n % i == 0)
    {
      d = i;
    }
  }
  return d;
}

int phi(int n)
{
  int s = 0;
  for (int i = 1; i <= n; ++i)
  {
    s += gcd(i, n) != 0;
  }
  return s;
}
