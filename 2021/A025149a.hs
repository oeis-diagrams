{-
A025149
Number of partitions of n into distinct parts >= 4.
1, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 3, 3, 4, 4, 6, 6, 8, 9, 11, 12, 15, 17, ...
a(61) = 2021
a(47) = 447
a(36) = 120
a(27) = 36
-}

import Data.List (tails)

partitions target parts
  | target <  0 = []
  | target == 0 = return []
  | target >  0 = do
      part:nexts <- tails parts
      rest <- partitions (target - part) nexts
      return (part : rest)

n = 36

ps = [4 .. n]

an = partitions n ps

s = unlines . map (\l -> unwords . map show . (length l :) $ l) $ an

main = putStr s
