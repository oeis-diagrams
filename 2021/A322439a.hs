{-
A322439
Number of ordered pairs of integer partitions of n where no part of the first is greater than any part of the second.
1, 1, 3, 5, 11, 15, 33, 42, 82, 114, 195, 258, 466, ...
a(16) = 2021
-}

import Data.List (tails)

n = 16

partitions target parts
  | target == 0 = return []
  | target <= 0 = []
  | target >  0 = do
      nexts@(part:_) <- tails parts
      rest <- partitions (target - part) nexts
      return (part : rest)

an =
  [ [length p] ++ p ++ [length q] ++ q
  | p <- partitions n [1..n]
  , q <- partitions n [1..n]
  , maximum p <= minimum q
  ]

s = unlines . map (unwords . map show) $ an

main = putStr s
