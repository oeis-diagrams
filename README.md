OEIS Diagrams
=============

Some diagrams of sequences from:

> The Online Encyclopedia of Integer Sequences <https://oeis.org>

a registered trademark of The OEIS Foundation, Inc.

The OEIS is licensed under CC-BY-NC:
<https://creativecommons.org/licenses/by-nc/3.0/>

This diagramming project is neither endorsed by or affiliated with OEIS.


Running the code
----------------

In the absence of a cabal file for the project, you can do this:

    cabal sandbox init
    cabal install diagrams diagrams-cairo
    # --allow-newer flag only needed on ghc-8.2.1 as of 2017-11-11
    cabal install diagrams-pgf --allow-newer
    # most of them use the SVG backend, only 2016 is PGF, so try this
    cabal exec -- runghc dir/file.hs -w 1000 -o output.svg
    # or even
    for h in */*.hs
    do
      cabal exec -- runghc "${h}" -w 1000 -o "${h}.svg"
    done
