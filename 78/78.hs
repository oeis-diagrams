import Control.Monad (forM_)
import Data.Char (chr)
import Data.Fixed (mod')

a000700 :: Integer -> [[Integer]]
a000700 n = go 1 n
  where
    go m n
      | n <  0 = []
      | n == 0 = [[]]
      | m >  n = []
      | otherwise = map (m :) (go (m + 2) (n - m)) ++ go (m + 2) n

pixel :: Integer -> [String]
pixel m = concat (replicate (fromIntegral m) $ rgb8 $ huesatval ((fromIntegral m * 17 / 48) `mod'` 1) (0.5 + 0.5 * ((fromIntegral m * 13 / 48) `mod'` 1)) (0.5 + 0.5 * ((fromIntegral m * 19 / 48) `mod'` 1)))

rgb8 :: (Double, Double, Double) -> [String]
rgb8 (r, g, b) = [c r, c g, c b]
  where c k = show (round (255 * k))

huesatval :: Double -> Double -> Double -> (Double, Double, Double)
huesatval h s v = (c 0, c 1, c 2)
  where c k = v * (1 + s * cos (2 * pi * (h + k / 3))) / (1 + s)

main :: IO ()
main = do
  putStr "P3\n47 78\n255\n"
  forM_ (a000700 47) $ putStrLn . unwords . concatMap pixel
