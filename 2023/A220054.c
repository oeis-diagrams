// A220054 Number A(n,k) of tilings of a k X n rectangle using right trominoes and 1 X 1 tiles
/*
Square array A(n,k) begins:
  1,  1,   1,     1,       1,        1,          1,            1, ...
  1,  1,   1,     1,       1,        1,          1,            1, ...
  1,  1,   5,    11,      33,       87,        241,          655, ...
  1,  1,  11,    39,     195,      849,       3895,        17511, ...
  1,  1,  33,   195,    2023,    16839,     151817,      1328849, ...
  1,  1,  87,   849,   16839,   249651,    4134881,     65564239, ...
  1,  1, 241,  3895,  151817,  4134881,  128938297,   3814023955, ...
  1,  1, 655, 17511, 1328849, 65564239, 3814023955, 207866584389, ...

A(4,4) = 2023

2023 = 36 *28 + 35 * 29
*/

#include <stdio.h>

#define N 4
#define K 4

const int tronimo[4][3][2] =
{{{0,0},{0,1},{1,0}}
,{{0,0},{0,1},{1,1}}
,{{0,0},{1,0},{1,-1}}
,{{0,0},{1,0},{1,1}}
};

int count;

void output(int g[N+2][K+2])
{
  int x = count % (35 + 36) * 2 + 1;
  int y = count / (35 + 36) * 2;
  if (x >= 71)
  {
    x -= 71;
    y += 1;
  }
  y *= 2;
  x *= K + 1;
  y *= N + 1;
  x += 1;
  y += 1;
  for (int j = 0; j < N; ++j)
  {
    for (int i = 0; i < K; ++ i)
    {
      if (g[j+1][i+1] == g[j+1][i+2])
      {
        printf("<path class='%c' d='M %d,%d L %d,%d' />\n", 'a' + (g[j+1][i+1]%5), x+2*i,y+2*j, x+2*i+2,y+2*j);
      }
      if (g[j+1][i+1] == g[j+2][i+1])
      {
        printf("<path class='%c' d='M %d,%d L %d,%d' />\n", 'a' + (g[j+1][i+1]%5), x+2*i,y+2*j, x+2*i,y+2*j+2);
      }
      if ((g[j+1][i+1]%5)==4)
      {
        printf("<circle cx='%d' cy='%d' r='0.5' />", x+2*i,y+2*j);
      }
    }
  }
  ++count;
}

void visit(int g[N+2][K+2], int x, int y, int n)
{
  if (y == N)
  {
    output(g);
  }
  else if (x == K)
  {
    visit(g, 0, y + 1, n);
  }
  else if (g[y+1][x+1])
  {
    visit(g, x + 1, y, n);
  }
  else
  {
    g[y+1][x+1] = 5 * n + 4;
    visit(g, x + 1, y, n + 1);
    g[y+1][x+1] = 0;
    for (int t = 0; t < 4; ++t)
    {
      int fits = 1;
      for (int v = 0; v < 3; ++v)
      {
        fits &= ! g[y+1 + tronimo[t][v][0]][x+1 + tronimo[t][v][1]];
      }
      if (fits)
      {
        for (int v = 0; v < 3; ++v)
        {
          g[y+1 + tronimo[t][v][0]][x+1 + tronimo[t][v][1]] = 5 * n + t;
        }
        visit(g, x+1, y, n+1);
        for (int v = 0; v < 3; ++v)
        {
          g[y+1 + tronimo[t][v][0]][x+1 + tronimo[t][v][1]] = 0;
        }
      }
    }
  }
}

int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  int g[N+2][K+2];
  for (int y = 0; y < N + 2; ++y)
  {
    for (int x = 0; x < K + 2; ++x)
    {
      g[y][x] = -(x==0 || x==K+1 || y==0 || y==N+1);
    }
  }
  count = 0;
  printf(
    "<svg width='%d' height='%d'>\n"
    "<style>\n"
    "path { stroke-width: 1; stroke-linecap: round; fill: none; }\n"
    ".a { stroke: #f80; }\n"
    ".b { stroke: #f08; }\n"
    ".c { stroke: #08f; }\n"
    ".d { stroke: #80f; }\n"
    "circle { fill: #4c4; }\n"
    "</style>\n"
    "<rect width='100%%' height='100%%' fill='white' stroke='none' />\n"
    "<g transform='translate(374,344) scale(14,14)'>\n"
//  , (36 * (K+1) - 1) * 2, (57 * (N + 1) - 1) * 2
    , 4 * 1440, 6 * 1440
  );
  visit(g, 0, 0, 1);
  printf("</g>\n</svg>\n");
  fprintf(stderr, "%d\n", count);
  return 0;
}
