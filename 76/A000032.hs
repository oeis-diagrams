-- https://oeis.org/A000032
-- 2, 1, 3, 4, 7, 11, 18, 29, 47, 76, 123, 199, ...
-- Lucas numbers beginning at 2: L(n) = L(n-1) + L(n-2), L(0) = 2, L(1) = 1.
-- Also the number of matchings in the n-cycle graph C_n for n >= 3. - Eric W. Weisstein, Oct 01 2017

import Control.Monad (replicateM)
import Data.Complex
import Data.List (groupBy, nub, scanl, sortBy, tails, zipWith3)
import Data.Monoid (mappend)
import Data.Ord (comparing)

r = 0.45
h = 1 / 9
d = 1.2

beta = 0

warp z = (beta - 1) * z / ((2 * beta) * z + (beta - 1))

n :: Int
n = 9

o :: Int
o = 4

p = head [ j | j <- [0..n], mod (j * o) n == 1 ]

equating f a b = f a == f b

chunk n xs = take n xs : chunk n (drop n xs)

rep xs = minimum . vars $ xs
vars xs = take n . map (take n . head) . chunk p . tails . cycle $ xs
len xs = length . filter id $ xs

graphsss= groupBy (equating (len . head))
        . map (nub . vars . head)
        . groupBy (equating rep)
        . sortBy (comparing len `mappend` comparing rep) . filter valid . replicateM n $ [False, True]

valid (x:xs) = not (or (zipWith (&&) (x:xs) (xs ++ [x])))

scales w = [ w / 9 | g <- graphsss ]
heights w = [ w / 9 * (d * fromIntegral (length g) + 0.5) | g <- graphsss ]

drawGraphsss w = concat $ zipWith3 drawGraphss graphsss (scales w) (scanl (+) (w / 9 / 2) (heights w))
drawGraphss graphss scale y = concat $ zipWith3 (drawGraphs scale) (repeat (0.5*scale)) [y + 0.5 * scale, y + 0.5 * scale + d * scale ..] graphss
drawGraphs scale x0 y0 graphs = "<g transform='translate(" ++ show x0 ++ "," ++ show y0 ++ ")'>\n" ++ (concat $ zipWith3 (drawGraph (scale)) xs ys graphs) ++ "</g>\n"
  where
   k = 9
   h | length graphs == 3 = 1
     | otherwise = 0
   xs = [ 0.5 + i + fromIntegral (9 - length graphs) / 2 + (if length graphs == 3 then 3 * (i - 1) else 0) {- (-0.7 * cos (2 * pi * fromIntegral (i - h) / fromIntegral k))-} | i <- [0..k-1] ]
   ys = [ 0 {-(-0.7 * sin (2 * pi * fromIntegral (i - h) / fromIntegral k))-} | i <- [0..k-1] ]

drawGraph scale x0 y0 es = unlines $ map (uncurry $ drawEdge scale x0 y0) exys
  where
   exys = filter (not . fst) (zip es [0..]) ++ filter fst (zip es [0..])

drawEdge scale x0 y0 e i = "<path class='" ++ c ++ "' d='M " ++ s xi ++ "," ++ s yi ++ " L " ++ s xj ++ "," ++ s yj ++ "' />"
  where
   c = if e then "t" else "f"
   ti = 2 * pi * (fromIntegral (o * i) + h) / fromIntegral n
   tj = 2 * pi * (fromIntegral (o * (i + 1)) + h) / fromIntegral n
   ci = x0 + r * cos ti
   si = y0 - r * sin ti
   cj = x0 + r * cos tj
   sj = y0 - r * sin tj
   xi:+yi = warp (ci :+ si)
   xj:+yj = warp (cj :+ sj)
   s x = show (round (scale * x))


svg = "<svg width='" ++ s (4 * dpi) ++ "' height='" ++ s (6 * dpi) ++ "'>\n" ++
      "<style>\n.f { stroke: black; stroke-dasharray: " ++ show (round $ 0.5 * dpi / 72) ++ ", " ++ show (round $ 3 * 0.5 * dpi / 72) ++ "; }\n.t { stroke: red; }\n</style>\n" ++
      "<rect width='100%' height='100%' stroke='none' fill='white' />\n" ++
      "<g stroke-width='" ++ show (round $ 0.5 * dpi / 72) ++ "' stroke-linecap='round'>\n" ++ drawGraphsss w ++ "</g></svg>"
  where
   s x = show (round x)
   w = 4 * dpi * 9 / 10
   dpi = 1440

main = putStrLn svg
