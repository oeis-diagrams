// A099032 Number of different patterns of n-motifs.
/*
Equivalence classes of $Z_{12} \times Z_{12}$
under invertible affine transformations.

1, 1, 5, 26, 216, 2024, ...

a(5) = 2024 patterns of 5-motifs
a(3) = 26 patterns of 3-motifs depicted
*/

#include <stdio.h>

int main(int argc, char ** argv)
{
  (void) argc;
  (void) argv;
  const int yoff[] = { 11, 5, 4, 2, 9, 0, 8 };
  printf(
    "<svg width='3375' height='2625'>\n"
    "<style>\n"
    "rect { fill: white; stroke: none; }\n"
    ".grid circle { fill: black: stroke; none; }\n"
    ".motif circle { fill: red; stroke: black; stroke-width: 0.1; }\n"
    ".motif path { fill: none; stroke: black; stroke-width: 0.1; }\n"
    "</style>\n"
    "<rect width='100%%' height='100%%' />"
    "<g transform='translate(225, 225) scale(70.5555555555556, 70.5555555555556) translate(1.5, 1.5)'>"
  );
  printf("<g class='grid'>");
  for (int j = -1; j <= 37; ++j)
  for (int i = -1; i <= 32; ++i)
    printf("<circle cy='%d' cx='%d' r='0.05' />\n", i, j);
  printf("</g>\n");
  int column = 0;
  int h = 0;
  int x = 0, y = -1;
  for (int z = 0; z < 26; ++z)
  {
    int mi[3];
    scanf("%d %d %d", &mi[1], &mi[0], &mi[2]);
    int transpose = mi[1] == 6 || mi[1] == 9;
    if (transpose)
    {
      int t = mi[0]; mi[0] = mi[1]; mi[1] = t;
    }
    if (transpose || mi[1] > h)
    {
      x = yoff[column++];
      y += h + 2;
      h = 0;
    }
    h = mi[1] > h ? mi[1] : h;
    int ii[3], jj[3];
    for (int k = 0; k < 3; ++k)
    {
      if (transpose)
        scanf("%d %d", &jj[k], &ii[k]);
      else
        scanf("%d %d", &ii[k], &jj[k]);
    }
    printf("<g class='motif'>\n");
    for (int k = 0; k < 3; ++k)
    {
      printf("<path d='M %d,%d L %d,%d' />\n", y + jj[(k + 1) % 3], x + ii[(k + 1) % 3], y + jj[k], x + ii[k]);
    }
    for (int k = 0; k < 3; ++k)
    {
      printf("<circle cy='%d' cx='%d' r='0.4' />\n", x + ii[k], y + jj[k]);
    }
    printf("</g>\n");
    x += mi[0] + 2;
  }
  printf("</g>\n");
  printf("</svg>\n");
  return 0;
}
