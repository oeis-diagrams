y=2024
for m in $(seq 1 12)
do
  svg="$(printf "%02d.svg" $m)"
  png="$(printf "%02d.png" $m)"
  n="$(ncal -M -b -y $y -m $m -1 | grep -v "                      " | tail -n +3 | wc -l)"
  ncal -M -b -y $y -m $m -1 | grep -v "                      " |
  ( read month y
    read line
    echo "<svg width='3375' height='2625'>"
    echo "<rect width='100%' height='100%' fill='white' stroke='none' />"
    echo "<text font-family='LMSans10' font-weight='bold' font-size='225' x='225' y='420'>${month}</text>"
    dy=0
    y1=1260
    if (( "$n" < 6 ))
    then
      dy=$((dy + 120))
      y1=$((y1 - 240))
    fi
    if (( "$n" < 5 ))
    then
      dy=$((dy + 120))
      y1=$((y1 - 240))
    fi
    echo "<g font-family='LMSans10' text-anchor='end' font-size='75' transform='translate(645,$((742 + dy))'>"
    echo "<path d='M1600,-120 L1600,${y1}' fill='none' stroke='black' stroke-width='3' />"
    cat -n |
    sed "s|^ *\(.\).\(..\) \(..\) \(..\) \(..\) \(..\) \(..\) \(..\)  $|\1 \2x \3x \4x \5x \6x \7x \8x|g" |
    while read j days
    do
      echo "<g transform='translate(0,$(( 240 * (j - 1) )))'>"
      i=0
      for day in ${days}
      do
        if (( "$i" < 5 ))
        then
          bold=""
        else
          bold=" fill='none' stroke='black' stroke-width='3' font-weight='bold'"
        fi
        if [ "$day" != "x" ]
        then
          echo "<text x='$(( 360 * i ))'${bold}>${day%x}</text>"
          date="$(printf "%04d-%02d-%02d" "$y" "$m" "${day%x}")"
          cat special.txt |
          grep "^${date}" |
          cat -n |
          while read k date time text
          do
            echo "<text x='$(( 360 * i ))' y='$((k * 60))' font-size='40'>${text}</text>"
          done
        fi
        i=$((i + 1))
      done
      echo "</g>"
    done
    echo "</g>"
    tac captions.txt |
    grep "^$(printf "%02d" "$m")" |
    cat -n |
    while read j junk1 junk2 text
    do
      echo "<text x='225' y='$(( 2400 - 75 * (j - 1) ))' font-family='LMSans10' font-size='40'>${text}</text>"
    done
    echo "</svg>"
  ) > "${svg}"
  rsvg-convert --width 3375 --height 2625 --dpi-x=300 --dpi-y=300 --keep-aspect-ratio "${svg}" > "${png}"
done
