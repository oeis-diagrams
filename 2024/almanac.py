year = 2024

from skyfield import api

ts = api.load.timescale()
eph = api.load('de421.bsp')
from skyfield import almanac

t0 = ts.utc(year, 1, 1)
t1 = ts.utc(year, 12, 31)
t, y = almanac.find_discrete(t0, t1, almanac.seasons(eph))
for yi, ti in zip(y, t):
    print(ti.utc_iso(' '), almanac.SEASON_EVENTS[yi])

t, y = almanac.find_discrete(t0, t1, almanac.moon_phases(eph))
for yi, ti in zip (y, t):
    print(ti.utc_iso(' '), almanac.MOON_PHASES[yi])

from skyfield import eclipselib
t, y, details = eclipselib.lunar_eclipses(t0, t1, eph)
for yi, ti in zip (y, t):
    print(ti.utc_iso(' '), eclipselib.LUNAR_ECLIPSES[yi], "Lunar Eclipse")
