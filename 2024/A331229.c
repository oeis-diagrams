// A331229 a(n) = number of triangles with integer sides i <= j <= k with radius of circumcircle <= n.
/*
1,7,22,47,91,148,231,334,469,631,830,1062,1339,1657,2024,2434,2905,3427,4014,4653,5362,6141,6994,7911,8917,10000,11169,12425,13774,...

a(15) = 2024
a(4) = 47 depicted
*/

#include <complex.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define R 4
#define LIMIT 47
#define pi 3.14159265358979

double radius(double a, double b, double c)
{
  return a*b*c / sqrt((a+b+c)*(a+b-c)*(a-b+c)*(-a+b+c));
}

struct output
{
  double r;
  int i, j, k;
};

int cmp_r(const void *a, const void *b)
{
  const struct output *p = a, *q = b;
  double x = p->r, y = q->r;
  return (x > y) - (x < y);
}

struct output buffer[LIMIT];

int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  srand(331229+6);
  printf(
    "<svg width='3375' height='2625'>\n"
    "<style>\n"
    "path { stroke: black; fill: red; stroke-width: 0.1; }\n"
    "circle { stroke: none; fill: black; }\n"
    "circle.c { stroke: black; stroke-width: 0.1; fill: none; }\n"
    "</style>\n"
    "<rect width='100%%' height='100%%' fill='white' stroke='none' />\n"
    "<g transform='translate(237.5,225) scale(40.2777777777778,40.2777777777778)'>"
  );
  int count = 0;
  double r;
  for (int i = 1; i <= 2 * R; ++i)
  for (int j = i; j <= 2 * R; ++j)
  for (int k = j; k <= 2 * R; ++k)
  if ((r = radius(i, j, k)) <= R)
  {
    struct output out = { r, i, j, k };
    buffer[count++] = out;
  }
  qsort(buffer, count, sizeof(*buffer), cmp_r);
  for (int ix = 0; ix < count; ++ix)
  {
    int i = buffer[ix].i;
    int j = buffer[ix].j;
    int k = buffer[ix].k;
    int x = ix % 8;
    int y = ix / 8;
    double theta = acos((i * i + k * k - j * j) / (2.0 * i * k));
    double _Complex a = 0, b = I * k, c = I * i * (cos(theta) + I * sin(theta));
    double _Complex w = (c - a) / (b - a);
    double _Complex o = (b - a) * (w - cabs(w*w))/(2*I*cimag(w)) + a;
    double r = cabs(a - o);
    double _Complex d = (2 * R + 1) * ((x + 0.5) + I * (y + 0.5)) - o;
    double _Complex phi = cexp(2 * pi * I * rand() / RAND_MAX);
    a = (a - o) * phi + o;
    b = (b - o) * phi + o;
    c = (c - o) * phi + o;
    printf("<circle class='c' cx='%f' cy='%f' r='%f' />\n"
    , creal(o + d), cimag(o + d), r
    );
    printf("<circle class='c' cx='%f' cy='%f' r='%f' />\n"
    , creal(o + d), cimag(o + d), (double) R
    );
    printf("<path d='M %f,%f L %f,%f %f,%f %f,%f Z' />\n"
    , creal(a + d), cimag(a + d)
    , creal(b + d), cimag(b + d)
    , creal(c + d), cimag(c + d)
    , creal(a + d), cimag(a + d)
    );
    for (int t = 0; t < k; ++t)
    {
      double _Complex z = a + (b - a) * t / k + d;
      printf("<circle cx='%f' cy='%f' r='0.2' />\n", creal(z), cimag(z));
    }
    for (int t = 0; t < j; ++t)
    {
      double _Complex z = b + (c - b) * t / j + d;
      printf("<circle cx='%f' cy='%f' r='0.2' />\n", creal(z), cimag(z));
    }
    for (int t = 0; t < i; ++t)
    {
      double _Complex z = c + (a - c) * t / i + d;
      printf("<circle cx='%f' cy='%f' r='0.2' />\n", creal(z), cimag(z));
    }
  }
  printf(
    "</g>\n"
    "</svg>\n"
  );
  fprintf(stderr, "%d\n", count);
  return 0;
}
