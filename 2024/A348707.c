// A348707 Number of tree polyominoes with width 4 and height n.
/*
1, 22, 243, 2024, 14981, 104946

cf A348663 Number of tree polyominoes with width 3 and height n.
1, 10, 55, 243, 980, 3789,

T(n,k) = Number of tree polyominoes with width k and height n.

T(4,4) = 2024
T(3,3) = 55 depicted
*/

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#define N 3
#define K 3

bool width(bool g[N][K])
{
  bool q = true;
  for (int x = 0; x < K; ++x)
  {
    bool p = false;
    for (int y = 0; y < N; ++y)
    {
      p |= g[y][x];
    }
    q &= p;
  }
  return q;
}

bool height(bool g[N][K])
{
  bool q = true;
  for (int y = 0; y < N; ++y)
  {
    bool p = false;
    for (int x = 0; x < K; ++x)
    {
      p |= g[y][x];
    }
    q &= p;
  }
  return q;
}

int popcount(bool g[N][K])
{
  int m = 0;
  for (int y = 0; y < N; ++y)
  {
    for (int x = 0; x < K; ++x)
    {
      if (g[y][x])
      {
        ++m;
      }
    }
  }
  return m;
}

int edgecount(bool g[N][K])
{
  int m = 0;
  for (int y = 0; y < N; ++y)
  {
    for (int x = 0; x < K - 1; ++x)
    {
      if (g[y][x] && g[y][x + 1])
      {
        ++m;
      }
    }
  }
  for (int y = 0; y < N - 1; ++y)
  {
    for (int x = 0; x < K; ++x)
    {
      if (g[y][x] && g[y + 1][x])
      {
        ++m;
      }
    }
  }
  return m;
}

void visit(bool g[N][K], int y, int x)
{
  if (0 <= y && y < N && 0 <= x && x < K)
  {
    if (g[y][x])
    {
      g[y][x] = false;
      visit(g, y - 1, x);
      visit(g, y, x - 1);
      visit(g, y, x + 1);
      visit(g, y + 1, x);
    }
  }
}

bool connected(bool g[N][K])
{
  bool h[N][K];
  for (int y = 0; y < N; ++y)
  {
    for (int x = 0; x < K; ++x)
    {
      h[y][x] = g[y][x];
    }
  }
  for (int y = 0; y < N; ++y)
  {
    for (int x = 0; x < K; ++x)
    {
      if (h[y][x])
      {
        visit(h, y, x);
        return popcount(h) == 0;
      }
    }
  }
  return true;
}

bool tree(bool g[N][K])
{
  return edgecount(g) + 1 == popcount(g);
}

bool valid(bool g[N][K])
{
  return width(g) && height(g) && tree(g) && connected(g);
}

int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  printf(
    "<svg width='3375' height='2625'>\n"
    "<rect width='100%%' height='100%%' fill='white' stroke='none' />\n"
    "<g transform='translate(435.227272727273,225) scale(65.9090909090909,65.9090909090909)' fill='red' stroke='black' stroke-width='0.1'>\n"
  );
  int count = 0;
  uint64_t total = (uint64_t) 1 << (N * K);
  for (uint64_t i = 0; i < total; ++i)
  {
    bool g[N][K];
    uint64_t m = 1;
    for (int y = 0; y < N; ++y)
    {
      for (int x = 0; x < K; ++x)
      {
        g[y][x] = i & m;
        m <<= 1;
      }
    }
    if (valid(g))
    {
      int yy = (count / 8) * (N + 2);
      int xx = (count % 8) * (K + 2);
      for (int y = 0; y < N; ++y)
      {
        for (int x = 0; x < K; ++x)
        {
          if (g[y][x])
          {
            printf("<rect x='%d' y='%d' width='1' height='1' />\n", xx + x, yy + y);
          }
        }
      }
      ++count;
    }
  }
  printf("</g>\n</svg>\n");
  fprintf(stderr, "%d\n", count);
  return 0;
}
