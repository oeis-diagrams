// A188148 Number of 3-step self-avoiding walks on an n X n square
/*
summed over all starting positions. 		1
0, 8, 44, 104, 188, 296, 428, 584, 764, 968, 1196, 1448, 1724, 2024, 2348, 2696, 3068

a(14) = 2024
a(3) = 44 depicted

44 = 4 * 5 + 4 * 6
*/

#include <stdio.h>

#define N 3

int count;

void output(int g[N+2][N+2])
{
  int v = count % (5 + 6) * 2 + 1;
  int u = count / (5 + 6) * 2;
  if (v >= 11)
  {
    v -= 11;
    u += 1;
  }
  u *= 2 * N + 1;
  v *= 2 * N + 1;
  for (int y = 0; y < N; ++y)
  for (int x = 0; x < N; ++x)
  {
    double r = (g[y+1][x+1] + 1.0) / (N + 1.0);
    printf("<circle class='%c' cy='%d' cx='%d' r='%f' />\n", g[y+1][x+1] ? 'r' : 'b', u + 2 * x + 2, v + 2 * y + 2, r);
  }
  ++count;
}

void visit(int g[N+2][N+2], int x, int y, int n)
{
  if (! g[x+1][y+1])
  {
    g[x+1][y+1] = n;
    if (n >= N)
    {
      output(g);
    }
    else
    {
      visit(g, x, y - 1, n + 1);
      visit(g, x - 1, y, n + 1);
      visit(g, x + 1, y, n + 1);
      visit(g, x, y + 1, n + 1);
    }
    g[x+1][y+1] = 0;
  }
}

int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  count = 0;
  int g[N+2][N+2];
  for (int y = 0; y < N + 2; ++y)
  for (int x = 0; x < N + 2; ++x)
    g[y][x] = -(x == 0 || x == N + 1 || y == 0 || y == N + 1);
  printf(
    "<svg width='3375' height='2625'>\n"
    "<style>\n"
    "circle.r { fill: red; stroke: black; stroke-width: 0.1; }\n"
    "circle.b { fill: black; stroke: none; }\n"
    "</style>\n"
    "<rect width='100%%' height='100%%' fill='white' stroke='none' />\n"
    "<g transform='translate(199.342105263158,225) scale(38.1578947368421,38.1578947368421)'>\n"
  );
  for (int y = 0; y < N; ++y)
  for (int x = 0; x < N; ++x)
    visit(g, x, y, 1);
  printf(
    "</g>\n"
    "</svg>\n"
  );
  fprintf(stderr, "%d\n", count);
  return 0;
}
