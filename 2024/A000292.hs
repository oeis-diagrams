-- A000292 Tetrahedral numbers
{-
0, 1, 4, 10, 20, 35, 56, 84, 120, 165, 220, 286, 364, 455, ...

a(n+1) = number of 4-vectors over [0...n] summing to n

a(22) = 2024
a(6) = 56 depicted

4 d22 summing to 25 gives 2024 possibilities
4 d6 summing to 9 gives 56 possibilities depicted
-}

import Control.Monad (replicateM)

a n = filter ((n ==) . sum) . replicateM 4 $ [0..n]

draw [y, x] xs
  = concat (zipWith dice (sequence[[-2.5,2.5],[-2.5,2.5]]) xs)
  where
    dice [j,i] b
      = "<use href='#" ++ [['a'..]!!b] ++
        "' x='" ++ show (fromIntegral (12 * x + 6) + i) ++
        "' y='" ++ show (fromIntegral (12 * y + 6) + j) ++
        "' />\n"

svg =
    "<svg width='3375' height='2625'>\n" ++
    "<defs>\n" ++
    "<style>\n" ++
    "g rect { fill: white; stroke: black; stroke-width: 0.1; stroke-linejoin: round; }\n" ++
    "g circle { fill: red; stroke: black; stroke-width: 0.1; }\n" ++
    "</style>\n" ++
    "<g id='a'>\n" ++
    "<rect x='-2' y='-2' width='4' height='4' />\n" ++
    "<circle cx='0' cy='0' r='0.4' />\n" ++
    "</g>\n" ++
    "<g id='b'>\n" ++
    "<rect x='-2' y='-2' width='4' height='4' />\n" ++
    "<circle cx='1' cy='-1' r='0.4' />\n" ++
    "<circle cx='-1' cy='1' r='0.4' />\n" ++
    "</g>\n" ++
    "<g id='c'>\n" ++
    "<rect x='-2' y='-2' width='4' height='4' />\n" ++
    "<circle cx='1' cy='-1' r='0.4' />\n" ++
    "<circle cx='0' cy='0' r='0.4' />\n" ++
    "<circle cx='-1' cy='1' r='0.4' />\n" ++
    "</g>\n" ++
    "<g id='d'>\n" ++
    "<rect x='-2' y='-2' width='4' height='4' />\n" ++
    "<circle cx='1' cy='-1' r='0.4' />\n" ++
    "<circle cx='1' cy='1' r='0.4' />\n" ++
    "<circle cx='-1' cy='-1' r='0.4' />\n" ++
    "<circle cx='-1' cy='1' r='0.4' />\n" ++
    "</g>\n" ++
    "<g id='e'>\n" ++
    "<rect x='-2' y='-2' width='4' height='4' />\n" ++
    "<circle cx='1' cy='-1' r='0.4' />\n" ++
    "<circle cx='1' cy='1' r='0.4' />\n" ++
    "<circle cx='0' cy='0' r='0.4' />\n" ++
    "<circle cx='-1' cy='-1' r='0.4' />\n" ++
    "<circle cx='-1' cy='1' r='0.4' />\n" ++
    "</g>\n" ++
    "<g id='f'>\n" ++
    "<rect x='-2' y='-2' width='4' height='4' />\n" ++
    "<circle cx='1' cy='-1' r='0.4' />\n" ++
    "<circle cx='1' cy='1' r='0.4' />\n" ++
    "<circle cx='1' cy='0' r='0.4' />\n" ++
    "<circle cx='-1' cy='0' r='0.4' />\n" ++
    "<circle cx='-1' cy='-1' r='0.4' />\n" ++
    "<circle cx='-1' cy='1' r='0.4' />\n" ++
    "</g>\n" ++
    "</defs>\n" ++
    "<rect width='100%' height='100%' fill='white' stroke='none' />" ++
    "<g transform='translate(444.642857142857,225) scale(25.8928571428571,25.8928571428571)'>\n" ++
    concat (zipWith draw (sequence [[0..6],[0..7]]) (a 5)) ++
    "</g>\n" ++
    "</svg>\n"

main = putStr svg
