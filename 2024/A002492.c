// A002492 bishop moves on a chessboard
/*
n+1 X n+1 chessboard, total number, if the bishop is placed anywhere.
E.g., on a 3 X 3-Board: bishop has 8 X 2 moves and 1 X 4 moves, so a(2)=20.

0, 4, 20, 56, 120, 220, 364, 560, 816, 1140, 1540, 2024, 2600, 3276, 4060,

a(11) = 2024 depicted
*/

#include <stdio.h>

#define N 11

int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  printf(
    "<svg width='3375' height='2625'>\n"
    "<style>\n"
    ".b { stroke: black; fill: none; }\n"
    ".w { stroke: red; fill: none; }\n"
    "path { stroke-width: 0.01; }\n"
    "rect { stroke-width: 0.1; }\n"
    "</style>\n"
    "<rect width='100%%' height='100%%' fill='white' stroke='none' />\n"
    "<g transform='translate(600,225) scale(90.625,90.625)'>\n"
    "<g transform='translate(%d,%d) scale(%f,%f) rotate(45) translate(%d,%d)'>\n"
  , N + 1, N + 1, 1.4142135623731, 1.4142135623731, -N, -N
  );
  int moves[N][N][2];
  for (int k = 0; k < N; ++k)
  for (int j = 0; j < N; ++j)
  for (int i = 0; i < 2; ++i)
    moves[k][j][i] = 0;
  for (int k = 0; k < N + 1; ++k)
  for (int j = 0; j < N + 1; ++j)
  for (int d = 1; d <= N + 1; ++d)
  {
    if (0 <= k - d && k - d < N && 0 <= j - d && j - d < N) moves[k-d][j-d][0]++;
    if (0 <= k - d && k - d < N && 0 <= j + d - 1 && j + d - 1 < N) moves[k-d][j+d-1][1]++;
    if (0 <= k + d - 1 && k + d - 1 < N && 0 <= j - d && j - d < N) moves[k+d-1][j-d][1]++;
    if (0 <= k + d - 1 && k + d - 1 < N && 0 <= j + d - 1 && j + d - 1 < N) moves[k+d-1][j+d-1][0]++;
  }
  int m = 0, t = 0;
  for (int k = 0; k < N; ++k)
  for (int j = 0; j < N; ++j)
  for (int i = 0; i < 2; ++i)
  {
    t += moves[k][j][i];
    m = moves[k][j][i] > m ? moves[k][j][i] : m;
  }
  fprintf(stderr, "%d %d\n", t, m);
  for (int k = 0; k < N; ++k)
  for (int j = 0; j < N; ++j)
  {
    int x = k + j;
    int y = k - j + N - 1;
    char c = (k & 1) == (j & 1) ? 'b' : 'w';
    printf("<g class='%c'>\n", c);
    for (int i = 0; i < moves[k][j][0]; ++i)
    {
      double z = ((i + 0.5) / moves[k][j][0] - 0.5) * moves[k][j][0] / (double) m + 1;
      printf("<path d='M %d,%f L %d,%f' />\n", x, y + z, x + 2, y + z);
    }
    printf("</g>\n");
    c = (k & 1) != (j & 1) ? 'b' : 'w';
    printf("<g class='%c'>\n", c);
    for (int i = 0; i < moves[k][j][1]; ++i)
    {
      double z = ((i + 0.5) / moves[k][j][1] - 0.5) * moves[k][j][1] / (double) m + 1;
      printf("<path d='M %f,%d L %f,%d' />\n", x + z, y, x + z, y + 2);
    }
    printf("</g>\n");
  }
  printf("</g>\n");
  for (int j = 0; j < N + 1; ++j)
  for (int i = 0; i < N + 1; ++i)
  {
    char c = (j & 1) == (i & 1) ? 'b' : 'w';
    printf("<rect class='%c' x='%d.1' y='%d.1' width='1.8' height='1.8' />", c, i * 2, j * 2);
  }
  printf(
    "</g>\n"
    "</svg>\n"
  );
  return 0;
}
