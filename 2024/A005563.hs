-- A005563 Toads and frogs puzzle
{-
number of moves that it takes n frogs to swap places with n toads
on a strip of 2n + 1 squares (or positions, or lily pads)
where a move is a single slide or jump

0, 3, 8, 15, 24, 35, 48, 63, 80, 99, 120, 143, 168, 195, 224, 255, 288, 323, 360, 399, 440, 483, 528, 575, 624, 675, 728, 783, 840, 899, 960, 1023, 1088, 1155, 1224, 1295, 1368, 1443, 1520, 1599, 1680, 1763, 1848, 1935, 2024, 2115, 2208, 2303, 2400, 2499, 2600

a(44) = 2024
a(4) = 24 depicted
-}

import Data.List (elemIndex)
import qualified Data.Map as Map
import Data.Maybe (fromJust)

second :: (b -> c) -> (a, b) -> (a, c)
second f (a, b) = (a, f b)

start :: Int -> [Maybe Bool]
start n = replicate n (Just True) ++ [Nothing] ++ replicate n (Just False)

target :: Int -> [Maybe Bool]
target n = replicate n (Just False) ++ [Nothing] ++ replicate n (Just True)

toI :: [Maybe Bool] -> Integer
toI [] = 0
toI (Just False:xs) = 3 * toI xs + 0
toI (Nothing   :xs) = 3 * toI xs + 1
toI (Just True :xs) = 3 * toI xs + 2


moves :: ([Int], [Maybe Bool]) -> [([Int], [Maybe Bool])]

moves (ys, a:b:Nothing:c:d:xs) =
  [ (-2:ys, Nothing:b:a:c:d:xs)
  , (-1:ys, a:Nothing:b:c:d:xs)
  , ( 1:ys, a:b:c:Nothing:d:xs)
  , ( 2:ys, a:b:d:c:Nothing:xs)
  ]

moves (ys, a:b:Nothing:c:xs) =
  [ (-2:ys, Nothing:b:a:c:xs)
  , (-1:ys, a:Nothing:b:c:xs)
  , ( 1:ys, a:b:c:Nothing:xs)
  ]

moves (ys, a:b:Nothing:xs) =
  [ (-2:ys, Nothing:b:a:xs)
  , (-1:ys, a:Nothing:b:xs)
  ]

moves (ys, a:Nothing:c:d:xs) =
  [ (-1:ys, Nothing:a:c:d:xs)
  , ( 1:ys, a:c:Nothing:d:xs)
  , ( 2:ys, a:d:c:Nothing:xs)
  ]

moves (ys, a:Nothing:b:xs) =
  [ (-1:ys, Nothing:a:b:xs)
  , ( 1:ys, a:b:Nothing:xs)
  ]

moves (ys, a:Nothing:xs) =
  [ (-1:ys, Nothing:a:xs)
  ]

moves (ys, Nothing:c:d:xs) =
  [ ( 1:ys, c:Nothing:d:xs)
  , ( 2:ys, d:c:Nothing:xs)
  ]
moves (ys, Nothing:b:xs) =
  [ ( 1:ys, b:Nothing:xs)
  ]

moves (ys, x:xs) = fmap (second (x:)) (moves (ys, xs))

moves (ys, []) = [(ys, [])]


type Graph = Map.Map Integer [([Int], Integer)]

graph :: [Maybe Bool] -> [Maybe Bool] -> [Int]
graph y x = go Map.empty [([], x)]
  where
    go :: Graph -> [([Int], [Maybe Bool])] -> [Int]
    go m ins = case [ path | (path, z) <- ins, z == y ] of
      p:ps -> p
      [] ->
          let nexts = filter new (concatMap (\i -> (fmap ((,) i) (moves i))) ins)
              new ((_, from), (_, _)) = not (Map.member (toI from) m)
              update ((_, from), (path, to)) m' = Map.insertWith (++) (toI from) [(path, toI to)] m'
          in  if null nexts then [] else go (foldr update m nexts) (map snd nexts)

move :: Int -> [Maybe a] -> [Maybe a]
move (-2) (a:b:Nothing:xs) = Nothing:b:a:xs
move (-1) (a:Nothing:xs) = Nothing:a:xs
move   1  (Nothing:a:xs) = a:Nothing:xs
move   2  (Nothing:a:b:xs) = b:a:Nothing:xs
move   n  (x:xs) = x : move n xs

main :: IO ()
main = putStr svg

svg :: String
svg
  = "<svg width='3375' height='2625'>\n" ++
    "<defs>\n" ++
    "<circle id='f' cx='0' cy='0' r='0.4' fill='white' stroke='black' stroke-width='0.1' />\n" ++
    "<circle id='t' cx='0' cy='0' r='0.4' fill='red'   stroke='black' stroke-width='0.1' />\n" ++
    "<style>\n" ++
    ".w { stroke: white; stroke-width: 0.3; fill: none; }\n" ++
    ".f { stroke: black; stroke-width: 0.1; fill: none; }\n" ++
    ".t { stroke: black; stroke-width: 0.1; fill: none; }\n" ++
    "</style>\n" ++
    "</defs>\n" ++
    "<rect width='100%' height='100%' fill='white' stroke='none' />\n" ++
    "<g transform='translate(187.5,750) scale(60,60)'>\n" ++
    concatMap (p False (==)) [0 .. 2 * n - 1] ++
    concatMap (p True  (/=)) [0 .. 2 * n - 1] ++
    concatMap (p False (/=)) [0 .. 2 * n - 1] ++
    concatMap a (amphibians `zip` [0..]) ++
    "</g>\n" ++
    "</svg>\n"
  where
    amphibians = foldr (\n (x:xs) -> let y = move n x in y:x:xs) [map Just [0..n-1] ++ [Nothing] ++ map Just [n..2*n-1]] path
    path = graph (target n) (start n)
    n = 4
    p :: Bool -> (Int -> Int -> Bool) -> Int -> String
    p w f k = let (x0, y0):xys = [0..] `zip` map (fromJust . elemIndex (Just k)) amphibians
                  seg (s, (x,y)) (u,v) = (s ++ (if f y v then "L " else "M ") ++ show (2 * u + 1) ++ "," ++ show (2 * v + 1), (u, v))
              in  fst (foldl seg ("<path class='" ++ (if w then "w" else if k < n then "f" else "t") ++ "' d='M " ++ show (2 * x0 + 1) ++ "," ++ show (2 * y0 + 1), (x0, y0)) xys) ++ "' />\n"
    a :: ([Maybe Int], Int) -> String
    a (ams, x) = concatMap (b x) (zip ams [0..])
    b :: Int -> (Maybe Int, Int) -> String
    b x (Nothing, y) = ""
    b x (Just k, y) = "<use href='#" ++ (if k < n then "f" else "t") ++ "' x='" ++ show (2 * x + 1) ++ "' y='" ++ show (2 * y + 1) ++ "' />\n"
