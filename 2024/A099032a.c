// A099032 Number of different patterns of n-motifs.
/*
Equivalence classes of $Z_{12} \times Z_{12}$
under invertible affine transformations.

1, 1, 5, 26, 216, 2024, ...

a(5) = 2024 patterns of 5-motifs
a(3) = 26 patterns of 3-motifs depicted
*/

#include <stdint.h>
#include <stdio.h>

#define K 3
#define LIMIT 3333

static inline int atomic_inc(volatile int *p)
{
  int q;
  #pragma omp atomic capture
  q = (*p)++;
  return q;
}

static inline signed char is_canonical(const short *A, int nA, short source[K][2])
{
  signed char source_map[144];
  for (int i = 0; i < 144; ++i)
  {
    source_map[i] = 0;
  }
  for (int i = 0; i < K; ++i)
  {
    (source_map[source[i][0] + 12 * source[i][1]])++;
  }
  for (int k = 0; k < nA; ++k)
  {
    short a = A[4 * k + 0];
    short b = A[4 * k + 1];
    short d = A[4 * k + 2];
    short e = A[4 * k + 3];
    for (short c = 0; c < 12; ++c)
    for (short f = 0; f < 12; ++f)
    {
      short target[K][2];
      for (int i = 0; i < K; ++i)
      {
        target[i][0] = (a * source[i][0] + b * source[i][1] + c) % 12;
        target[i][1] = (d * source[i][0] + e * source[i][1] + f) % 12;
      }
      signed char target_map[144];
      for (int i = 0; i < 144; ++i)
      {
        target_map[i] = 0;
      }
      for (int i = 0; i < K; ++i)
      {
        (target_map[target[i][0] + 12 * target[i][1]])++;
      }
      signed char compare = 0;
      for (int i = 0; i < 144 && compare == 0; ++i)
      {
        compare = target_map[i] - source_map[i];
      }
      if (compare > 0)
      {
        return 0;
      }
    }
  }
  return 1;
}

static inline void A099032_1(volatile int *counter, signed char *output, short *A, int nA, short k[K])
{
  for (int i = 0; i < K - 1; ++i)
  {
    if (! (k[i] < k[i + 1]))
    {
      return;
    }
  }
  short motif[K][2];
  for (int i = 0; i < K; ++i)
  {
    motif[i][0] = k[i] % 12;
    motif[i][1] = k[i] / 12;
  }
  if (is_canonical(A, nA, motif))
  {
    short out[K][2], mi[3] = { 12, 12, 144 };
    for (int i = 0; i < nA; ++i)
    {
      short a = A[4 * i + 0];
      short b = A[4 * i + 1];
      short d = A[4 * i + 2];
      short e = A[4 * i + 3];
      for (short c = 0; c < 12; ++c)
      for (short f = 0; f < 12; ++f)
      {
        short target[K][2];
        short ma[2] = { 0, 0 };
        for (int j = 0; j < K; ++j)
        {
          target[j][0] = (a * motif[j][0] + b * motif[j][1] + c) % 12;
          target[j][1] = (d * motif[j][0] + e * motif[j][1] + f) % 12;
          if (target[j][0] + 1 > ma[0])
          {
            ma[0] = target[j][0] + 1;
          }
          if (target[j][1] + 1 > ma[1])
          {
            ma[1] = target[j][1] + 1;
          }
        }
        if (ma[0] * ma[1] < mi[2] || (ma[0] * ma[1] == mi[2] && ma[0] < mi[0]) || (ma[0] * ma[1] == mi[2] && ma[0] == mi[0] && ma[1] < mi[1]))
        {
          mi[0] = ma[0];
          mi[1] = ma[1];
          mi[2] = ma[0] * ma[1];
          for (int j = 0; j < K; ++j)
          {
            out[j][0] = target[j][0];
            out[j][1] = target[j][1];
          }
        }
      }
    }
    int ix = atomic_inc(counter);
    if (ix < LIMIT)
    {
      output[(2 * K + 3) * ix + 0] = mi[1];
      output[(2 * K + 3) * ix + 1] = mi[0];
      output[(2 * K + 3) * ix + 2] = mi[2];
      for (int i = 0; i < K; ++i)
      {
        for (int j = 0; j < 2; ++j)
        {
          output[(2 * K + 3) * ix + 3 + 2 * i + j] = out[i][j];
        }
      }
    }
  }
}

signed char buffer[LIMIT * (2 * K + 3)];
short A[12 * 12 * 12 * 12 * 4];

int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  int nA = 0;
  for (short a = 0; a < 12; ++a)
  for (short b = 0; b < 12; ++b)
  for (short d = 0; d < 12; ++d)
  for (short e = 0; e < 12; ++e)
  {
    short det = (a * e - b * d + 144) % 12;
    if (! (det == 1 || det == 5 || det == 7 || det == 11))
      continue;
    signed char aut[144];
    for (int i = 0; i < 144; ++i)
    {
      aut[i] = 0;
    }
    int valid = 1;
    for (short i = 0; i < 12 && valid; ++i)
    {
      for (short j = 0; j < 12 && valid; ++j)
      {
        short ii = (a * i + b * j) % 12;
        short jj = (d * i + e * j) % 12;
        short kk = ii + 12 * jj;
        if (aut[kk]++)
        {
          valid = 0;
        }
      }
    }
    if (valid)
    {
      A[4 * nA + 0] = a;
      A[4 * nA + 1] = b;
      A[4 * nA + 2] = d;
      A[4 * nA + 3] = e;
      ++nA;
    }
  }
  int64_t total = 1;
  for (int i = 1; i < K; ++i)
  {
    total *= 144;
  }
  volatile int count = 0;
  #pragma omp parallel for schedule(dynamic, 144)
  for (int64_t i = 0; i < total; ++i)
  {
    short k[K];
    k[0] = 0;
    int64_t j = i;
    for (int l = 1; l < K; ++l)
    {
      k[l] = j % 144;
      j /= 144;
    }
    A099032_1(&count, buffer, A, nA, k);
  }
  for (int i = 0; i < count && i < LIMIT; ++i)
  {
    for (int j = 0; j < 2 * K + 3; ++j)
    {
      printf("%3d", buffer[(2 * K + 3) * i + j]);
    }
    printf("\n");
  }
  fprintf(stderr, "%d\n", count);
  return 0;
}
