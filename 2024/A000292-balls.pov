// A000292 Tetrahedral numbers
//
// a(n) is the number of balls in a triangular pyramid
// in which each edge contains n balls.
//
// 0, 1, 4, 10, 20, 35, 56, 84, 120, 165, 220, 286, 364, 455, 560, 680, ...
//
// a(22) = 2024
// a(4) = 20 depicted
//

#version 3.7;

global_settings { assumed_gamma 1.0 }

#include "functions.inc"
  
camera {
  location <-13.1883297,-6.04619203,22.4755003>
  look_at <-7.92257805,-2.85922963,16.6346845>
  sky <-0.111482905,0.88617659,0.383023579>
  angle 43.79
}

background { color rgb <0,0,0> }
light_source { <12, 0.48, 60> color rgb<1,1,1> }

// for i in 0 1 2 3 4 ; do for j in 0 1 2 3 4 ; do for k in 0 1 2 3 4; do if ((i + j + k < 4)) ; then echo "sphere{ <$((2*i+j)),$((2*j+k)),$((2*k+i+2))>, 0.8 }" ; fi; done; done ; done
union{
sphere{ <0,0,2>, 0.8 }
sphere{ <0,1,4>, 0.8 }
sphere{ <0,2,6>, 0.8 }
sphere{ <0,3,8>, 0.8 }
sphere{ <1,2,2>, 0.8 }
sphere{ <1,3,4>, 0.8 }
sphere{ <1,4,6>, 0.8 }
sphere{ <2,4,2>, 0.8 }
sphere{ <2,5,4>, 0.8 }
sphere{ <3,6,2>, 0.8 }
sphere{ <2,0,3>, 0.8 }
sphere{ <2,1,5>, 0.8 }
sphere{ <2,2,7>, 0.8 }
sphere{ <3,2,3>, 0.8 }
sphere{ <3,3,5>, 0.8 }
sphere{ <4,4,3>, 0.8 }
sphere{ <4,0,4>, 0.8 }
sphere{ <4,1,6>, 0.8 }
sphere{ <5,2,4>, 0.8 }
sphere{ <6,0,5>, 0.8 }
  scale 1.25
    pigment { color rgbf <0.9,0.1,0.2,0.8> }
    finish {
      specular 0.9
      roughness 0.003
      ambient 0
      diffuse 0
      reflection {
        0.2, 1.0
        fresnel on
      }
      conserve_energy
    }
    interior
    {
      ior 1.5
      dispersion 1.1
      fade_distance 1.0
      fade_power 2
      caustics 1.0
    }
}

plane {
 z, 0
 pigment { checker color rgb <1,1,1>, color rgb <0.5,0.5,0.5> }
 finish {
   specular 0
   ambient 0
   diffuse 1
   conserve_energy
  }
 scale 5
}
