-- A096751 : T(n,k) equals the number of n-dimensional partitions of k.
{-
Array begins:
      k=0:  k=1:  k=2:  k=3:  k=4:  k=5:  k=6:  k=7:  k=8:
  n=0:  1     1     1     1     1     1     1     1     1
  n=1:  1     1     2     3     5     7    11    15    22
  n=2:  1     1     3     6    13    24    48    86   160
  n=3:  1     1     4    10    26    59   140   307   684
  n=4:  1     1     5    15    45   120   326   835  2145
  n=5:  1     1     6    21    71   216   657  1907  5507
  n=6:  1     1     7    28   105   357  1197  3857 12300
  n=7:  1     1     8    36   148   554  2024  7134 24796
  n=8:  1     1     9    45   201   819  3231 12321 46209
  n=9:  1     1    10    55   265  1165  4927 20155 80920 

T(7, 6) = 2024 7-dimensional partitions of 6
T(5, 4) = 71 5-dimensional partitions of 4 depicted
-}

import Data.List (sort)

partitions :: Int -> [[Int]]
partitions n = p n [n, n - 1 .. 1]
  where
    p 0 _ = [[]]
    p _ [] = []
    p n bs@(b:bs')
      | b <= n = (fmap (b :) (p (n - b) bs)) ++ p n bs'
      | otherwise = p n bs'

partitions2 :: Int -> [[[Int]]]
partitions2 n =
  [ q
  | p <- partitions n
  , q <- sequence $ map partitions p
  , descending (map length q)
  , descending (map head q)
  ]

partitions3 :: Int -> [[[[Int]]]]
partitions3 n =
  [ q
  | p <- partitions n
  , q <- sequence $ map partitions2 p
  , descending (map length q)
  , descending (map head q)
  , descending (map (length . head) q)
  , descending (map (head . head) q)
  ]

partitions4 :: Int -> [[[[[Int]]]]]
partitions4 n =
  [ q
  | p <- partitions n
  , q <- sequence $ map partitions3 p
  , descending (map length q)
  , descending (map head q)
  , descending (map (length . head) q)
  , descending (map (head . head) q)
  , descending (map (length . head . head) q)
  , descending (map (head . head . head) q)
  ]

partitions5 :: Int -> [[[[[[Int]]]]]]
partitions5 n =
  [ q
  | p <- partitions n
  , q <- sequence $ map partitions4 p
  , descending (map length q)
  , descending (map head q)
  , descending (map (length . head) q)
  , descending (map (head . head) q)
  , descending (map (length . head . head) q)
  , descending (map (head . head . head) q)
  , descending (map (length . head . head . head) q)
  , descending (map (head . head . head . head) q)
  ]

partitions6 :: Int -> [[[[[[[Int]]]]]]]
partitions6 n =
  [ q
  | p <- partitions n
  , q <- sequence $ map partitions5 p
  , descending (map length q)
  , descending (map head q)
  , descending (map (length . head) q)
  , descending (map (head . head) q)
  , descending (map (length . head . head) q)
  , descending (map (head . head . head) q)
  , descending (map (length . head . head . head) q)
  , descending (map (head . head . head . head) q)
  , descending (map (length . head . head . head . head) q)
  , descending (map (head . head . head . head . head) q)
  ]

partitions7 :: Int -> [[[[[[[[Int]]]]]]]]
partitions7 n =
  [ q
  | p <- partitions n
  , q <- sequence $ map partitions6 p
  , descending (map length q)
  , descending (map head q)
  , descending (map (length . head) q)
  , descending (map (head . head) q)
  , descending (map (length . head . head) q)
  , descending (map (head . head . head) q)
  , descending (map (length . head . head . head) q)
  , descending (map (head . head . head . head) q)
  , descending (map (length . head . head . head . head) q)
  , descending (map (head . head . head . head . head) q)
  , descending (map (length . head . head . head . head . head) q)
  , descending (map (head . head . head . head . head . head) q)
  ]

descending xs = reverse xs == sort xs

coords xs = concatMap (\(n,cs) -> map (\(ns, k) -> (n:ns, k)) cs) . zip [0..] $ xs

coordinates1 :: [[Int]] -> [[([Int], Int)]]
coordinates1 = map (zipWith (\n k -> ([n], k)) [0..])

coordinates2 :: [[[Int]]] -> [[([Int], Int)]]
coordinates2 = map (coords . coordinates1)

coordinates3 :: [[[[Int]]]] -> [[([Int], Int)]]
coordinates3 = map (coords . coordinates2)

coordinates4 :: [[[[[Int]]]]] -> [[([Int], Int)]]
coordinates4 = map (coords . coordinates3)

coordinates5 :: [[[[[[Int]]]]]] -> [[([Int], Int)]]
coordinates5 = map (coords . coordinates4)

coordinates6 :: [[[[[[[Int]]]]]]] -> [[([Int], Int)]]
coordinates6 = map (coords . coordinates5)

coordinates7 :: [[[[[[[[Int]]]]]]]] -> [[([Int], Int)]]
coordinates7 = map (coords . coordinates6)

point :: ([Int], Int) -> (String, String)
point (ns, k) =
  ( "<path d='M 0,0 L " ++
    (unwords (zipWith (\x y -> show x ++ "," ++ show y)
      (scanl1 (+) cxs) (scanl1 (+) cys))) ++
    "' />"
  , "<circle cx='" ++ show cx ++
    "' cy='" ++ show cy ++
    "' r='" ++ show r ++
    "' />"
  )
  where
    d = length ns
    ts = [ fromIntegral i / fromIntegral d * pi | i <- [0 .. d - 1] ]
    cs = map cos ts
    ss = map sin ts
    cxs = zipWith (\n c -> fromIntegral n * c) ns cs
    cys = zipWith (\n c -> fromIntegral n * c) ns ss
    cx = sum cxs
    cy = sum cys
    r  = sqrt (fromIntegral k) / sqrt (fromIntegral d) / 2

points :: [([Int], Int)] -> String
points = uncurry (++) . both unlines . unzip . map point

both :: (a -> b) -> (a, a) -> (b, b)
both f (a, b) = (f a , f b)

page :: [[([Int], Int)]] -> String
page ps = concat $
  [ "<g transform='translate(" ++
    show (4{-6-} * fromIntegral tx + (if even ty then 2{-3-} else 0)) ++ "," ++
    show (4{-6-} * fromIntegral ty * sqrt 3 / 2) ++ ")'>\n" ++
    points p ++ "</g>\n"
  | (p, [ty, tx]) <- ps `zip` sequence [[1..8{-44-}], [1..9{-46-}]]
  ]

svg :: String
svg = "<svg width='3375' height='2625'>\n" ++
      "<style>\n" ++ css ++ "</style>\n" ++
      "<rect width='100%' height='100%' />\n" ++
      -- page (coordinates7 $ partitions7 6) ++
      "<g transform='translate(222.618665811417,225) scale(69.7562540089801,69.7562540089801)'>\n" ++
      page (coordinates5 $ partitions5 4) ++
      "</g>\n" ++
      "</svg>\n"

css :: String
css = "rect { stroke: none; fill: white; }\n" ++
      "path { stroke: black; fill: none; stroke-width: 0.1; " ++
      "stroke-linejoin: round; stroke-linecap: round; }\n" ++
      "circle { stroke: black; fill: red; stroke-width: 0.1; }\n"

main :: IO ()
main = putStr svg
