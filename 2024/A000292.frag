// A000292 Tetrahedral numbers
/*
a(n) is the number of balls in a triangular pyramid
in which each edge contains n balls.

0, 1, 4, 10, 20, 35, 56, 84, 120, 165, 220, 286, 364, 455, 560, 680, ...

a(22) = 2024
a(4) = 20 depicted
*/

#version 330 core

#include "MathUtils.frag"
#include "DE-Raytracer.frag"

float DE(vec3 pos)
{
  const int N = 4;
  float d = 1.0 / 0.0;
  for (int i = 0; i <= N; ++i)
  for (int j = 0; j <= N; ++j)
  for (int k = 0; k <= N; ++k)
  {
    if (i + j + k < N)
    {
      vec3 p = vec3(i + j * 0.5, j + k * 0.5, k + i * 0.5);
      d = min(d, distance(pos, p) - 0.25);
    }
  }
  return d;
}


#preset Default
FOV = 0.21875
Eye = -1.22902549,7.03406186,10.0401887
Target = 0.786171857,2.16358755,1.54210303
Up = 0.416232518,0.827955492,-0.37581938
EquiRectangular = false
AutoFocus = false
FocalPlane = 10
Aperture = 0.2
Gamma = 1
ToneMapping = 1
Exposure = 1
Brightness = 1
Contrast = 1
AvgLumin = 0.50000763,0.50000763,0.50000763
Saturation = 1
LumCoeff = 0.2125,0.7154,0.0721
Hue = 0
GaussianWeight = 1
AntiAliasScale = 2
DepthToAlpha = false
ShowDepth = false
DepthMagnitude = 1
Detail = -7
DetailAO = -7
FudgeFactor = 1
MaxDistance = 5000
MaxRaySteps = 200
Dither = 0.5
NormalBackStep = 1
AO = 0,0,0,0
Specular = 1
SpecularExp = 100
SpecularMax = 100
SpotLight = 1,1,1,1
SpotLightDir = 0.11246156,0.14676924
CamLight = 1,1,1,0
CamLightMin = 0
Glow = 1,1,1,0
GlowMax = 20
Fog = 0
HardShadow = 1 NotLocked
ShadowSoft = 20
QualityShadows = false
Reflection = 0.001 NotLocked
DebugSun = false
BaseColor = 1,0.0980392157,0.0980392157
OrbitStrength = 0
X = 0.5,0.6,0.6,0.7
Y = 1,0.6,0,0.4
Z = 0.8,0.78,1,0.5
R = 0.4,0.7,1,0.12
BackgroundColor = 1,1,1
GradientBackground = 0
CycleColors = false
Cycles = 1.1
EnableFloor = true
FloorNormal = 1,1,1
FloorHeight = -0.25
FloorColor = 1,1,1
#endpreset
