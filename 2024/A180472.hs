-- A180472 T(n, k) is the number of chiral bracelets with k white beads and n - k black beads
{-
T(n, k) is the number of bracelets (turnover necklaces) of length n that have
no reflection symmetry and consist of k white beads and n - k black beads.
(Bracelets that have no reflection symmetry are also known as chiral bracelets.)

A008804 a(0) = 1, a(42) = 2024, a(n) = T(n + 6, 4)
1, 2, 4, 6, 10, 14, 20, 26, 35, 44, 56, 68, 84, 100, 120, 140, 165, 190, ...

2024 chiral bracelets with 4 white beads and 44 black beads
35 chiral bracelets with 4 white beads and 11 black beads depicted
-}

rotations xs = take (length xs) . map (take (length xs)) . iterate tail . cycle $ xs

canonical xs = xs == maximum (rotations xs ++ rotations (reverse xs))

chiral xs = all (reverse xs /=) (rotations xs)

valid xs = canonical xs && chiral xs

choose 0 0 = [[]]
choose a 0 = [replicate a True]
choose 0 b = [replicate b False]
choose a b = map (True :) (choose (a - 1) b) ++ map (False :) (choose a (b - 1))

t n k
  | k <= n = filter valid (choose k (n - k))
  | otherwise = []

draw n [y, x] xs
  = concat (zipWith (ball "") [0..] xs) ++
    "<clipPath id='cp-" ++ show y ++ "-" ++ show x ++ "'>\n" ++
    "<rect x='" ++ show (10 * x) ++ "' y='" ++ show (10 * y) ++ "' width='5' height='10' />\n" ++
    "</clipPath>\n" ++
    ball (" clip-path='url(#cp-" ++ show y ++ "-" ++ show x ++ ")'") 0 (head xs)
  where
    ball a i b
      = "<circle class='" ++ (if b then "t" else "f") ++
        "' cx='" ++ show (fromIntegral (10 * x + 5) + 3.5 * sin t) ++
        "' cy='" ++ show (fromIntegral (10 * y + 5) - 3.5 * cos t) ++
        "' r='1'" ++ a ++ " />\n"
      where
        t = 2 * pi * fromIntegral i / fromIntegral n

svg =
    "<svg width='3375' height='2625'>\n" ++
    "<style>\n" ++
    ".t { fill: red; }\n" ++
    ".f { fill: white; }\n" ++
    "circle { stroke: black; stroke-width: 0.1; }\n" ++
    "clipPath { display: none; }\n" ++
    "</style>\n" ++
    "<rect width='100%' height='100%' fill='white' stroke='none' />" ++
    "<g transform='translate(187.5,241.071428571429) scale(42.8571428571429,42.8571428571429)'>\n" ++
    concat (zipWith (draw 15) (sequence [[0..4],[0..6]]) (t 15 4)) ++
    "</g>\n" ++
    "</svg>\n"

main = putStr svg
