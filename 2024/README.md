# 2024

## Notes

- 2024 = 44 * 46
- 2024 = 45^2 - 1
- 2024 = 2^3 * 11 * 23

## Print

- full bleed size 11.25x8.75in (3375x2625px @300dpi)

- cropped size 11x8.5in

- safe area 10x7.25in (3000x2175px @300dpi)

  top left at 0.625x0.75in (187.5x225px @300dpi)

## TODO

- A099032 redo layout

## Done

- A000427	7-dim part of 6		Number of 7-dimensional partitions of n.

  A042984	7-dim part of 6		Number of n-dimensional partitions of 6.

  A096751	7-dim part of 6		Square table, read by antidiagonals, where T(n,k) equals the number of n-dimensional partitions of k.

  a = 2024

- A099032	n-motif patterns	Number of different patterns of n-motifs.

  a(5) = 2024

  5-motifs in `Z_12 \times Z_12`

- A220675	4x4 links		Number of ways to reciprocally link elements of an n X n array either to themselves or to exactly two horizontal, vertical and antidiagonal neighbors, without consecutive collinear links

  A220677	4x4 links		Number of ways to reciprocally link elements of an nX4 array either to themselves or to exactly two horizontal, vertical and antidiagonal neighbors, without consecutive collinear links

  A220681	4x4 links		T(n,k)=Number of ways to reciprocally link elements of an nXk array either to themselves or to exactly two horizontal, vertical and antidiagonal neighbors, without consecutive collinear links

- A233357				Triangle read by rows: T(n,k) = ((Stirling2)^2)(n,k) * k! 	T(n,k) is the number of partitions of an n-set into colored blocks, such that exactly k colors are used. T(3,2) = 12: 1a|23b, 1b|23a, 13a|2b, 13b|2a, 12a|3b, 12b|3a, 1a|2a|3b, 1b|2b|3a, 1a|2b|3a, 1b|2a|3b, 1a|2b|3b, 1b|2a|3a. - Alois P. Heinz, Sep 01 2019

  T(6,2) = 2024

- A348707	Polyominoes		Number of tree polyominoes with width 4 and height n.

  a(4) = 2024

- A002492	Bishop moves		Total number of possible bishop moves on an n+1 X n+1 chessboard, if the bishop is placed anywhere. E.g., on a 3 X 3-Board: bishop has 8 X 2 moves and 1 X 4 moves, so a(2)=20.

  a(11) = 2024

- A331229	Triangles		a(n) = number of triangles with integer sides i <= j <= k with radius of circumcircle <= n.

  a(15) = 2024

  A331240	Triangles		a(n) = number of triangles with integer sides i <= j <= k with diameter of circumcircle <= n.

  a(30) = 2024

- A188148	3-step walks		Number of 3-step self-avoiding walks on an n X n square summed over all starting positions.

  a(14) = 2024

- A005563	Toads and frogs puzzle	the number of moves that it takes n frogs to swap places with n toads on a strip of 2n + 1 squares (or positions, or lily pads) where a move is a single slide or jump,

  a(44) = 2024

- A180472 Chiral bracelets    T(n, k) is the number of chiral bracelets with k white beads and n - k black beads

  A008804	Bracelets		a(n) is the number of bracelets with 4 black beads and n+3 white beads which have no reflection symmetry. For n=1 we have for example 2 such bracelets with 4 black beads and 4 white beads: BBBWBWWW and BBWBWBWW. - Herbert Kociemba, Nov 27 2016

- A293821	Quadrilaterals		Number of integer-sided quadrilaterals having perimeter n, modulo rotations but not reflections.  	For example, there are 4 rotation-classes of perimeter-7 quadrilaterals: 3211, 3121, 3112, 2221. Note that 3211 and 3112 are reflections of each other, but these are not rotationally equivalent.

  a(47) = 2024

- A000292	Tetrahedral numbers	a(n) is the number of balls in a triangular pyramid in which each edge contains n balls.

  a(22) = 2024

- A000292	Tetrahedral numbers	4 n sided dicesumming to n+3

  a(22) = 2024

## Reject

- A005232	n-colour 4-necklace	Also number of non-equivalent necklaces of 4 beads each of them painted by one of n colors.  a(43) = 2024
- A052515	Binary reject rare	Number of ordered pairs of complementary subsets of an n-set with both subsets of cardinality at least 2.  a(n) is the number of binary sequences of length n having at least two 0's and at least two 1's. a(4)=6 because there are six binary sequences of length four that have two or more 0's and two or more 1's: 0011, 0101, 0110, 1100, 1010, 1001. -  a(11)	= 2024
- A335352	Lines in a square	a(n) is the number of vertices formed in a square by dividing each of its sides into n equal parts giving a total of 4*n nodes and drawing straight line segments from node k to node (k+n+1) mod 4*n, 0 <= k < 4*n.  a(22) = 2024
- A275553	endofunctions		Number of classes of endofunctions of [n] under vertical translation mod n, complement to n+1 and reversal.
- A128719	skew Dyck paths		Triangle read by rows: T(n,k) is the number of skew Dyck paths of semilength n and having k UUU's (triplerises) (n >= 0; 0 <= k <= n-2 for n >= 2). A skew Dyck path is a path in the first quadrant which begins at the origin, ends on the x-axis, consists of steps U=(1,1)(up), D=(1,-1)(down) and L=(-1,-1)(left) so that up and left steps do not overlap. The length of the path is defined to be the number of its steps.
- A187276	convex polyominoes	Number of d+/d- diagonally convex polyominoes with n cells.  A polyomino is d+ [d-] convex if the intersection of its interior with any line of slope 1 [-1] through the centers of the cells is connected.
- A345459	Lines in a square	Number of polygons formed when connecting all 4n points on the perimeter of an n X n square by infinite lines.
- A358882	Farey diagram		The number of regions in a Farey diagram of order (n,n).
- A039720	Countdown juggle	Period of n-countdown club-passing juggling pattern. 	  Tarim's countdown: 2 people pass clubs to each other after n throws, then n-1 throws, then n-2, ..., 2, 1, 2, ... n-1. Thus for 3-countdown it is 3,2,1,2 (and repeat), or put in terms of pass throws and self-throws, pass-self-self-pass-self-pass-pass-self and repeat.
- A006566	Dodecahedral numbers	a(n) = n*(3*n - 1)*(3*n - 2)/2.
- A223953				Number of 6 X n 0..1 arrays with diagonals and antidiagonals unimodal and rows nondecreasing.
- A045854	24dim 2norm		Number of nonnegative solutions of x1^2 + x2^2 + ... + x24^2 = n.
- A188777	Bishop tours		T(n,k)=Number of n-turn bishop's tours on a kXk board summed over all starting positions
- A177724	8-point line segments	Number of line segments connecting exactly 8 points in an n x n grid of points  	a(n) is also the number of pairs of points visible to each other exactly through 6 points in an n x n grid of points.
- A137932	`K_{5,n}` crossing	Also the crossing number of the complete bipartite graph `K_{5,n}`. - Eric W. Weisstein, Sep 11 2018  a(46) = 2024
- A006456	Square compositions	Number of compositions (ordered partitions) of n into squares.
- A326540	Prime paratitions	Sum of all the parts in the partitions of n into 9 primes.
- A100883	Partition freq		Number of partitions of n in which the sequence of frequencies of the summands is nondecreasing.
- A292090	Watanabe		Preperiod (or threshold) of orbit of Watanabe's 3-shift tag system {00/1011} applied to the word (100)^n.
- A292091	Watanabe		Period of orbit of Watanabe's 3-shift tag system {00/1011} applied to the word (100)^n. 		+10
- A271889	2D CA			Number of active (ON,black) cells in n-th stage of growth of two-dimensional cellular automaton defined by "Rule 411", based on the 5-celled von Neumann neighborhood.
- A273334	2D CA			Number of active (ON,black) cells in n-th stage of growth of two-dimensional cellular automaton defined by "Rule 657", based on the 5-celled von Neumann neighborhood.
- A273443	2D CA			Number of active (ON,black) cells in n-th stage of growth of two-dimensional cellular automaton defined by "Rule 721", based on the 5-celled von Neumann neighborhood.
- A244370	Toothpicks		Total number of toothpicks after n-th stage in the toothpick structure of the symmetric representation of sigma in the four quadrants.
- A308089	Triangles		Sum of the perimeters of all integer-sided triangles with perimeter n.
- A100435	Cuboids			Number of distinct products i*j*k for 1 <= i <= j < k <= n.
- A265175				Number of nX3 0..n*3-1 arrays with upper left zero and lower right n*3-1 and each element differing from its horizontal and vertical neighbors by a power of two.
- A326505	max heap		Number of (binary) max-heaps on n elements from the set {0,1} containing exactly four 0's.
- A237500				Number of binary strings of length 2n which contain the ones' complement of each of their two halves.  	The two halves of 011001 are 011 and 001. Their complements are 100 and 110, and both are substrings of 011001. Since there are 11 other strings of length 2*3 with this property, a(3) = 12.
- A105291				Triangle read by rows: T(m,n) = binomial(m!,n), m>=0, 0 <= n <= m!. This is the number of nXm arrays with each row a permutation of 1..m, and rows in lexicographically strictly increasing order.
- A190093	rhombuses		Number of rhombuses on an (n+1) X 5 grid.
