// A220681 T(n,k)=Number of ways to reciprocally link elements of an nXk array...
/*
...either to themselves or to exactly two horizontal, vertical and
   antidiagonal neighbors, without consecutive collinear links

Table starts
   .1.....1........1............1................1...................1
   .1.....4........9...........25...............64.................169
   .1.....9.......43..........201..............968................4604
   .1....25......201.........2024............21380..............220299
   .1....64......968........21380...........468036.............9973284
   .1...169.....4604.......220299..........9973284...........455249977
   .1...441....220090......2316754........215926776.........21051034046
   .1..1156...104976.....24198121.......4649097520........969333059888

T(4, 4) = 2024
T(3, 3) = 43 depicted

._._.
|/|/|
._._.
|/|/|
._._.

*/

#include <stdio.h>

int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  int count = 1;

  printf(
    "<svg width='3375' height='2625'>\n"
    "<style>\n"
    "rect { fill: white; stroke: none; }\n"
    "path, circle { stroke: black; stroke-width: 0.1; stroke-linecap: round; }\n"
    "path { fill: none; }\n"
    "circle { fill: red; }\n"
    "</style>\n"
    "<rect width='100%%' height='100%%' />\n"
    "<g transform='translate(187.5,479.166666666667) scale(41.6666666666667,41.6666666666667)'>\n"
  );

  for (int a1 = 0; a1 < 2; ++a1) {
  for (int a2 = 0; a2 < 2; ++a2) { if (2 * a1 + a2 > 2) continue;
  for (int a3 = 0; a3 < 2; ++a3) { if (2 * a3 + a2 > 2) continue;
  for (int a4 = 0; a4 < 2; ++a4) { if (2 * a3 + a2 + a4 > 2 || (a2 && a4)) continue;
  for (int a5 = 0; a5 < 2; ++a5) { if (2 * a5 + a4 > 2) continue;
  
  for (int b1 = 0; b1 < 2; ++b1) { if (2 * a1 + a2 + b1 != 2) continue;
  for (int b2 = 0; b2 < 2; ++b2) { if (2 * a3 + a2 + a4 + b2 > 2) continue;
  for (int b3 = 0; b3 < 2; ++b3) { if (2 * a3 + a2 + a4 + b2 + b3 != 2) continue;
  for (int b4 = 0; b4 < 2; ++b4) { if (2 * a5 + a4 + b4 > 2) continue;
  for (int b5 = 0; b5 < 2; ++b5) { if (2 * a5 + a4 + b4 + b5 != 2) continue;
  
  for (int c1 = 0; c1 < 2; ++c1) { if (2 * c1 + b1 + b2 > 2) continue;
  for (int c2 = 0; c2 < 2; ++c2) { if (2 * c1 + c2 + b1 + b2 > 2) continue;
  for (int c3 = 0; c3 < 2; ++c3) { if (2 * c3 + c2 + b3 + b4 > 2) continue;
  for (int c4 = 0; c4 < 2; ++c4) { if (2 * c3 + c2 + c4 + b3 + b4 > 2 || (c2 && c4)) continue;
  for (int c5 = 0; c5 < 2; ++c5) { if (2 * c5 + c4 + b5 > 2) continue;
  
  for (int d1 = 0; d1 < 2; ++d1) { if (2 * c1 + c2 + b1 + b2 + d1 != 2 || (b1 && d1)) continue;
  for (int d2 = 0; d2 < 2; ++d2) { if (2 * c3 + c2 + c4 + b3 + b4 + d2 > 2 || (d2 && b4)) continue;
  for (int d3 = 0; d3 < 2; ++d3) { if (2 * c3 + c2 + c4 + b3 + b4 + d2 + d3 != 2 || (d3 && b3)) continue;
  for (int d4 = 0; d4 < 2; ++d4) { if (2 * c5 + c4 + b5 + d4 > 2) continue;
  for (int d5 = 0; d5 < 2; ++d5) { if (2 * c5 + c4 + b5 + d4 + d5 != 2 || (d5 && b5)) continue;
  
  for (int e1 = 0; e1 < 2; ++e1) { if (2 * e1 + d1 + d2 > 2) continue;
  for (int e2 = 0; e2 < 2; ++e2) { if (2 * e1 + e2 + d1 + d2 != 2) continue;
  for (int e3 = 0; e3 < 2; ++e3) { if (2 * e3 + e2 + d3 + d4 > 2) continue;
  for (int e4 = 0; e4 < 2; ++e4) { if (2 * e3 + e2 + e4 + d3 + d4 != 2) continue;
  for (int e5 = 0; e5 < 2; ++e5) { if (2 * e5 + e4 + d5 != 2 || (e2 && e4)) continue;
  {

    int x = count % 9;
    int y = count / 9;
    printf("<g transform='translate(%d,%d)'>\n", 8 * x, 8 * y);
    if (a2) printf("<path d='M 2,2 L 4,2' />\n");
    if (a4) printf("<path d='M 4,2 L 6,2' />\n");
    if (b1) printf("<path d='M 2,4 L 2,2' />\n");
    if (b2) printf("<path d='M 2,4 L 4,2' />\n");
    if (b3) printf("<path d='M 4,4 L 4,2' />\n");
    if (b4) printf("<path d='M 4,4 L 6,2' />\n");
    if (b5) printf("<path d='M 6,4 L 6,2' />\n");
    if (c2) printf("<path d='M 2,4 L 4,4' />\n");
    if (c4) printf("<path d='M 4,4 L 6,4' />\n");
    if (d1) printf("<path d='M 2,6 L 2,4' />\n");
    if (d2) printf("<path d='M 2,6 L 4,4' />\n");
    if (d3) printf("<path d='M 4,6 L 4,4' />\n");
    if (d4) printf("<path d='M 4,6 L 6,4' />\n");
    if (d5) printf("<path d='M 6,6 L 6,4' />\n");
    if (e2) printf("<path d='M 2,6 L 4,6' />\n");
    if (e4) printf("<path d='M 4,6 L 6,6' />\n");
    if (a1) printf("<circle cx='2' cy='2' r='0.5' />\n");
    if (a3) printf("<circle cx='4' cy='2' r='0.5' />\n");
    if (a5) printf("<circle cx='6' cy='2' r='0.5' />\n");
    if (c1) printf("<circle cx='2' cy='4' r='0.5' />\n");
    if (c3) printf("<circle cx='4' cy='4' r='0.5' />\n");
    if (c5) printf("<circle cx='6' cy='4' r='0.5' />\n");
    if (e1) printf("<circle cx='2' cy='6' r='0.5' />\n");
    if (e3) printf("<circle cx='4' cy='6' r='0.5' />\n");
    if (e5) printf("<circle cx='6' cy='6' r='0.5' />\n");
    printf("</g>\n");
    count++;

  }
  }}}}}}}}}}}}}}}}}}}}}}}}}

  printf(
    "</g>\n"
    "</svg>\n"
  );
  fprintf(stderr, "%d\n", --count);
  return 0;
}
