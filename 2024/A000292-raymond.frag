#version 430 core

// A000292 Tetrahedral numbers
/*
a(n) is the number of balls in a triangular pyramid
in which each edge contains n balls.

0, 1, 4, 10, 20, 35, 56, 84, 120, 165, 220, 286, 364, 455, 560, 680, ...

a(22) = 2024
a(4) = 20 depicted
*/


#include "Raymond.frag"

vec3 film(Random PRNG, float wavelength, float intensity)
{
//  return vec3(fp4plus(wavelength) * intensity);
  return xyz2rgb(observer(wavelength) * intensity);
}

float screen(vec4 tex, float wavelength)
{
  return CRT(sRGB2linear(tex.rgb), wavelength);
}

uniform vec3 LightDir; slider[(-1,-1,-1),(0,0,0),(1,1,1)]
uniform float LightDistance; slider[10,10,100]

vec4 light(Random PRNG, vec3 from, vec3 dir)
{
  vec3 d = LightDistance * normalize(LightDir) - from;
  return vec4(d, max(dot(normalize(d), normalize(LightDir)), 0));
}

#define SCENE(hit,surface,scene_tag) \
hit scene(scene_tag tag, Random PRNG, Ray V) \
{ \
  hit H = Union(Light(Invert(Sphere(tag, Scale(LightDistance), V)), V, D65(V.wavelength) * pow(max(dot(normalize(V.origin), normalize(LightDir)), 0), 256.0)) \
              , Diffuse(srand(PRNG, 1), Plane(tag, Identity(), V, Z, 0.0), V, 0.1) \
              ); \
  const int N = 4; \
  int r = 3; \
  for (int i = 0; i <= N; ++i) \
  for (int j = 0; j <= N; ++j) \
  for (int k = 0; k <= N; ++k) \
  { \
    if (i + j + k < N) \
    { \
      H = Union(H, CookTorrance(srand(PRNG, ++r), Sphere(tag, Translate(2.5 * vec3(i + j * 0.5, j + k * 0.5, k + i * 0.5 + 2)), V), V, screen(vec4(1,0,0,0), V.wavelength), 0.5, 0.00, vec2(1.3, 0.1))); \
    } \
  } \
  return H; \
}

SCENE(Hit,Surface,Scene_HIT)
#if 1
// fast
SCENE(float,float,Scene_DE)
#else
// slow
float scene(Scene_DE tag, Random PRNG, Ray V)
{
  Scene_HIT HIT;
  return scene(HIT, PRNG, V).surface.de;
}
#endif




#preset Default
Steps = 100
Depth = 10
MinDist = -16
Acne = -12
SampleLights = false
CompensatedHack = 1
Compensated2IEEEAdd = true
Wave1Period = 1
Wave1Colour1 = 1,0,0
Wave1Colour2 = 1,1,0
Wave1Colour3 = 0,0.5,0
Wave1Colour4 = 0,0,1
Wave2Period = 10
Wave2Colour1 = 1,0,0
Wave2Colour2 = 1,1,0
Wave2Colour3 = 0,0.5,0
Wave2Colour4 = 0,0,1
Wave3Period = 100
Wave3Colour1 = 1,0,0
Wave3Colour2 = 1,1,0
Wave3Colour3 = 0,0.5,0
Wave3Colour4 = 0,0,1
Wave4Period = 1000
Wave4Colour1 = 1,0,0
Wave4Colour2 = 1,1,0
Wave4Colour3 = 0,0.5,0
Wave4Colour4 = 0,0,1
InteriorColour = 0,0,0
FOV = 0.5
Eye = -13.1883297,-6.04619203,22.4755003
Target = -7.92257805,-2.85922963,16.6346845
Up = -0.111482905,0.88617659,0.383023579
DebugDepth = false
DebugNormal = false
DebugBounce = false
Background = 
Distance = 100
Aperture = 0.1,0.01
Size = 35
Wavelengths = 300,780
LightDir = 0.20000002,0.008,1
LightDistance = 60
Exposure = 10
ShowHotPixels = false
#endpreset
