#include <stdio.h>
#include <time.h>

#include <glm/glm.hpp>
using namespace glm;

const dvec2 triangles[26][3] =
{{{ 0, 0}, {0, 1}, {1, 0}}
,{{ 0, 0}, {0, 1}, {0, 2}}
,{{ 0, 0}, {1, 0}, {0, 2}}
,{{ 0, 0}, {0, 2}, {2, 0}}
,{{ 0, 0}, {1, 2}, {2, 0}}
,{{ 0, 1}, {1, 2}, {2, 0}}
,{{ 0, 0}, {0, 1}, {0, 3}}
,{{ 0, 0}, {1, 0}, {0, 3}}
,{{ 0, 0}, {1, 3}, {2, 0}}
,{{ 0, 0}, {2, 1}, {0, 3}}
,{{ 0, 0}, {0, 3}, {3, 0}}
,{{ 0, 0}, {0, 1}, {0, 4}}
,{{ 0, 0}, {0, 2}, {0, 4}}
,{{ 0, 0}, {1, 0}, {0, 4}}
,{{ 0, 0}, {2, 0}, {0, 4}}
,{{ 0, 0}, {0, 4}, {4, 0}}
,{{ 0, 2}, {2, 4}, {4, 0}}
,{{ 0, 0}, {0, 1}, {0, 5}}
,{{ 0, 0}, {0, 1}, {0, 6}}
,{{ 0, 0}, {0, 2}, {0, 6}}
,{{ 0, 0}, {0, 3}, {0, 6}}
,{{ 0, 0}, {1, 0}, {0, 6}}
,{{ 0, 0}, {2, 0}, {0, 6}}
,{{ 0, 0}, {3, 0}, {0, 6}}
,{{ 0, 0}, {0, 6}, {6, 0}}
,{{ 0, 0}, {0, 4}, {0, 8}}
};

double distance_between_line_segments(dvec2 a0, dvec2 a1, dvec2 b0, dvec2 b1)
{
  const double eta = 1e-6;
  dvec2 r = b0 - a0, u = a1 - a0, v = b1 - b0;
  double ru = dot(r,u), rv = dot(r,v), uu = dot(u,u), uv = dot(u,v), vv = dot(v,v), det = uu*vv - uv*uv, s, t;
  if (det < eta*uu*vv)
  {
    s = clamp(ru/uu, 0.0, 1.0);
    t = 0;
  }
  else
  {
    s = clamp((ru*vv - rv*uv)/det, 0.0, 1.0);
    t = clamp((ru*uv - rv*uu)/det, 0.0, 1.0);
  }
  double S = clamp((t*uv + ru)/uu, 0.0, 1.0);
  double T = clamp((s*uv - rv)/vv, 0.0, 1.0);
  dvec2 A = a0 + S*u;
  dvec2 B = b0 + T*v;
  return distance(A, B);
}

double distance_between_triangles(dvec2 a[3], dvec2 b[3])
{
  double d  = distance_between_line_segments(a[0], a[1], b[0], b[1]);
  d = fmin(d, distance_between_line_segments(a[0], a[1], b[1], b[2]));
  d = fmin(d, distance_between_line_segments(a[0], a[1], b[2], b[0]));
  d = fmin(d, distance_between_line_segments(a[1], a[2], b[0], b[1]));
  d = fmin(d, distance_between_line_segments(a[1], a[2], b[1], b[2]));
  d = fmin(d, distance_between_line_segments(a[1], a[2], b[2], b[0]));
  d = fmin(d, distance_between_line_segments(a[2], a[0], b[0], b[1]));
  d = fmin(d, distance_between_line_segments(a[2], a[0], b[1], b[2]));
  d = fmin(d, distance_between_line_segments(a[2], a[0], b[2], b[0]));
  return d;
}

int main(int argc, char **argv)
{
  srand(time(0));
  bool valid = false;
  dvec2 T[26][3];
  while (! valid)
  {
    valid = true;
    for (int i = 0; valid && i < 26; ++i)
    {
      dvec2 o;
      o.x = rand() % 40;
      o.y = rand() % 25;
      T[i][0] = triangles[i][0] + o;
      T[i][1] = triangles[i][1] + o;
      T[i][2] = triangles[i][2] + o;
      for (int j = 0; valid && j < i; ++j)
      {
        if (distance_between_triangles(T[i], T[j]) < 3)
        {
          valid = false;
        }
      }
    }
  }

  dvec2 mi = dvec2(1.0/0.0, 1.0/0.0), ma = dvec2(-1.0/0.0, -1.0/0.0);
  for (int i = 0; i < 26; ++i)
  {
    mi = min(mi, T[i][0]);
    mi = min(mi, T[i][1]);
    mi = min(mi, T[i][2]);
    ma = max(ma, T[i][0]);
    ma = max(ma, T[i][1]);
    ma = max(ma, T[i][2]);
  }

  int w = 3375;
  int h = 2625;
  int b = 225;
  double x1 = mi.x - 2;
  double y1 = mi.y - 2;
  double s = (w - b - b) / ((ma.x + 2) - (mi.x - 2));
  double x0 = b;
  double y0 = (h - s * ((ma.y + 2) - (mi.y - 2))) / 2;

  printf(
    "<svg width='%d' height='%d'>\n"
    "<style>\n"
    "#grid circle { fill: black: stroke; none; }\n"
    "#motifs circle { fill: red; stroke: black; stroke-width: 0.1; }\n"
    "#motifs path { fill: none; stroke: black; stroke-width: 0.1; }\n"
    "</style>\n"
    "<rect width='100%%' height='100%%' fill='white' stroke='none' />"
    "<g transform='translate(%g, %g) scale(%g, %g) translate(%g, %g)'>"
  , w, h, x0, y0, s, s, x1, y1
  );
  printf("<g id='grid'>\n");
  for (int j = mi.y - 2; j <= ma.y + 2; ++j)
  for (int i = mi.x - 2; i <= ma.x + 2; ++i)
  {
    printf("<circle cx='%d' cy='%d' r='0.05' />\n", i, j);
  }
  printf("</g><g id='motifs'>\n");
  for (int i = 0; i < 26; ++i)
  {
    printf("<g id='motif-%d'>\n", i);
    for (int k = 0; k < 3; ++k)
    {
      printf("<path d='M %d,%d L %d,%d' />\n", (int)T[i][k].x, (int)T[i][k].y, (int)T[i][(k+1)%3].x, (int)T[i][(k+1)%3].y);
    }
    for (int k = 0; k < 3; ++k)
    {
      printf("<circle cy='%d' cx='%d' r='0.4' />\n", (int)T[i][k].x, (int)T[i][k].y);
    }
    printf("</g>\n");
  }
  printf("</g>\n");
  printf("</svg>\n");
  return 0;
}
