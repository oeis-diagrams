// A293821 Number of integer-sided quadrilaterals having perimeter n, modulo rotations
/*
but not reflections.
For example, there are 4 rotation-classes of perimeter-7 quadrilaterals:
3211, 3121, 3112, 2221. Note that 3211 and 3112 are reflections of each other,
but these are not rotationally equivalent.

1, 1, 2, 4, 6, 10, 12, 20, 23, 35, 38, ...

a(47) = 2024
a(13) = 35 depicted
*/

#include <math.h>
#include <stdint.h>
#include <stdio.h>

#define pi 3.14159265358979

#define N 13
#define R (13 / pi / 2)

int count;
void output(int l[4])
{
  int x = (4 * (count % 7) + 3) * 2;
  int y = (4 * (count / 7) + 3) * 2;
  double p[4][2];
  int j = 0;
  for (int i = 0; i < 4; ++i)
  {
    double t = 2 * pi * j / N;
    p[i][0] = R * cos(t);
    p[i][1] = R * sin(t);
    j += l[i];
  }
  for (int j = 0; j < 1000; ++j)
  {
    for (int i = 0; i < 4; ++i)
    {
      p[i][0] *= 1.01;
      p[i][1] *= 1.01;
    }
    double F[4][2];
    for (int i = 0; i < 4; ++i)
    {
      F[i][0] = 0;
      F[i][1] = 0;
      for (int di = -1; di <= 1; di += 2)
      {
        double dx = p[(i + di + 4) % 4][0] - p[i][0];
        double dy = p[(i + di + 4) % 4][1] - p[i][1];
        double d = sqrt(dx * dx + dy * dy);
        double e = d - l[(di == -1 ? i + 3 : i) % 4];
        F[i][0] += dx / d * e;
        F[i][1] += dy / d * e;
      }
    }
    for (int i = 0; i < 4; ++i)
    {
      p[i][0] += 0.1 * F[i][0];
      p[i][1] += 0.1 * F[i][1];
    }
    double o[2] = { 0, 0 };
    for (int i = 0; i < 4; ++i)
    {
      o[0] += p[i][0];
      o[1] += p[i][1];
    }
    o[0] /= 4;
    o[1] /= 4;
    for (int i = 0; i < 4; ++i)
    {
      p[i][0] -= o[0];
      p[i][1] -= o[1];
    }
  }
  for (int i = 0; i < 4; ++i)
  {
    p[i][0] += x;
    p[i][1] += y;
  }
  printf
  ( "<path d='M %f,%f L %f,%f %f,%f %f,%f %f,%f Z' />\n"
  , p[0][0], p[0][1], p[1][0], p[1][1], p[2][0], p[2][1], p[3][0], p[3][1], p[0][0], p[0][1]
  );
  for (int i = 0; i < 4; ++i)
  {
    for (int j = 0; j < l[i]; ++j)
    {
      double t = j / (double) l[i];
      printf
      ( "<circle cx='%f' cy='%f' r='0.2' />\n"
      , p[i][0] + t * (p[(i + 1) % 4][0] - p[i][0])
      , p[i][1] + t * (p[(i + 1) % 4][1] - p[i][1])
      );
    }
  }
  ++count;
}

int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  printf(
    "<svg width='3375' height='2625'>\n"
    "<style>\n"
    "path { fill: red; stroke: black; stroke-width: 0.1; }\n"
    "circle { fill: black; stroke: none; }\n"
    "</style>\n"
    "<rect width='100%%' height='100%%' fill='white' stroke='none' />"
    "<g transform='translate(204.545454545455,225) scale(49.4318181818182,49.4318181818182)'>\n"
  );
  int l[4];
  for (l[0] = 1; l[0] <= N; ++l[0])
  for (l[1] = 1; l[1] <= N; ++l[1])
  for (l[2] = 1; l[2] <= N; ++l[2])
  for (l[3] = 1; l[3] <= N; ++l[3])
  {
    if (l[0] + l[1] + l[2] + l[3] == N)
    {
      int ma = 0;
      int su = 0;
      for (int i = 0; i < 4; ++i)
      {
        ma = l[i] > ma ? l[i] : ma;
        su += l[i];
      }
      if (ma <= su - ma)
      {
        uint64_t ix[4] = {0,0,0,0}, mi = ~(uint64_t)0;
        for (int r = 0; r < 4; ++r)
        {
          for (int i = 0; i < 4; ++i)
          {
            ix[r] *= N;
            ix[r] += l[(i + r) % 4];
          }
          mi = ix[r] < mi ? ix[r] : mi;
        }
        if (ix[0] == mi)
        {
          output(l);
        }
      }
    }
  }
  printf(
    "</g>\n"
    "</svg>\n"
  );
  fprintf(stderr, "%d\n", count);
  return 0;
}
