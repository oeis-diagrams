-- A233357 T(n,k) = the number of partitions of an n-set into colored blocks, such that exactly k colors are used.
{-
Triangle begins:
       k = 1     2      3      4       5      6      7     8           sums
1          1                                                              1
2          2     2                                                        4
3          5    12      6                                                23
4         15    64     72     24                                        175
5         52   350    660    480     120                               1662
6        203  2024   5670   6720    3600    720                       18937
7        877 12460  48552  83160   71400  30240   5040               251729
8       4140 81638 424536 983808 1201200 806400 282240 40320        3824282

T(6,2) = 2024
T(4,2) = 64 depicted
-}

import Control.Monad (replicateM)
import Data.List (groupBy, inits, nub, {- permutations, -} sort, tails)

-- base-4.17.0.0
-- | The 'permutations' function returns the list of all permutations of the argument.
--
-- >>> permutations "abc"
-- ["abc","bac","cba","bca","cab","acb"]
permutations            :: [a] -> [[a]]
permutations xs0        =  xs0 : perms xs0 []
  where
    perms []     _  = []
    perms (t:ts) is = foldr interleave (perms ts (t:is)) (permutations is)
      where interleave    xs     r = let (_,zs) = interleave' id xs r in zs
            interleave' _ []     r = (ts, r)
            interleave' f (y:ys) r = let (us,zs) = interleave' (f . (y:)) ys r
                                     in  (y:us, f (t:y:us) : zs)


equating f a b = f a == f b

blocks :: Ord a => [a] -> [[[a]]]
blocks [] = [[]]
blocks xs = nub [ sort $ f ++ [sort b] | (fs, b) <- inits xs `zip` tails xs, not (null b), f <- blocks fs]

colours :: Ord a => Int -> [[a]] -> [[(Int, a)]]
colours k ps = nub
  [ sort $ zip cs p
  | p <- ps
  , cs <- replicateM (length p) [1 .. k]
  , length (nub cs) == k
  ]

t :: Int -> Int -> [[[[Char]]]]
t n k
  = map (map (map snd) . groupBy (equating fst))
  . sort
  . colours k
  . nub
  . concatMap blocks
  . permutations
  . take n
  $ ['a'..]

shape :: Int -> Int -> Char -> String
shape y x s = "<use x='" ++ show y ++ "' y='" ++ show x ++ "' href='#" ++ [s] ++ "' />\n"

shapes :: Int -> [Char] -> String
shapes y ss = concat $ zipWith (shape y) [0..] ss

shapess :: Int -> Int -> [[[Char]]] -> String
shapess y x [xs, ys] =
  "<g transform='translate(" ++ show y ++ "," ++ show x ++ ")'>\n" ++
  "<g class='x'>\n" ++ concat (zipWith shapes [0..] xs) ++ "</g>\n" ++
  "<g class='y'>\n" ++ concat (zipWith shapes [length xs..] ys) ++ "</g>\n" ++
  "</g>\n"

svg :: [[[[Char]]]] -> String
svg s =
  "<svg width='3375' height='2625'>\n" ++
  "<defs>\n" ++
  "<circle id='a' cx='0.5' cy='0.5' r='0.4' />\n" ++
  "<path id='b' d='M 0.2,0.1 L 0.5,0.4 0.8,0.1 0.9,0.2 0.6,0.5 0.9,0.8 0.8,0.9 0.5,0.6 0.2,0.9 0.1,0.8 0.4,0.5 0.1,0.2 0.2,0.1 Z' />\n" ++
  "<rect id='c' x='0.1' y='0.1' width='0.8' height='0.8' />\n" ++
  "<path id='d' d='M 0.5,0.1536 L 0.9,0.8464 0.1,0.8464 0.5,0.1536 Z' />\n" ++
  "<style>\n" ++
  ".x { fill: red; }\n" ++
  ".y { fill: white; }\n" ++
  "</style>\n" ++
  "</defs>\n" ++
  "<rect width='100%' height='100%' fill='white' stroke='none' />\n" ++
  "<g transform='translate(319.354838709678,225) scale(70.1612903225806,70.1612903225806)' stroke='black' stroke-width='0.05' stroke-linejoin='round'>\n" ++
  concat [ shapess (5 * y) (4 * x) z | (z, [y, x]) <- zip s (sequence [[0..7], [0..7]]) ] ++
  "</g>\n" ++
  "</svg>\n"

main = putStr . svg $ t 4 2
