#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <glm/glm.hpp>
using namespace glm;

dvec2 triangles[26][3] =
{{{ 0, 0}, {0, 1}, {1, 0}}
,{{ 0, 0}, {0, 1}, {0, 2}}
,{{ 0, 0}, {1, 0}, {0, 2}}
,{{ 0, 0}, {0, 2}, {2, 0}}
,{{ 0, 0}, {1, 2}, {2, 0}}
,{{ 0, 1}, {1, 2}, {2, 0}}
,{{ 0, 0}, {0, 1}, {0, 3}}
,{{ 0, 0}, {1, 0}, {0, 3}}
,{{ 0, 0}, {1, 3}, {2, 0}}
,{{ 0, 0}, {2, 1}, {0, 3}}
,{{ 0, 0}, {0, 3}, {3, 0}}
,{{ 0, 0}, {0, 1}, {0, 4}}
,{{ 0, 0}, {0, 2}, {0, 4}}
,{{ 0, 0}, {1, 0}, {0, 4}}
,{{ 0, 0}, {2, 0}, {0, 4}}
,{{ 0, 0}, {0, 4}, {4, 0}}
,{{ 0, 2}, {2, 4}, {4, 0}}
,{{ 0, 0}, {0, 1}, {0, 5}}
,{{ 0, 0}, {0, 1}, {0, 6}}
,{{ 0, 0}, {0, 2}, {0, 6}}
,{{ 0, 0}, {0, 3}, {0, 6}}
,{{ 0, 0}, {1, 0}, {0, 6}}
,{{ 0, 0}, {2, 0}, {0, 6}}
,{{ 0, 0}, {3, 0}, {0, 6}}
,{{ 0, 0}, {0, 6}, {6, 0}}
,{{ 0, 0}, {0, 4}, {0, 8}}
};

double area(const dvec2 t[3])
{
  return 0.5 * fabs(determinant(dmat2(t[2]-t[0],t[1]-t[0])));
}

int cmp_area(const void *a, const void *b)
{
  double A = area((const dvec2 *) a);
  double B = area((const dvec2 *) b);
  return (A < B) - (A > B);
}

double distance_between_line_segments(dvec2 a0, dvec2 a1, dvec2 b0, dvec2 b1)
{
  const double eta = 1e-6;
  dvec2 r = b0 - a0, u = a1 - a0, v = b1 - b0;
  double ru = dot(r,u), rv = dot(r,v), uu = dot(u,u), uv = dot(u,v), vv = dot(v,v), det = uu*vv - uv*uv, s, t;
  if (det < eta*uu*vv)
  {
    s = clamp(ru/uu, 0.0, 1.0);
    t = 0;
  }
  else
  {
    s = clamp((ru*vv - rv*uv)/det, 0.0, 1.0);
    t = clamp((ru*uv - rv*uu)/det, 0.0, 1.0);
  }
  double S = clamp((t*uv + ru)/uu, 0.0, 1.0);
  double T = clamp((s*uv - rv)/vv, 0.0, 1.0);
  dvec2 A = a0 + S*u;
  dvec2 B = b0 + T*v;
  return distance(A, B);
}

double distance_between_triangles(dvec2 a[3], dvec2 b[3])
{
  double d  = distance_between_line_segments(a[0], a[1], b[0], b[1]);
  d = fmin(d, distance_between_line_segments(a[0], a[1], b[1], b[2]));
  d = fmin(d, distance_between_line_segments(a[0], a[1], b[2], b[0]));
  d = fmin(d, distance_between_line_segments(a[1], a[2], b[0], b[1]));
  d = fmin(d, distance_between_line_segments(a[1], a[2], b[1], b[2]));
  d = fmin(d, distance_between_line_segments(a[1], a[2], b[2], b[0]));
  d = fmin(d, distance_between_line_segments(a[2], a[0], b[0], b[1]));
  d = fmin(d, distance_between_line_segments(a[2], a[0], b[1], b[2]));
  d = fmin(d, distance_between_line_segments(a[2], a[0], b[2], b[0]));
  return d;
}

dvec2 T[26][3];

struct completed
{
};

void put(int i)
{
  if (i == 26)
  {
    throw completed{};
  }
  int w = 35;
  int h = 35;
  dvec2 origin = dvec2(w/2.0, h/2.0);
  double radius = 16;
  int x0 = rand() % w;
  int y0 = rand() % h;
  for (int y = 0; y < h; ++y)
  {
    for (int x = 0; x < w; ++x)
    {
      dvec2 o = dvec2((double) ((x0 + x) % w), (double) ((y0 + y) % h));
      T[i][0] = triangles[i][0] + o;
      T[i][1] = triangles[i][1] + o;
      T[i][2] = triangles[i][2] + o;
      bool valid = true;
      for (int j = 0; valid && j < 3; ++j)
      {
        if (distance(T[i][j], origin) > radius)
        {
          valid = false;
        }
      }
      for (int j = 0; valid && j < i; ++j)
      {
        if (distance_between_triangles(T[i], T[j]) < 2.5)
        {
          valid = false;
        }
      }
      if (valid)
      {
        put(i + 1);
      }
    }
  }
}

int main(int argc, char **argv)
{
  long long seed = atoll(argv[1]);
  fprintf(stderr, "%lld\n", seed);
  srand(seed);
  qsort(triangles, 26, 3 * sizeof(dvec2), cmp_area);
  try
  {
    put(0);
  }
  catch (const completed &e)
  {

  dvec2 mi = dvec2(1.0/0.0, 1.0/0.0), ma = dvec2(-1.0/0.0, -1.0/0.0);
  for (int i = 0; i < 26; ++i)
  {
    mi = min(mi, T[i][0]);
    mi = min(mi, T[i][1]);
    mi = min(mi, T[i][2]);
    ma = max(ma, T[i][0]);
    ma = max(ma, T[i][1]);
    ma = max(ma, T[i][2]);
  }

  int w = 3375;
  int h = 2625;
  int b = 225;
  double x1 = mi.x - 2;
  double y1 = mi.y - 2;
  double s = (h - b - b) / ((ma.y + 2) - (mi.y - 2));
  double x0 = (w - s * ((ma.x + 2) - (mi.x - 2))) / 2;
  double y0 = b;

  printf(
    "<svg width='%d' height='%d'>\n"
    "<style>\n"
    "#grid circle { fill: black: stroke; none; }\n"
    "#motifs circle { fill: red; stroke: black; stroke-width: 0.1; }\n"
    "#motifs path { fill: none; stroke: black; stroke-width: 0.1; }\n"
    "</style>\n"
    "<rect width='100%%' height='100%%' fill='white' stroke='none' />"
    "<g transform='translate(%g, %g) scale(%g, %g) translate(%g, %g)'>"
  , w, h, x0, y0, s, s, -x1, -y1
  );
  printf("<g id='grid'>\n");
  for (int j = mi.y - 2; j <= ma.y + 2; ++j)
  for (int i = mi.x - 2; i <= ma.x + 2; ++i)
  {
    printf("<circle cx='%d' cy='%d' r='0.05' />\n", i, j);
  }
  printf("</g><g id='motifs'>\n");
  for (int i = 0; i < 26; ++i)
  {
    printf("<g id='motif-%d'>\n", i);
    for (int k = 0; k < 3; ++k)
    {
      printf("<path d='M %d,%d L %d,%d' />\n", (int)T[i][k].x, (int)T[i][k].y, (int)T[i][(k+1)%3].x, (int)T[i][(k+1)%3].y);
    }
    for (int k = 0; k < 3; ++k)
    {
      printf("<circle cx='%d' cy='%d' r='0.4' />\n", (int)T[i][k].x, (int)T[i][k].y);
    }
    printf("</g>\n");
  }
  printf("</g></g></svg>\n");
  return 0;

  }
  return 1;
}
