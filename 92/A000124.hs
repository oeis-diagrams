-- oeis-diagrams -- unofficial diagrams of OEIS sequences
-- Copyright (C) 2016-2018 Claude Heiland-Allen
-- License CC-BY-NC <https://creativecommons.org/licenses/by-nc/3.0/>

-- https://oeis.org/A000124
-- a(n) is the maximal number of grandchildren of a binary vector of length
-- n+2. E.g., a binary vector of length 6 can produce at most 11 different
-- vectors when 2 bits are deleted.
--
-- a(13) = 92

{-# LANGUAGE FlexibleContexts #-}
import Diagrams.Prelude hiding (size)
import Diagrams.Backend.SVG.CmdLine (B, defaultMain)

import Control.Monad (replicateM)
import Data.List (sortBy, groupBy, transpose)
import Data.List.Split (chunksOf)
import Data.Ord (comparing)
import Data.Set (Set, fromList, toList, size)
import System.Random (StdGen, newStdGen, randomRs)

deletions :: [a] -> [[a]]
deletions [x] = [[]]
deletions (x:xs) = map (x:) (deletions xs) ++ [xs]

{-
n :: Int
n = 13

candidates :: [String]
candidates = replicateM (n + 2) "/\\"

equating :: Eq e => (a -> e) -> a -> a -> Bool
equating f a b = f a == f b

ancestor :: String
grandchildren :: [String]
(ancestor, grandchildren)
  = fmap toList
  . head
  . head
  . groupBy (equating (size . snd))
  . sortBy (flip $ comparing (size . snd))
  $ [ (c, s)
    | c <- candidates
    , let s = fromList
            . concatMap deletions
            . toList
            . fromList
            . deletions
            $ c
    ]
-}

ancestor :: String
grandchildren :: [String]
ancestor = "/\\/\\/\\/\\/\\/\\/\\/"
grandchildren
  = toList
  . fromList
  . concatMap deletions
  . toList
  . fromList
  . deletions
  $ ancestor

up, down :: V2 Double
up = r2 (1, 1)
down = r2 (1, -1)

wiggle :: String -> Diagram B
wiggle s
  = atop (strutY 3)
  . centerXY
  . lineCap LineCapRound
  . lineJoin LineJoinRound
  . lwL 1
  . lc (colour . rampage $ s)
  . strokeP
  . fromOffsets
  . map wig
  $ s

wig :: Char -> V2 Double
wig '/'  = up
wig '\\' = down

rampage :: String -> (Int, Int, Int)
rampage s =
  ( minimum $ scanl (+) 0 $ map ramp s
  , maximum $ scanl (+) 0 $ map ramp s
  , sum (map ramp s)
  )

ramp :: Char -> Int
ramp '/' = 1
ramp '\\' = -1

colour :: (Int, Int, Int) -> Colour Double
colour ( 0, 1,  1) = black
colour (-1, 0, -1) = black
colour ( 0, 2,  1) = sRGB24 0xe7 0xd9 0x40 -- yellow
colour (-2, 0, -1) = sRGB24 0x44 0xde 0xd5 -- cyan
colour (-1, 1,  1) = sRGB24 0x40 0x45 0xce -- blue
colour (-1, 1, -1) = sRGB24 0xbf 0x6d 0xe5 -- magenta
colour (-2, 1, -1) = sRGB24 0x50 0xc3 0x36 -- green
colour ( 0, 3,  3) = sRGB24 0xcf 0x0c 0x4c -- red
colour s = error (show s)

shuffle :: StdGen -> [a] -> [a]
shuffle g
  = map snd
  . sortBy (comparing fst)
  . zip (randomRs (0, 1 :: Double) g)

diagram :: StdGen -> Diagram B
diagram g
  = bg white
  . frame 4
  . centerXY
  . vsep (1 :: Double)
  . map
      ( centerXY
      . hsep (2 :: Double)
      . map wiggle
      )
  . chunksOf 4
  . shuffle g
  $ grandchildren

main :: IO ()
main = newStdGen >>= defaultMain . diagram
