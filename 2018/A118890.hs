-- oeis-diagrams -- unofficial diagrams of OEIS sequences
-- Copyright (C) 2016-2017 Claude Heiland-Allen
-- License CC-BY-NC <https://creativecommons.org/licenses/by-nc/3.0/>

-- https://oeis.org/A118890
-- Triangle read by rows: T(n,k) is the number of binary sequences of length n
-- containing k subsequences 0110 (n,k>=0).

-- a(25) = T(11,2) = 142
-- a(38) = T(14,2) = 2018

{-# LANGUAGE FlexibleContexts #-}
import Diagrams.Prelude hiding (zoom)
import Diagrams.Backend.SVG.CmdLine (B, defaultMain)

import Data.List (sort)
import System.Random (newStdGen, randoms)

binary :: Int -> [[Bool]]
binary 0 = [[]]
binary n = [ b:bs | b <- [False, True], bs <- binary_memo !! (n - 1) ]

binary_memo :: [[[Bool]]]
binary_memo = map binary [0..]

count0110 :: [Bool] -> Int
count0110 (False:True:True:rest@(False:_)) = 1 + count0110 rest
count0110 (_:rest) = count0110 rest
count0110 _ = 0

row :: Int -> Int -> [[Bool]]
row k = filter ((k ==) . count0110) . (binary_memo !!)

a118890_t :: Int -> Int -> Int
a118890_t k = length . row k

a118890_nk :: [((Int, Int), Int)]
a118890_nk = concat [ takeWhile ((/= 0) . snd) [ ((n, k), a118890_t k n) | k <- [0..] ] | n <- [0..] ]

a118890 :: [Int]
a118890 = map snd a118890_nk

chunking :: [Int] -> [a] -> [[a]]
chunking _ [] = []
chunking (n:ns) zs = let (xs, ys) = splitAt n zs in xs : chunking ns ys

colourize :: [Bool] -> [Int]
colourize (False:True:True:False:True:True:False:rest) = [1,1,1,3,2,2,2] ++ map (const 0) (colourize rest)
colourize (False:True:True:False:rest) = [1,1,1,1] ++ map (\x -> if x == 0 then x else 1 + x) (colourize rest)
colourize (_:rest) = 0 : colourize rest

colours = [grey, red, blue, yellow]
colour False = (cycle (map (blend 0.5 white) colours) !!)
colour True  = (cycle (map (blend 0.5 black) colours) !!)

bit b c
  = circle (1 :: Double)
  # centerXY
  # padX 1.2
  # lw thin
  # lc black
  # fc (colour b c)

bits bs = hcat $ zipWith bit bs (colourize bs)

diagram :: [Double] -> Diagram B
diagram g
  = bg white
  . padX (780 / 750)
  . pad 1.2
  . rotate (1/4 @@ turn)
  . centerXY
  . cat' unitX (with & sep .~ sqrt 3)
  . zipWith translateY (cycle [0, 2])
  . map (cat' unitY (with & sep .~ 2) . map bits)
  . chunking [36,35,36,35]
  . map snd . sort . zip g
  $ row k n
  where
    k = 2
    n = 11

main :: IO ()
main = do
  g <- newStdGen
  print g
  defaultMain . diagram . randoms $ g
