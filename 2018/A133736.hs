-- oeis-diagrams -- unofficial diagrams of OEIS sequences
-- Copyright (C) 2016-2017 Claude Heiland-Allen
-- License CC-BY-NC <https://creativecommons.org/licenses/by-nc/3.0/>

-- https://oeis.org/A133736
-- Number of graphs on n unlabeled nodes that have an Eulerian cycle, i.e., a
-- cycle that goes through every edge in the graph exactly once.

-- a(6) = 15
-- a(9) = 2018

{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeFamilies #-}
import Diagrams.Prelude
import Diagrams.Backend.SVG.CmdLine (B, defaultMain)

import Data.List (sort)
import System.Random (newStdGen, randoms)

chunk :: Int -> [a] -> [[a]]
chunk _ [] = []
chunk n zs = let (xs, ys) = splitAt n zs in xs : chunk n ys

cyclic ps = (ps, zip ps (drop 1 (cycle ps)))
bicyclic ps = zip ps (drop 2 (cycle ps))

graphs =
  [ case [origin] of ps -> (ps,[])
  , cyclic $ triangle (s*2)
  , cyclic $ square (s*2)
  , cyclic $ pentagon 2
  , case origin : square (s*2) of
      ps@[o,a,b,c,d] -> (ps, [(o,a),(o,b),(o,c),(o,d),(a,b),(c,d)])
  , case map p2 [(0,s),(0,-s),(-s,0),(sqrt 6 / 2,0),(sqrt 6, 0)] of
      ps@[u,d,l,m,r] -> (ps, [(u,l),(u,m),(u,r),(u,d),(d,l),(d,m),(d,r)])
  , case cyclic $ pentagon 2 of (ps, es) -> (ps, es ++ bicyclic ps)
  , cyclic $ hexagon 2
  , case square (s*2) ++ [origin, p2(s*2,0)] of
      ps@[a,b,c,d,o,r] -> (ps, [(c,d),(a,r),(b,r),(o,a),(o,b),(o,c),(o,d)])
  , case square (s*2) ++ [origin, p2(s*2,0)] of
      ps@[a,b,c,d,o,r] -> (ps, [(a,b),(b,c),(c,d),(d,a),(a,r),(b,r),(o,a),(o,b)])
  , case map p2 [(0,s),(0,-s),(-1.5*s,0),(-0.5*s,0),(0.5*s,0),(1.5*s,0)] of
      ps@[u,l,a,b,c,d] -> (ps, map ((,) u) [a,b,c,d] ++ map ((,) l) [a,b,c,d])
  , case cyclic $ hexagon 2 of
      (ps@[a,b,c,d,e,f], es) -> (ps, es ++ [(a,c),(c,e),(e,a)])
  , case hexagon 2 of
      ps@[a,r,b,c,l,d] -> (ps, [(a,b),(b,c),(c,d),(d,a),(a,c),(b,d),(a,r),(b,r),(c,l),(d,l)])
  , case cyclic $ hexagon 2 of
      (ps@[a,b,c,d,e,f], es) -> (ps, es ++ bicyclic (drop 1 ps))
  , case cyclic $ hexagon 2 of (ps, es) -> (ps, es ++ bicyclic ps)
  ]
  where
    s = sqrt 2

node es p
  = (circle r `at` p)
  # translateX r
  # strokeLocTrail
  # lc black
  # fc (blend 0.5 white ([yellow, blue, red] !! (multiplicity es p `div` 2)))
  where
    r = 0.5

multiplicity es p = length . filter (p ==) $ map fst es ++ map snd es

edge (p, q) = p ~~ q # lc black

graph :: ([P2 Double], [(P2 Double, P2 Double)]) -> Diagram B
graph (ps, es)
  = (mconcat (map (node es) ps) `atop` mconcat (map edge es))
  # centerXY `atop` strutX 6 `atop` strutY 6

diagram :: [Double] -> Diagram B
diagram g
  = bg white
  . pad 1.15
  . padY (750/600)
  . centerXY
  . vcat
  . map hcat
  . chunk 5
  . map graph
  . map snd . sort . zip g
  $ graphs

main :: IO ()
main = do
  g <- newStdGen
  print g
  defaultMain . diagram . randoms $ g
