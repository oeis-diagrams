-- oeis-diagrams -- unofficial diagrams of OEIS sequences
-- Copyright (C) 2016-2017 Claude Heiland-Allen
-- License CC-BY-NC <https://creativecommons.org/licenses/by-nc/3.0/>

-- https://oeis.org/A229915
-- Number of espalier polycubes of a given volume in dimension 3.

-- a(7)  = 34
-- a(20) = 2018

{-# LANGUAGE FlexibleContexts #-}
import Diagrams.Prelude hiding (cube)
import Diagrams.Backend.SVG.CmdLine (B, defaultMain)

import Control.Monad (guard)
import Data.List (nub, sort, sortOn)
import Data.Tuple (swap)
import System.Random (newStdGen, randoms)

partitions :: Int -> [[Int]]
partitions 0 = [[]]
partitions n
  = nub [ sort $ m : ms | m <- [1..n], ms <- partitions_memo !! (n - m) ]

partitions_memo :: [[[Int]]]
partitions_memo = map partitions [0..]

espaliers n = do
  a <- concat $ take (2 * n) partitions_memo
  b <- concat $ take (2 * n) partitions_memo
  guard (not (null a))
  guard (not (null b))
  guard (length a == length b)
  e <- pure $ combine (ferrers a) (ferrers b)
  guard (length e == n)
  pure e

ferrers = concat . zipWith (\i j -> map ((,) i) [1..j]) [1..] . reverse

combine as bs
  = nub $ sort [ (i, j, k) | (i, j) <- as, (i', k) <- bs, i == i' ]

a229915 = map (length . nub . map sort . espaliers) [0..]

chunking :: [Int] -> [a] -> [[a]]
chunking _ [] = []
chunking (n:ns) zs = let (xs, ys) = splitAt n zs in xs : chunking ns ys

withEnvelope' :: Diagram B -> Diagram B -> Diagram B
withEnvelope' = withEnvelope

cube (i, j, k)
  = translateX x
  . translateY y
   $ rhombus grey [o,a,b,c] `atop`
     rhombus red  [o,c,d,e] `atop`
     rhombus blue [o,e,f,a]
  where
    x = fromIntegral (j - k) * sqrt 3 / 2
    y = fromIntegral i - fromIntegral (j + k) * 0.5
    [o,a,b,c,d,e,f] = origin : hexagon 1 # map (rotate (1.5 / 6 @@ turn))
    rhombus c xs
      = fromVertices xs
      # mapLoc closeTrail
      # strokeLocTrail
      # lc black
      # fc (blend 0.5 white c)

draw
  = centerXY
  . mconcat
  . map cube
  . sortOn (\(i, j, k) -> (-i, -j, -k))


diagram :: [Double] -> Diagram B
diagram g
  = bg white
  . pad 1.2
  . padY (750/739)
  . centerXY
  . lineCap LineCapRound
  . lineJoin LineJoinRound
  . vcat
  . map (centerXY . hcat)
  . chunking [7,7,6,7,7]
  . map (withEnvelope' env . draw)
  . map snd . sort . zip g
  $ espaliers n
  where
    n = 7
    env = centerXY . mconcat . map draw . espaliers $ n


main :: IO ()
main = do
  g <- newStdGen
  print g
  defaultMain . diagram . randoms $ g
--  mapM_ print . zip [0..] . takeWhile (<= 2018) $ a229915
