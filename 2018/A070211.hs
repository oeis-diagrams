-- oeis-diagrams -- unofficial diagrams of OEIS sequences
-- Copyright (C) 2016-2017 Claude Heiland-Allen
-- License CC-BY-NC <https://creativecommons.org/licenses/by-nc/3.0/>

-- https://oeis.org/A070211
-- Number of compositions (ordered partitions) of n that are concave sequences.
-- Here, a finite sequence is concave if each term (other than the first or
-- last) is at least the average of the two adjacent terms. - Eric M. Schmidt,
-- Sep 29 2013

-- a(8) = 24
-- a(35) = 2018

{-# LANGUAGE FlexibleContexts #-}
import Diagrams.Prelude hiding (zoom)
import Diagrams.Backend.SVG.CmdLine (B, defaultMain)

import Data.List (sort, transpose)
import System.Random (newStdGen, randoms)

compositions :: Int -> [[Int]]
compositions 0 = [[]]
compositions n =
  [ c
  | m <- [1 .. n]
  , ms <- compositions_memo !! (n - m)
  , let c = m : ms
  , concave c
  ]

compositions_memo :: [[[Int]]]
compositions_memo = map compositions [0..]

concave :: [Int] -> Bool
concave [] = True
concave [_] = True
concave [_,_] = True
concave (a:bs@(b:c:_)) = 2 * b >= a + c

sequences :: Int -> [[Int]]
sequences n = compositions_memo !! n

a070211 :: [Int]
a070211 = map length compositions_memo

chunk :: Int -> [a] -> [[a]]
chunk _ [] = []
chunk n zs = let (xs, ys) = splitAt n zs in xs : chunk n ys

baseColours = [grey, red, blue, yellow]
lightColours = map (blend 0.5 white) baseColours
darkColours = map (blend 0.5 black) baseColours
colours = drop 7 . cycle . concat . transpose $ [lightColours, darkColours]

zoom f d = withEnvelope d (scale f d)

bar s m
  = (rect 0.75 (fromIntegral m :: Double) `atop` strutX 1)
  # scaleX s
  # lc black
  # fc (colours !! m)

barchart n xs
  = map (bar (fromIntegral n / fromIntegral (length xs))) xs
  # hcat
  # centerXY
  `atop` strutX (fromIntegral n)
  `atop` strutY (fromIntegral n)

diagram :: [Double] -> Diagram B
diagram g
  = bg white
  . zoom 1.2
  . padX (4 / 3)
  . padY (3 / 2)
  . centerXY
  . vcat
  . map (hcat . map (pad 1.2 . barchart n))
  . chunk 6
  . map snd . sort . zip g
  $ sequences n
  where
    n = 8

main :: IO ()
main = do
  g <- newStdGen
  print g
  defaultMain . diagram . randoms $ g
