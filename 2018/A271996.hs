-- oeis-diagrams -- unofficial diagrams of OEIS sequences
-- Copyright (C) 2016-2017 Claude Heiland-Allen
-- License CC-BY-NC <https://creativecommons.org/licenses/by-nc/3.0/>

-- https://oeis.org/A271996
-- The crystallogen sequence (a(n) = A018227(n)-4).

-- a(7)  = 114
-- a(21) = 2018

{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeFamilies #-}
import Diagrams.Prelude
import Diagrams.Backend.SVG.CmdLine (B, defaultMain)

shells = [2, 8, 8, 18, 18, 32, 32]
isLast = map (const False) (drop 1 shells) ++ [True]

nucleus = circle 0.25 # lc black # fc (blend 0.5 black blue)

hole p = (circle 0.25 `at` p) # translateX 0.25 # strokeLocTrail # fc (blend 0.5 white red)

electron p = (circle 0.125 `at` p) # translateX 0.125 # strokeLocTrail # fc (blend 0.5 white blue)

shell :: Int -> Int -> Bool -> Diagram B
shell i n b
  = (if odd i then rotate ((0.5 / fromIntegral n) @@ turn) else id)
  $ mconcat (zipWith ($) (if b then concat $ replicate 4 (hole : replicate ((n - 4) `div` 4) electron) else replicate n electron)
  (polygon (PolygonOpts (PolyRegular n r) NoOrient origin))) # lc black
  `atop` circle r # lc black
  where
    r = 4 * (fromIntegral (i + 2) / fromIntegral (length shells + 1))

diagram :: Diagram B
diagram
  = bg white
  . pad 1.2
  . padX (1000/750)
  . rotate (1 / 3 @@ turn)
  . centerXY
  . atop nucleus
  . mconcat
  $ zipWith3 shell [0..] shells isLast

main = defaultMain diagram
