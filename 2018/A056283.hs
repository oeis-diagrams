-- oeis-diagrams -- unofficial diagrams of OEIS sequences
-- Copyright (C) 2016-2017 Claude Heiland-Allen
-- License CC-BY-NC <https://creativecommons.org/licenses/by-nc/3.0/>

-- https://oeis.org/A056283
-- Number of n-bead necklaces with exactly three different colored beads.

-- a(4) = 30
-- a(8) = 2018

{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeFamilies #-}
import Diagrams.Prelude hiding (size, zoom)
import Diagrams.Backend.SVG.CmdLine (B, defaultMain)

import Control.Monad (replicateM)
import Data.List (inits, nub, sort, tails)
import System.Random (newStdGen, randoms)

rotations :: [a] -> [[a]]
rotations xs = zipWith (++) (tails xs) (inits xs)

canonical :: Ord a => [a] -> [a]
canonical xs = minimum (rotations xs)

isCanonical :: Ord a => [a] -> Bool
isCanonical xs = xs == canonical xs

isFull :: Eq a => Int -> [a] -> Bool
isFull k xs = length (nub xs) == k

sequences :: Int -> Int -> [[Int]]
sequences k n = replicateM n [1..k]

necklaces :: Int -> Int -> [[Int]]
necklaces k = filter (\xs -> isCanonical xs && isFull k xs) . sequences k

a056283 :: [Int]
a056283 = map (length . necklaces 3) [1..]

chunk :: Int -> [a] -> [[a]]
chunk _ [] = []
chunk n zs = let (xs, ys) = splitAt n zs in xs : chunk n ys

colours = cycle . map (blend 0.5 black) $ [red, yellow, blue]

bead n m p
  = (circle r `at` p)
  # strokeLocTrail
  # translateX r
  # lc black
  # fc (colours !! m)
  where
    r :: Double
    r = 2 / fromIntegral n

necklace n xs =
    zipWith (bead n) xs ps # mconcat `atop`
    unitCircle # lc black `atop`
    strutX size `atop`
    strutY (size * sqrt 3 / 2)
  where
    ps = polygon (PolygonOpts (PolyRegular n 1) OrientH origin)

size = 3.5

zoom f d = withEnvelope d (scale f d)

diagram :: [Double] -> Diagram B
diagram g
  = bg white
  . zoom 1.2
  . padX (4 / 3)
  . padY (3 / 2)
  . centerXY
  . vcat
  . zipWith translateX (cycle [0, size / 2])
  . map (hcat . map (necklace n))
  . chunk 6
  . (\xs -> zipWith (\r ys -> cycle (rotations ys) !! floor (fromIntegral n * r)) (drop (length xs) g) xs)
  . map snd . sort . zip g
  $ necklaces k n
  where
    k = 3
    n = 5

main :: IO ()
main = do
  g <- newStdGen
  print g
  defaultMain . diagram . randoms $ g
--  mapM_ print . zip [0..] . takeWhile (<= 2018) $ a056283
