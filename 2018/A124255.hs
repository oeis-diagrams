-- oeis-diagrams -- unofficial diagrams of OEIS sequences
-- Copyright (C) 2016-2017 Claude Heiland-Allen
-- License CC-BY-NC <https://creativecommons.org/licenses/by-nc/3.0/>

-- https://oeis.org/A124255
-- Forest-and-trees problem: square of distance to most distant visible tree.

-- a(8)  = 61   (most distant tree at (5, 6))
-- a(45) = 2018

{-# LANGUAGE FlexibleContexts #-}
import Diagrams.Prelude
import Diagrams.Backend.SVG.CmdLine (B, defaultMain)

import Data.Maybe (catMaybes)

n :: Double
n = 8

resolution :: Int
resolution = 4096

trees1 :: Diagram B
trees1 = mconcat
  [ (circle (1 / n) `at` p2 (i + 1 / n, j)) # strokeLocTrail
  | i <- [-n .. n]
  , j <- [-n .. n]
  , i /= 0 || j /= 0
  ]

trees2 :: Diagram B
trees2 = mconcat
  [ (circle (1 / n) `at` p2 (i + 1 / n, j))
  # strokeLocTrail
  # lw thin
  # if visible (p2(i, j))
    then lc black . fc (blend 0.5 black red)
    else lc (blend 0.5 black grey) . fc (blend 0.5 white grey)
  | i <- [-n .. n]
  , j <- [-n .. n]
  , i /= 0 || j /= 0
  ]

visible p = any ((< 0.5) . distance p) boundary

boundary = catMaybes
  [ rayTraceV origin (angleV $ t @@ turn) trees1
  | i <- [0 .. resolution - 1]
  , let t = fromIntegral i / fromIntegral resolution
  ] # map (`translate` origin)

visibility :: Diagram B
visibility
  = boundary
  # fromVertices
  # mapLoc closeTrail
  # strokeLocTrail
  # lw veryThin
  # lc black
  # fc (blend 0.5 white blue)

observer
  = circle (1 /n)
  # lw thin
  # lc black
  # fc (blend 0.5 black blue)

diagram :: Diagram B
diagram
  = bg white
  . pad 1.2
  . padX (4 / 3)
  $ observer `atop` trees2 `atop` visibility

main :: IO ()
main = defaultMain diagram
