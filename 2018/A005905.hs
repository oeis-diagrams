-- oeis-diagrams -- unofficial diagrams of OEIS sequences
-- Copyright (C) 2016-2017 Claude Heiland-Allen
-- License CC-BY-NC <https://creativecommons.org/licenses/by-nc/3.0/>

-- https://oeis.org/A005905
-- Number of points on surface of truncated tetrahedron: 14n^2 + 2 for n>0,
-- a(0)=1.

-- a(4)  = 226
-- a(12) = 2018

{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeFamilies #-}
import Diagrams.Prelude
import Diagrams.Backend.SVG.CmdLine (B, defaultMain)

triUp, triDown, hex :: [(Int, Int)]
triUp   = [ (0,0), (2,0), (1,1) ]
triDown = [ (0,0), (1,1), (-1,1) ]
hex     = [ (0,0), (2,0), (3,1), (2,2), (0,2), (-1,1) ]

net :: [((Int, Int), [(Int, Int)])]
net =
  [ (( 0,0), hex)
  , (( 3,1), hex)
  , (( 6,0), hex)
  , (( 9,1), hex)
  , (( 0,2), triUp)
  , (( 4,0), triDown)
  , (( 6,2), triUp)
  , ((10,0), triDown)
  ]

tabs :: [((Int, Int), (Int, Int))]
tabs =
  [ (( 0,0), ( 2,0))
  , (( 3,1), ( 4,0))
  , (( 5,1), ( 6,0))
  , (( 8,0), ( 9,1))
  , ((10,0), (11,1))
  , ((12,2), (11,3))
  , (( 9,3), ( 8,2))
  , (( 7,3), ( 6,2))
  , (( 5,3), ( 3,3))
  , (( 2,2), ( 1,3))
  , (( 0,2), (-1,1))
  ]

point :: (Int, Int) -> Point V2 Double
point (i, j) = p2 (fromIntegral i, fromIntegral j * sqrt 3)

tabVertices :: Point V2 Double -> Point V2 Double -> [Point V2 Double]
tabVertices p1 p2 = [p1,p4,p3,p2]
  where
    u = scale (1/6) (p2 .-. p1)
    p3 = translate (rotate (-2/6 @@ turn) u) p2
    p4 = translate (rotate (-1/6 @@ turn) u) p1

outline :: Path V2 Double
outline
  = fromVertices $ concat [ [point a, point b] | (a, b) <- tabs ]

drawTab :: Point V2 Double -> Point V2 Double -> Diagram B
drawTab p1 p2
  = fromVertices (tabVertices p1 p2)
  # mapLoc closeTrail
  # strokeLocTrail
  # lw thin
  # lc black
  # fc (blend 0.5 white grey)

drawShape :: (Int, Int) -> [(Int, Int)] -> Diagram B
drawShape p s
  = fromVertices (map point s)
  # closeTrail
  # (`at` point p)
  # strokeLocTrail
  # lw thin
  # lc black
  # fc (blend 0.5 white (if length s > 3 then blue else red))

drawTabs :: Diagram B
drawTabs
  = mconcat [ drawTab (point a) (point b) | (a, b) <- tabs]

drawNet :: Diagram B
drawNet
  = mconcat [ drawShape p s | (p, s) <- net]

drawPattern :: Int -> Diagram B
drawPattern n
  = mconcat
    [ (circle 0.25 `at` point (i + (j `mod` 2), j))
    # translateX 0.25
    # strokeLocTrail
    # lc black
    # fc (blend 0.5 black red)
    | i <- [-n,-n + 2 .. 12 * n]
    , j <- [ 0 ..  3 * n]
    ]
  # scale (1 / fromIntegral (n - 1))
  # clipped outline

diagram :: Int -> Diagram B
diagram n
  = mconcat [ drawPattern n, drawNet, drawTabs ]
  # lineCap LineCapRound
  # lineJoin LineJoinRound
  # centerXY
  # padY (750/433)
  # pad 1.1
  # bg white

main :: IO ()
main = defaultMain (diagram 4)
