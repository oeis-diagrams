-- oeis-diagrams -- unofficial diagrams of OEIS sequences
-- Copyright (C) 2016-2017 Claude Heiland-Allen
-- License CC-BY-NC <https://creativecommons.org/licenses/by-nc/3.0/>

-- https://oeis.org/A267255
-- Decimal representation of the n-th iteration of the "Rule 111" elementary
-- cellular automaton starting with a single ON (black) cell.

-- a(5) = 2018

{-# LANGUAGE FlexibleContexts #-}
import Diagrams.Prelude
import Diagrams.Backend.SVG.CmdLine (B, defaultMain)

import Control.Monad (replicateM)

data Cell = O | I

rule :: [Cell] -> [Cell]
rule (I:xs@(I:I:_)) = O : rule xs
rule (I:xs@(I:O:_)) = I : rule xs
rule (I:xs@(O:I:_)) = I : rule xs
rule (I:xs@(O:O:_)) = O : rule xs
rule (O:xs@(_:_:_)) = I : rule xs
rule _ = []

initial :: (Cell, [Cell])
initial = (O, [I])

step :: (Cell, [Cell]) -> (Cell, [Cell])
step (c, xs) = (case c of I -> O ; O -> I, rule ([c,c] ++ xs ++ [c,c]))

history :: [[Cell]]
history = map snd . take 15 $ iterate step initial

cell :: Bool -> Cell -> Diagram B
cell b O = circle 1 # pad 1.2 # lc black # fc (colour b blue)
cell b I = circle 1 # pad 1.2 # lc black # fc (colour b red)

colour b = blend 0.5 (if b then black else white)

key :: [Cell] -> Diagram B
key xs@[a,b,c]
  = (centerX (cell False a ||| cell False b ||| cell False c)
     ===
     centerX (cell True (head (rule xs))))
  # centerXY # padX 1.2

legend :: Diagram B
legend = centerXY . hcat . map key . replicateM 3 $ [I,O]

diagram
  = bg white
  . pad 1.2
  . padY (750/672)
  . centerXY
  . (legend ===)
  . (strutY 6 ===)
  . centerXY
  . vcat
  . zipWith (\n -> centerX . hcat . map (cell (n == 5))) [0..]
  $ history

main = defaultMain diagram
