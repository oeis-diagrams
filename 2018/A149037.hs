-- oeis-diagrams -- unofficial diagrams of OEIS sequences
-- Copyright (C) 2016-2017 Claude Heiland-Allen
-- License CC-BY-NC <https://creativecommons.org/licenses/by-nc/3.0/>

-- https://oeis.org/A149037
-- Number of walks within N^3 (the first octant of Z^3) starting at (0,0,0) and
-- consisting of n steps taken from {(-1, -1, 0), (-1, 0, 1), (0, 1, 1),
-- (1, -1, 1), (1, 0, -1)}.

-- a(4) = 35
-- a(7) = 2018

{-# LANGUAGE FlexibleContexts #-}
import Diagrams.Prelude hiding (project, translation)
import Diagrams.Backend.SVG.CmdLine (B, defaultMain)

import Control.Monad (guard)
import Data.List (sortOn, transpose)
import System.Random (newStdGen, randoms)

steps :: [V3 Int]
steps = [v (-1) (-1) 0, v (-1) 0 1, v 0 1 1, v 1 (-1) 1, v 1 0 (-1)]
  where
    v = mkR3

walk :: V3 Int -> [V3 Int]
walk p = do
  step <- steps
  p <- pure $ p ^+^ step
  guard (view _x p >= 0)
  guard (view _y p >= 0)
  guard (view _z p >= 0)
  pure p

extend :: [V3 Int] -> [[V3 Int]]
extend ps@(p:_) = do
  q <- walk p
  pure (q:ps)

initial :: [[V3 Int]]
initial = [[mkR3 0 0 0]]

walks :: [[[V3 Int]]]
walks = iterate (concatMap extend) initial

grid :: [V3 Int]
grid = [ mkR3 x y z | x <- [0 .. 3], y <- [0 .. 3], z <- [0 .. 3] ]

chunk :: Int -> [a] -> [[a]]
chunk _ [] = []
chunk n zs = let (xs, ys) = splitAt n zs in xs : chunk n ys

project r p = (z - 12, p2(u, v))
  where
    x = fromIntegral $ view _x p
    y = fromIntegral $ view _y p
    z = fromIntegral $ view _z p
    q = [x, y, z, 1]
    [a,b,c,d] = transformation r `mv` q
    [u,v,w] = map (/d) [a,b,c]

transformation :: Bool -> [[Double]]
transformation r
  = perspective `mm`
    translation 0 0 (-10) `mm`
    rotationX (-0.2) `mm`
    rotationY (if r then 1 + 0.1 else 1 - 0.1) `mm`
    translation (-2) (-2) (-2)

perspective :: [[Double]]
perspective
  = [ [ 1 / (ratio * tan_half_angle), 0, 0, 0 ]
    , [ 0, 1 / tan_half_angle, 0, 0 ]
    , [ 0, 0, -(far + near) / (far - near), -2 * (far * near) / (far - near) ]
    , [ 0, 0, -1, 0]
    ]
  where
    tan_half_angle = tan (angle / 2)
    near = 0.1
    far = 20
    angle = pi / 8
    ratio = 1

rotationX :: Double -> [[Double]]
rotationX t
  = [ [ 1, 0, 0, 0 ], [ 0, c, s, 0 ], [ 0, -s, c, 0 ], [ 0, 0, 0, 1 ] ]
  where
    c = cos t
    s = sin t

rotationY :: Double -> [[Double]]
rotationY t
  = [ [ c, 0, s, 0 ], [ 0, 1, 0, 0 ], [ -s, 0, c, 0 ], [ 0, 0, 0, 1 ] ]
  where
    c = cos t
    s = sin t

translation :: Double -> Double -> Double -> [[Double]]
translation x y z
  = [ [ 1, 0, 0, x ]
    , [ 0, 1, 0, y ]
    , [ 0, 0, 1, z ]
    , [ 0, 0, 0, 1 ]
    ]

vv :: [Double] -> [Double] -> Double
vv a b = sum $ zipWith (*) a b

mv :: [[Double]] -> [Double] -> [Double]
mv a b = map (`vv` b) a

mm :: [[Double]] -> [[Double]] -> [[Double]]
mm a b = [ [ u `vv` v | v <- transpose b ] | u <- a ]

path :: [V3 Int] -> Diagram B
path w
  = pad 1.2
  . withEnvelope' (centerXY $ fromVertices
      (map (snd . project False) grid ++ map (snd . project True) grid))
  . centerXY
  $ fromVertices (map (snd . project False) w) # lc (blend 0.5 black red) `atop`
    fromVertices (map (snd . project True ) w) # lc (blend 0.5 black blue)

withEnvelope' :: Diagram B -> Diagram B -> Diagram B
withEnvelope' = withEnvelope

diagram :: [Double] -> Diagram B
diagram g
  = bg white
  . pad 1.1
  . padY (750/663)
  . centerXY
  . vcat
  . map (hcat . map path)
  . chunk 7
  . map snd . sortOn fst . zip g
  $ walks !! 4

main :: IO ()
main = do
  g <- newStdGen
  print g
  defaultMain . diagram . randoms $ g
