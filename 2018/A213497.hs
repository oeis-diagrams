-- oeis-diagrams -- unofficial diagrams of OEIS sequences
-- Copyright (C) 2016-2017 Claude Heiland-Allen
-- License CC-BY-NC <https://creativecommons.org/licenses/by-nc/3.0/>

-- https://oeis.org/A213497
-- Number of (w,x,y) with all terms in {0,...,n} and w = min(|w-x|,|x-y|).

-- a(5)  = 40
-- a(40) = 2018

{-# LANGUAGE FlexibleContexts #-}
import Diagrams.Prelude
import Diagrams.Backend.SVG.CmdLine (B, defaultMain)

import Data.List (sort)
import System.Random (newStdGen, randoms)

tuples :: Int -> [(Int, Int, Int)]
tuples n =
  [ (w, x, y)
  | w <- [0 .. n]
  , x <- [0 .. n]
  , y <- [0 .. n]
  , w == min (abs (w - x)) (abs (x - y))
  ]

chunk :: Int -> [a] -> [[a]]
chunk _ [] = []
chunk n zs = let (xs, ys) = splitAt n zs in xs : chunk n ys

colours =
  [ l grey
  , d grey
  , l blue
  , l red
  , d blue
  , d red
  ]
  where
    d = blend 0.5 white
    l = blend 0.5 black

draw :: Int -> (Int, Int, Int) -> Diagram B
draw n (w, x, y)
  = (circle (r w) # fc (colours !! w) |||
     circle (r x) # fc (colours !! x) |||
     circle (r y) # fc (colours !! y))
  # centerXY
  # atop (strutX (4 * r n + 6))
  # atop (strutY (2 * r n + 6))
  # rotate (1/7 @@ turn)
  where
    r t = 0.5 + fromIntegral t

diagram :: [Double] -> Diagram B
diagram g
  = bg white
  . pad 1.2
  . padX (777/750)
  . centerXY
  . vcat
  . map hcat
  . chunk 8
  . map (draw n)
  . map snd . sort . zip g
  $ tuples n
  where
    n = 5

main :: IO ()
main = do
  g <- newStdGen
  print g
  defaultMain . diagram . randoms $ g
