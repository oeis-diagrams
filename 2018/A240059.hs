-- oeis-diagrams -- unofficial diagrams of OEIS sequences
-- Copyright (C) 2016-2017 Claude Heiland-Allen
-- License CC-BY-NC <https://creativecommons.org/licenses/by-nc/3.0/>

-- https://oeis.org/A240059
-- Number of partitions of n such that m(1) > m(3), where m = multiplicity.

-- a(12) = 46
-- a(27) = 2018

{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeFamilies #-}
import Diagrams.Prelude
import Diagrams.Backend.SVG.CmdLine (B, defaultMain)

import Data.List (nub, sort, zipWith4)
import System.Random (newStdGen, randoms)

partitions :: Int -> [[Int]]
partitions 0 = [[]]
partitions n
  = nub [ sort $ m : ms | m <- [1..n], ms <- partitions_memo !! (n - m) ]

partitions_memo :: [[[Int]]]
partitions_memo = map partitions [0..]

multiplicity :: Int -> [Int] -> Int
multiplicity k = length . filter (k ==)

a240059_ps :: [[[Int]]]
a240059_ps
  = map (filter (\xs -> multiplicity 1 xs > multiplicity 3 xs)) partitions_memo

a240059 = map length a240059_ps

chunking :: [Int] -> [a] -> [[a]]
chunking _ [] = []
chunking (n:ns) zs = let (xs, ys) = splitAt n zs in xs : chunking ns ys

colour' b = blend 0.5 (if b then black else white)
colour 1 b = colour' b red
colour 3 b = colour' b blue
colour _ b = colour' b grey

pieChart :: Int -> [Int] -> Diagram B
pieChart n xs
  = mconcat ws
  # centerXY
  # pad padding
  where
    ys = scanl (+) 0 xs
    ts = map (\y -> fromIntegral y / fromIntegral n) ys
    ws = zipWith4 w ts (tail ts) xs (cycle [True, False])
    w lo hi m b
      = wedge 1 (rotate (lo @@ turn) xDir) ((hi - lo) @@ turn)
      # lc black
      # lineCap LineCapRound
      # lineJoin LineJoinRound
      # fc (colour m b)

padding = 1.2

diagram :: [Double] -> Diagram B
diagram g
  = bg white
  . pad padding
  . padY (750/706)
  . centerXY
  . vcat
  . zipWith translateX ([padding, 0, padding, 0, padding, 2 * padding])
  . map hcat
  . chunking [7, 8, 8, 8, 8, 7]
  . map (pieChart n)
  . map snd . sort . zip g
  $ a240059_ps !! n
  where
    n = 12

main :: IO ()
main = do
  g <- newStdGen
  print g
  defaultMain . diagram . randoms $ g
--  mapM_ print . zip [0..] . takeWhile (<= 2018) $ a240059
