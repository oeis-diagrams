-- oeis-diagrams -- unofficial diagrams of OEIS sequences
-- Copyright (C) 2016-2017 Claude Heiland-Allen
-- License CC-BY-NC <https://creativecommons.org/licenses/by-nc/3.0/>

-- https://oeis.org/A121551
-- Number of parts in all the compositions of n into Fibonacci numbers.

-- a(8)  = 457  (number of composition is 94)
-- a(10) = 2018

{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeFamilies #-}
import Diagrams.Prelude
import Diagrams.Backend.SVG.CmdLine (B, defaultMain)

import Data.List (sort, transpose)
import Data.Maybe (fromJust)
import System.Random (newStdGen, randoms)

fibs :: [Int]
fibs = 0 : 1 : zipWith (+) fibs (tail fibs)

compositions :: Int -> [[Int]]
compositions 0 = [[]]
compositions n =
  [ m : ms
  | m <- takeWhile (<= n) (drop 2 fibs)
  , ms <- compositions_memo !! (n - m)
  ]

compositions_memo :: [[[Int]]]
compositions_memo = map compositions [0..]

a121551 :: [Int]
a121551 = map (length . concat) compositions_memo

chunking :: [Int] -> [a] -> [[a]]
chunking _ [] = []
chunking (n:ns) zs = let (xs, ys) = splitAt n zs in xs : chunking ns ys

baseColours = [grey, red, blue, yellow]
lightColours = map (blend 0.5 white) baseColours
darkColours = map (blend 0.5 black) baseColours
colours = [ blend 0.5 white grey, blend 0.5 black red, blend 0.5 white blue, blend 0.5 black blue, blend 0.5 white red]

colour n = fromJust . lookup n . zip (drop 2 fibs) $ colours

bar s n
  = rect (s * fromIntegral n) 1
  # lc black
  # fc (colour n)

bars ns = centerXY . cat' unitX (with & sep .~ d) . map (bar s) $ ns
  where
    d = 0.5
    n = sum ns
    m = length ns
    s = fromIntegral n / (fromIntegral (m - 1) * d + fromIntegral n)

diagram :: [Double] -> Diagram B
diagram g
  = bg white
  . padX (779 / 750)
  . pad 1.2
  . centerXY
  . cat' unitX (with & sep .~ sqrt 3)
  . zipWith translateY (4 : cycle [2, 0])
  . map (cat' unitY (with & sep .~ 2) . map bars)
  . chunking [15, 16, 16, 16, 16, 15]
  . map snd . sort . zip g
  $ compositions_memo !! 8

main :: IO ()
main = do
  g <- newStdGen
  print g
  defaultMain . diagram . randoms $ g
--  mapM_ print . zip [0..] . takeWhile (<= 2018) $ a121551
