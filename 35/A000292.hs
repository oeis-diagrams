-- oeis-diagrams -- unofficial diagrams of OEIS sequences
-- Copyright (C) 2016-2017 Claude Heiland-Allen
-- License CC-BY-NC <https://creativecommons.org/licenses/by-nc/3.0/>

-- https://oeis.org/A000292
-- The number of (n+2)-bit numbers which contain two runs of 1's in their binary
-- expansion. - Vladimir Shevelev, Jul 30 2010

{-# LANGUAGE FlexibleContexts #-}
import Diagrams.Prelude hiding (parts, size)
import Diagrams.Backend.SVG.CmdLine (B, defaultMain)
import Diagrams.TwoD.Arc (arcT)

import Data.List (sort, transpose)
import Data.List.Split (chunksOf)

parts n =
  [ draw 
      [ replicate b True
      , replicate c False
      , replicate d True
      , replicate e False
      ]
  `atop` strutXY (size * 5)
  | a <- [0]
  , b <- [1 .. n - 1 - a]
  , c <- [1 .. n - 1 - a - b]
  , d <- [1 .. n - a - b - c]
  , let e = n - a - b - c - d
  ]

size :: Double
size = 3

grid = vcat . map hcat

cell True  = circle 1 # fcA (red `withOpacity` 0.5) # lc black `atop` strutXY size
cell False = circle 1 # fc white # lc black `atop` strutXY size

draw = centerXY . rotate (1/5 @@ turn) . centerXY . vcat . map (centerXY . (`atop` strutXY size) . hcat . map cell)

strutXY x = strutX x `atop` strutY x

diagram m n
  = lw thick
  . bg white
  . centerXY
  . grid
  . chunksOf m
  $ parts (n + 2)

main = defaultMain (diagram 5 5)
