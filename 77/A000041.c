// https://oeis.org/A000041
// a(n) is the number of partitions of n (the partition numbers).
// a(n) = 1, 1, 2, 3, 5, 7, 11, 15, ...
// a(12) = 77

#include <math.h>
#include <stdio.h>

#define N 12
#define W 7
#define H 11
#define theta (2 * 3.14159265358979 / N)
#define phi (2 * 3.14159265358979 * 0.618033988749895)
#define dpi 600

int count = 0;
int parts[N];
int x = 0, y = 0;
int o = 0;

void partition(int number, int largest)
{
  if (number == 0)
  {
    int sum = 0;
    for (int ix = 0; ix < count; ++ix)
    {
      int part = parts[ix];
      int start = sum;
      sum += part;
      int end = sum;
      printf("<path class='p%do%d' d='M %g,%g ", part, o, (x + 1.), (y + 1) - 0.4 * (count == 1));
      o = ! o;
      for (int t = start + (count == 1); t <= end; ++t)
      {
        printf("L %g,%g ", (x + 1) + 0.4 * sin(theta * t), (y + 1) - 0.4 * cos(theta * t));
      }
      printf("Z' />\n");
    }
    if (++x == W)
    {
      x = 0;
      ++y;
    }
  }
  else
  {
    for (int part = largest; part > 0; --part)
    {
      if (number >= part)
      {
        parts[count++] = part;
        partition(number - part, part);
        --count;
      }
    }
  }
}

int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  printf("<svg width='%d' height='%d'>\n", (W + 1) * dpi, (H + 1) * dpi);
  printf("<style>\npath { stroke: black; stroke-width: 0.01; stroke-linejoin: round; }\n");
  for (int part = 1; part <= N; ++part)
  {
    int h = 360 * (((part - 1) * 7) % N) / N;
    for (int o = 0; o < 2; ++o)
    {
      printf(".p%do%d { fill: hsl(%d, 70%%, 70%%); fill-opacity: %g; }\n", part, o, h, 0.5 + 0.125 * (o - 0.5));
    }
  }
  printf("</style>\n");
  printf("<rect width='100%%' height='100%%' stroke='none' fill='white' />\n");
  printf("<g transform='scale(%d,%d)'>\n", dpi, dpi);
  o = 0;
  partition(N, N);
  printf("</g></svg>\n");
  return 0;
}
