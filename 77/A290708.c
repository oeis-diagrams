/*
A290708
Number of irredundant sets in the (2 n)-crossed prism graph.
a(2) = 77
*/

#include <stdio.h>

int neighbourhood(int S)
{
  int N = 0;
  for (int v = 0; v < 8; ++v)
  {
    if (S & (1 << v))
    {
      int x0 = v & (1 << 0);
      int y0 = v & (1 << 1);
      int z0 = v & (1 << 2);
      for (int x = 0; x < 2; ++x)
        N |= 1 << ((x << 0) | y0 | z0);
      for (int y = 0; y < 2; ++y)
        N |= 1 << (x0 | (y << 1) | z0);
      for (int z = 0; z < 2; ++z)
        N |= 1 << (x0 | y0 | (z << 2));
    }
  }
  return N;
}

int irredundant(int S)
{
  for (int v = 0; v < 8; ++v)
  {
    if (! (S & (1 << v))) continue;
    if (neighbourhood(S) == neighbourhood(S & ~ (1 << v))) return 0;
  }
  return 1;
}

void project(int x0, int y0, int x, int y, int z, int *u, int *v)
{
  *u = 360 + 720 * (x0 + 0.5 + 0.707106781186548 * (x - 0.5) / (z + 1));
  *v = 360 + 720 * (y0 + 0.5 + 0.707106781186548 * (y - 0.5) / (z + 1));
}

void line(int x0, int y0, int x1, int y1, int z1, int x2, int y2, int z2)
{
  int u1, v1, u2, v2;
  project(x0, y0, x1, y1, z1, &u1, &v1);
  project(x0, y0, x2, y2, z2, &u2, &v2);
  printf("<path d='M %d,%d L %d,%d' />\n", u1, v1, u2, v2);
}

void circle(int x0, int y0, int x, int y, int z, int r)
{
  int u, v;
  project(x0, y0, x, y, z, &u, &v);
  r *= 30;
  r /= 1.5;
  printf("<circle cx='%d' cy='%d' r='%d' />\n", u, v, r);
}

void draw(int S, int x0, int y0)
{
  for (int x = 0; x < 2; ++x)
  for (int y = 0; y < 2; ++y)
    line(x0, y0, x, y, 0, x, y, 1);
  for (int y = 0; y < 2; ++y)
  for (int z = 0; z < 2; ++z)
    line(x0, y0, 0, y, z, 1, y, z);
  for (int x = 0; x < 2; ++x)
  for (int z = 0; z < 2; ++z)
    line(x0, y0, x, 0, z, x, 1, z);
  int N = neighbourhood(S);
  for (int v = 0; v < 8; ++v)
  {
    if (N & (1 << v))
      circle(x0, y0, !!(v & (1 << 0)), !!(v & (1 << 1)), !!(v & (1 << 2)), 1);
    if (S & (1 << v))
      circle(x0, y0, !!(v & (1 << 0)), !!(v & (1 << 1)), !!(v & (1 << 2)), 2);
  }
}

int main(int argc, char **argv)
{
  printf("<svg xmlns='http://www.w3.org/2000/svg' width='5760' height='8640'>\n");
  printf("<rect width='100%%' height='100%%' stroke='none' fill='white' />\n");
  printf("<g stroke='black' fill='none' stroke-width='3' stroke-linecap='round'>\n");
  int x = 0, y = 0;
  for (int S = 0; S < 1 << 8; ++S)
  {
    if (irredundant(S))
    {
      draw(S, x, y);
      if (++x == 7)
      {
        x = 0;
        ++y;
      }
    }
  }
  printf("</g>\n</svg>\n");
  return 0;
}
