-- number of partitions of n into distinct odd parts
-- <https://oeis.org/A000700>

import Control.Monad (join)
import Data.Char (chr, ord)
import Data.List (tails, transpose)

generate :: Int -> [Int] -> [Int] -> [[Int]]
generate n parts accum
  | n == 0 = [accum]
  | n < 0 = []
  | null parts = []
  | otherwise = concat
      [ generate (n - part) more (part:accum)
      | (part:more) <- tails parts
      ]

character :: Int -> Char
character n = chr (ord 'a' + (n `div` 2))

main :: IO ()
main
  = putStr . unlines . transpose
  . map (map character . concatMap (join replicate) . reverse)
  . generate 46 [1,3..45]
  $ []
