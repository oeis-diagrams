// gcc A000700.c `pkg-config --cflags --libs cairo-pdf` -lm

#include <math.h>
#include <stdio.h>
#include <cairo.h>
#include <cairo-pdf.h>

#define pi 3.141592653589793

int main(int argc, char **argv)
{
  const char *filename = "A000700.pdf";
  double page_w = 108 / 25.4 * 72;
  double page_h = 155 / 25.4 * 72;
  double page_border = 16 / 25.4 * 72;
  double image_w = 46;
  double image_h = 72;
  double scale = (page_w - page_border) / image_w;
  cairo_surface_t *pdf = cairo_pdf_surface_create(filename, page_h, page_w);
  cairo_t *cr = cairo_create(pdf);
  cairo_translate(cr, page_h / 2, page_w / 2);
  cairo_rotate(cr, -pi / 2);
  cairo_scale(cr, scale, scale);
  cairo_translate(cr, -image_w / 2, -image_h / 2);
  cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND);
  cairo_set_line_join(cr, CAIRO_LINE_JOIN_ROUND);
  cairo_set_line_width(cr, 0.1);
  cairo_set_source_rgba(cr, 0, 0, 0, 1);
  for (int j = 0; j < 46; ++j)
  {
    for (int i = 0; i < 72; ++i)
    {
      int k = fgetc(stdin) - 'a';
      double r = 0.5 * 0.8;
      double t = pi * (k + 0.5) / 23.0;
      double x = j + 0.5;
      double y = i + 0.5;
      double dx = r * cos(t);
      double dy = r * sin(t);
      double x0 = x - dx;
      double y0 = y - dy;
      double x1 = x + dx;
      double y1 = y + dy;
      cairo_move_to(cr, x0, y0);
      cairo_line_to(cr, x1, y1);
    }
    fgetc(stdin); // '\n'
  }
  cairo_stroke(cr);
  cairo_show_page(cr);
  cairo_destroy(cr);
  cairo_surface_destroy(pdf);
  return 0;
}
