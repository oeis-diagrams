-- number of multigraphs with loops on 2 nodes with 15 edges
-- https://oeis.org/A002620

graphs n =
  [ (a, b, c)
  | a <- [0..n]
  , b <- [0..n]
  , c <- [0..n]
  , a + b + c == n
  , a <= c
  , a > 0 || b > 1 || c > 0
  ]

fx = 64
fy = 32

graph (u, v) (a, b, c)
  =  "<g transform='translate(" ++ dist (fx * (b + 1 - (if c < 8 then 8 - c else 0) + (if odd (round c) then 0.5 else 0) + fromIntegral ((round c - 8) `div` 2))) ++ "," ++ dist (fy * (c + 1)) ++ ")'>\n"
  ++ concat [ topLoop (r + 1) | r <- [1 .. a] ]
  ++ concat [ betweenCurve x | x <- [ -b + 1, -b + 3 .. b - 1 ] ]
  ++ concat [ bottomLoop (r + 1) | r <- [1 .. c] ]
  ++ nodes
  ++ "</g>\n"

dist = coord
coord x = show (round (10 * x) :: Int)

r0 = 1
y0 = 16

nodes =  "<circle cx='0' cy='0' r='" ++ dist r0 ++ "' fill='red' />\n"
      ++ "<circle cx='0' cy='" ++ coord y0 ++ "' r='" ++ dist r0 ++ "' fill='red' />\n"
topLoop r = "<circle cx='0' cy='" ++ coord (r + y0) ++ "' r='" ++ dist r ++ "' />\n"
bottomLoop r = "<circle cx='0' cy='" ++ coord (-r) ++ "' r='" ++ dist r ++ "' />\n"
betweenCurve x = "<path d='M0,0 Q" ++ coord (2 * x) ++ "," ++ coord (y0/2) ++ " 0," ++ coord y0 ++ "' />\n"

main = putStrLn $
  "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" ++
  "<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n" ++
  "<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 " ++ dist (fx * 16) ++ " " ++ dist (fy * 16) ++ "\">\n" ++
  "<g fill='none' stroke='black' stroke-width='1' transform='translate(5120,0) rotate(-45) translate(-5120,-2560)'>\n" ++
  concat (zipWith graph [(u, v) | v <- [1..6], u <- [1..12]] (graphs 15)) ++
  "</g></svg>"
