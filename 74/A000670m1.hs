{-
A000670 Fubini numbers; number of weak orders on n labeled elements; ...
a(n) = 1, 1, 3, 13, 75, 541, 4683, ...
a(4) - 1 = 74
(ignore the empty weak order)
-}

import Data.List (nub, permutations, sort)
import Data.Maybe (fromJust)
import qualified Data.Set as S
import Control.Monad (filterM)

type Relation = [(Int, Int)]

n = 4
u = [ (x, y) | x <- [1 .. n], y <- [1 .. n] ]

implies True False = False
implies _ _ = True

irreflexive r = and [ notElem (x, x) r | x <- [1 .. n] ]

asymmetric r = and [ elem (x, y) r `implies` notElem (y, x) r | (x, y) <- u ]

transitive r = and [ (elem (x, y) r && elem (y, z) r) `implies` elem (x, z) r | x <- [1 .. n], y <- [1 .. n] , z <- [1 .. n] ]

incomparable r = [ (x, y) | (x, y) <- u, notElem (x, y) r, notElem (y, x) r ]

isWO r = irreflexive r && asymmetric r && transitive r && transitive (incomparable r)

nonEmptyWOs = tail . sort . filter isWO . filterM (const [False, True]) $ u

nodes = [ (0, 0), (0, 1), (1, 1), (1, 0) ]

offsets = [ (s * x + 0.5, s * y + 0.5) | y <- [0 .. 10], x <- [0 .. 6] ]

draw (x0, y0) r = "<g transform='translate(" ++ show x0 ++ "," ++ show y0 ++ ")'>" ++ concatMap arc r ++ concatMap arrow r ++ concatMap circle nodes ++ "</g>\n"

arc (m, n) =
  "<path d='M " ++ show x0 ++ " " ++ show y0 ++ " L " ++ show x1 ++ " " ++ show y1 ++ "' stroke-width='0.15' stroke='white' />" ++ 
  "<path d='M " ++ show x0 ++ " " ++ show y0 ++ " L " ++ show x1 ++ " " ++ show y1 ++ "' stroke-width='0.05' stroke='black' />"
  where
    (x0, y0) = nodes !! (m - 1)
    (x1, y1) = nodes !! (n - 1)

arrow (m, n) = 
  "<path d='M " ++ show ox ++ " " ++ show oy ++ " L " ++ show ax ++ " " ++ show ay ++ 
     " L " ++ show bx ++ " " ++ show by ++ " L " ++ show ox ++ " " ++ show oy ++ " Z' stroke='none' fill='black' />"
  where
    (x0, y0) = nodes !! (m - 1)
    (x1, y1) = nodes !! (n - 1)
    dx = x1 - x0
    dy = y1 - y0
    d = sqrt (dx * dx + dy * dy)
    t | d > 1.1 = (sqrt 2 - 0.15) / sqrt 2
      | otherwise = 1 - 0.15
    ox = mix x0 x1 t
    oy = mix y0 y1 t
    ax = ox - dx / d / 4 - dy / d / 8
    ay = oy - dy / d / 4 + dx / d / 8
    bx = ox - dx / d / 4 + dy / d / 8
    by = oy - dy / d / 4 - dx / d / 8

mix a b x = a + (b - a) * x

circle (x, y) = "<circle cx='" ++ show x ++ "' cy='" ++ show y ++"' r='0.15' fill='red' stroke-width='0.05' />"

s = 1.75
w = s * 6 + 1 + 1
h = s * 10 + 1 + 1

main' =
  "<svg width='150' height='210' xmlns='http://www.w3.org/2000/svg'>\n" ++
  "<rect x='0' y='0' width='150' height='210' fill='white' stroke='none' />" ++
  "<g transform='translate(12.5, 7.5) scale(10)' stroke='black' stroke-width='0.1' fill='none'>\n" ++
  concat (zipWith draw offsets nonEmptyWOs) ++
  "</g>\n" ++
  "</svg>\n"

main = putStr main'
