#include <stdio.h>

int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  printf
    ( "<svg xmlns='http://www.w3.org/2000/svg' width='600' height='600'>\n"
      "<rect width='100%%' height='100%%' fill='white' stroke='none' />\n"
      "<g fill='none' stroke='black' stroke-width='1'>\n"
    );
  int y = 35;
  for (int j = 1; j <= 9; ++j)
  {
    int x = 35;
    for (int i = 1; i <= 9; ++i)
    {
      printf
        ( "<rect x='%d' y='%d' width='%d' height='%d' />\n"
        , x, y, i * 10, j * 10
        );
      x += (i + 1) * 10;
    }
    y += (j + 1) * 10;
  }
  printf
    ( "</g>\n"
      "</svg>\n"
    );
  return 0;
}
