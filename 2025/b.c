#include <math.h>
#include <stdio.h>

int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  printf
    ( "<svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' width='600' height='600'>\n"
      "<defs>\n"
      "<g id='t' fill='none' stroke='black' stroke-width='1' >\n"
    );
  for (int j = 1; j <= 9; ++j)
  {
    double y = 3 * (j - 1) * sqrt(3);
    for (int i = 1; i <= j; ++i)
    {
      int x = - 3 * (j - 1) + 6 * (i - 1);
      printf
        ( "<circle cx='%d' cy='%.4f' r='2' />\n"
        , x, y
        );
    }
  }
  printf
    ( "</g>\n"
      "</defs>\n"
      "<rect width='100%%' height='100%%' fill='white' stroke='none' />\n"
    );
  for (int j = 1; j <= 9; ++j)
  {
    double y = 60 + 30 * (j - 1) * sqrt(3);
    for (int i = 1; i <= j; ++i)
    {
      int x = 300 - 30 * (j - 1) + 60 * (i - 1);
      printf
        ( "<use xlink:href='#t' x='%d' y='%.4f' />\n"
        , x, y
        );
    }
  }
  printf
    ( "</svg>\n"
    );
  return 0;
}
