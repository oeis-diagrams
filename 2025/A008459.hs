{-

A008459

Number of lattice paths from (0, 0) to (n, n)
with steps (1, 0) and (0, 1),
having k right turns.

T(10, 2) = 2025

aka 2025 letter L at bottom left corner of a 10x10 box

-}

import Control.Applicative ((<$>))
import Control.Monad (forM_)
import Data.List (nub, sort, tails)

a008459 :: Int -> Int -> [[Bool]]
a008459 n k = nub .  sort . filter ((k ==) . sum . map f . tails) . go $ (0, 0)
  where
    go (x, y) =
      (if x < n then (True :) <$> go (x + 1, y) else if y == n then [[]] else []) ++
      (if y < n then (False:) <$> go (x, y + 1) else if x == n then [[]] else [])
    f (False:True:_) = 1
    f _ = 0

svg :: IO ()
svg = do
  putStrLn "<svg xmlns='http://www.w3.org/2000/svg' width='720' height='720'>"
  putStrLn "<rect width='100%' height='100%' fill='white' stroke='none' />"
  putStrLn "<g fill='black' stroke='none'>"
  forM_ (a008459 10 2 `zip` [(x, y) | y <- [44,43..0], x <- [0..44]]) $ \(l, (x, y)) ->
    putStrLn ("<path d='M " ++ show (25 + 15 * x + 10) ++ "," ++ show (25 + 15 * y + 10) ++ " l" ++ concatMap (\x -> if x then " -1,0" else " 0,-1") l ++ " 0,10 z' />")
  putStrLn "</g>"
  putStrLn "</svg>"

{-
main :: IO ()
main = forM_ (a008459 10 2) $ putStrLn . map (\x -> if x then '-' else '|')
-}

main :: IO ()
main = svg
