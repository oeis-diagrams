#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#ifndef M_PI
#define M_PI 3.14159265358979
#endif

#define N 9

float image[540][540][3];
uint8_t ppm[540][540][3];

int z = 0;

struct board
{
  uint8_t occupied[N][N];
  uint16_t attacked[N][N];
};

struct board board;

void output_image(void)
{
  for (int j = 0; j < 540; ++j)
  for (int i = 0; i < 540; ++i)
  for (int c = 0; c < 3; ++c)
    ppm[j][i][c] = 255 * sqrtf(image[j][i][c]);
  printf("P6\n540 540\n255\n");
  fwrite(ppm, sizeof(ppm), 1, stdout);
}

void draw_board(void)
{
  float palette[N][3];
  for (int j = 0; j < N; ++j)
  for (int i = 0; i < N; ++i)
  {
    if (board.occupied[j][i])
    {
      float t = 0.3 - j * 2 * M_PI / N;
      float s =       i * 2 * M_PI / N;
      float sat = 0.25 * (1 - cosf(s));
      float val = (1 - 0.25 * (1 - sinf(s))) / (1 + sat) / N;
      palette[board.occupied[j][i] - 1][0] = val * (1 + sat * cosf(t + 0));
      palette[board.occupied[j][i] - 1][1] = val * (1 + sat * cosf(t + 2));
      palette[board.occupied[j][i] - 1][2] = val * (1 + sat * cosf(t + 4));
    }
  }
  int y = (z / 45) * 12 + 3;
  int x = (z % 45) * 12 + 3;
  ++z;
  for (int j = 0; j < N; ++j)
  for (int i = 0; i < N; ++i)
  for (int k = 1; k <= N; ++ k)
  {
    if (board.occupied[j][i] == k)
    {
      image[y + j][x + i][0] += palette[k - 1][0] * N;
      image[y + j][x + i][1] += palette[k - 1][1] * N;
      image[y + j][x + i][2] += palette[k - 1][2] * N;
    }
    if (board.attacked[j][i] & (1 << k))
    {
      image[y + j][x + i][0] += palette[k - 1][0] / 2;
      image[y + j][x + i][1] += palette[k - 1][1] / 2;
      image[y + j][x + i][2] += palette[k - 1][2] / 2;
    }
  }
}

void add_piece(int j, int i, int b)
{
  uint16_t m = 1 << b;
  board.occupied[j][i] = b;
  for (int d = 1; d < N; ++d)
  {
    board.attacked[(j +         d) % N][(i +       2*d) % N] |= m;
    board.attacked[(j +       2*d) % N][(i +         d) % N] |= m;
    board.attacked[(j +   N -   d) % N][(i +       2*d) % N] |= m;
    board.attacked[(j +       2*d) % N][(i +   N -   d) % N] |= m;
    board.attacked[(j +         d) % N][(i + 2*N - 2*d) % N] |= m;
    board.attacked[(j + 2*N - 2*d) % N][(i +         d) % N] |= m;
    board.attacked[(j +   N -   d) % N][(i + 2*N - 2*d) % N] |= m;
    board.attacked[(j + 2*N - 2*d) % N][(i +   N -   d) % N] |= m;
  }
}

void recurse(int b, int n)
{
  if (! b)
  {
    draw_board();
  }
  else
  {
    struct board undo = board;
    for (int m = n; m < N*N; ++m)
    {
      if (! board.attacked[m / N][m % N])
      {
        add_piece(m / N, m % N, b);
        recurse(b - 1, m + 1);
        board = undo;
      }
    }
    board = undo;
  }
}

int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  recurse(9, 0);
  output_image();
  return 0;
}
