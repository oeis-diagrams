#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef M_PI
#define M_PI 3.14159265358979
#endif

float anchor[11][2025][4];
float position[2025][4];
float velocity[2025][4];
#define H 1080
#define W 1920
#define r 2
#define aa 4
unsigned char ppm[H * aa][W * aa][3];

int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  memset(anchor, 0, sizeof(anchor));
  float *p = &anchor[0][0][0];
  for (int j = 1; j <= 9; ++j)
  {
    for (int jj = 1; jj <= j; ++jj)
    {
      for (int i = 1; i <= 9; ++i)
      {
        for (int ii = 1; ii <= i; ++ii)
        {
          int x =    - (j - 1) * 10 - (4 - 1) + (4 - 1) * 2 + (jj - 1) * 20;
          int y = 12 + (j - 1) * 20 + (4 - 1) * 2;
          *p++ = x             / 179.0 * 2;
          *p++ = y * sqrt(3)/2 / 179.0 * 2 - 1;
          *p++ = 1;
          *p++ = 1;
        }
      }
    }
  }
  for (int j = 1; j <= 9; ++j)
  {
    for (int jj = 1; jj <= j; ++jj)
    {
      for (int i = 1; i <= 9; ++i)
      {
        for (int ii = 1; ii <= i; ++ii)
        {
          int x =    - (j - 1) * 10 - (i - 1) + (ii - 1) * 2 + (jj - 1) * 20;
          int y = 12 + (j - 1) * 20 + (i - 1) * 2;
          *p++ = x             / 179.0 * 2;
          *p++ = y * sqrt(3)/2 / 179.0 * 2 - 1;
          *p++ = 1;
          *p++ = 1;
        }
      }
    }
  }
#if 0
  for (int j = 0; j < 45; ++j)
  {
    for (int i = 0; i < 45; ++i)
    {
      *p++ = (i / 44.0 * 2 - 1) * 0.8;
      *p++ = (j / 44.0 * 2 - 1) * 0.8;
      *p++ = 0;
      *p++ = 1;
    }
  }
#endif
  for (int j = 1; j <= 9; ++j)
  {
    for (int i = 1; i <= 9; ++i)
    {
      for (int jj = 1; jj <= j; ++jj)
      {
        for (int ii = 1; ii <= i; ++ii)
        {
          int x = (i - 1) + (ii - 1) + i * (i - 1) / 2;
          int y = (j - 1) + (jj - 1) + j * (j - 1) / 2;
          *p++ = (x / 52.0 * 2 - 1) * 0.8;
          *p++ = (y / 52.0 * 2 - 1) * 0.8;
          *p++ = 1;
          *p++ = 1;
        }
      }
    }
  }

#define PERMUTED(inner) \
  for (int k = 1; k <= 9; ++k) \
  { \
    for (int l = 1; l <= k; ++l) \
    { \
      for (int i = 1; i <= k; ++i) \
      { \
        for (int ii = 1; ii <= i; ++ii) \
        { \
          int j = k; \
          int jj = l; \
          inner \
        } \
      } \
      for (int j = k - 1; j >= 1; --j) \
      { \
        for (int jj = 1; jj <= j; ++jj) \
        { \
          int i = k; \
          int ii = l; \
          inner \
        } \
      } \
    } \
  } \

#define inner \
  int x = (i - 1) + (ii - 1) + i * (i - 1) / 2; \
  int y = (j - 1) + (jj - 1) + j * (j - 1) / 2; \
  *p++ = (x / 52.0 * 2 - 1) * 0.8; \
  *p++ = (y / 52.0 * 2 - 1) * 0.8; \
  *p++ = 1; \
  *p++ = 1;
  PERMUTED(inner)
#undef inner

  for (int k = 0; k < 2025; ++k)
  {
    position[k][0] = 0*anchor[0][k][0] + 0.01 * (rand() / (double) RAND_MAX * 2 - 1);
    position[k][1] = 0*anchor[0][k][1] + 0.01 * (rand() / (double) RAND_MAX * 2 - 1);
    position[k][2] = 0*anchor[0][k][2] + 0.01 * (rand() / (double) RAND_MAX * 2 - 1);
  }
  const float dt = 1.0/60.0;
  for (int frame = 0; frame < 960; ++frame)
  {
    memset(ppm, 0, sizeof(ppm));
    float t = frame / (10.0 * 60.0) * 4;
    t *= 2;
    if (t > 4) t = (t - 4) / 2 + 4;
    if (t > 6) t = (t - 6) * 3 + 6;
    if (t > 8) t = (t - 8) / 3 * 2 + 8;
    int w = floor(t);
    if (w > 10) w = 10;;
    for (int k = 0; k < 2025; ++k)
    {
      switch (w)
      {
        case 0:
        case 1:
        case 2:
        case 3:
          break;

        case 4:
          {
            float s = 2 * M_PI * (t - 4) / 4;
            float co = cosf(s);
            float si = sinf(s);
            float *p = &anchor[w][0][0];
#define inner \
            float x = (i - 1) + (ii - 1) + i * (i - 1) / 2; \
            float y = (j - 1) + (jj - 1) + j * (j - 1) / 2; \
            if (i > j) \
            { \
              float x0 = (i - 1) + (0.5 * (i + 1) - 1) + i * (i - 1) / 2; \
              float y0 = x0; \
              x -= x0; \
              y -= y0; \
              float x1 = co * x - si * y; \
              float y1 = si * x + co * y; \
              x = x1 + x0; \
              y = y1 + y0; \
            } \
            *p++ = (x / 52.0 * 2 - 1) * 0.8; \
            *p++ = (y / 52.0 * 2 - 1) * 0.8; \
            *p++ = 1; \
            *p++ = 1;
            PERMUTED(inner)
#undef inner
            break;
          }

        case 5:
          {
            float s = 2 * M_PI * (t - 5) / 4;
            float co = cosf(s);
            float si = sinf(s);
            float *p = &anchor[w][0][0];
#define inner \
            float x = (i - 1) + (ii - 1) + i * (i - 1) / 2; \
            float y = (j - 1) + (jj - 1) + j * (j - 1) / 2; \
            float z = 0; \
            if (i > j) \
            { \
              float x0 = (i - 1) + (0.5 * (i + 1) - 1) + i * (i - 1) / 2; \
              float y0 = x0; \
              x -= x0; \
              y -= y0; \
              float x1 = - y; \
              float y1 = x; \
              x = x1 + x0; \
              y = y1 + y0; \
              z = si * (y - y0); \
              y = co * (y - y0) + y0; \
            } \
            else \
            { \
              float x0 = (j - 1) + (0.5 * (j + 1) - 1) + j * (j - 1) / 2; \
              float y0 = x0; \
              z = si * (y - y0); \
              y = co * (y - y0) + y0; \
            } \
            *p++ = (x / 52.0 * 2 - 1) * 0.8; \
            *p++ = (y / 52.0 * 2 - 1) * 0.8; \
            *p++ = (z / 52.0 * 2    ) * 0.8 + 1; \
            *p++ = 1;
            PERMUTED(inner)
#undef inner
            break;
          }

        case 6:
          {
            float *p = &anchor[w][0][0];
#define inner \
            float x = (i - 1) + (ii - 1) + i * (i - 1) / 2; \
            float y = (j - 1) + (jj - 1) + j * (j - 1) / 2; \
            float z = 0; \
            if (i > j) \
            { \
              float x0 = (i - 1) + (0.5 * (i + 1) - 1) + i * (i - 1) / 2; \
              float y0 = x0; \
              x -= x0; \
              y -= y0; \
              float x1 = - y; \
              float y1 = x; \
              x = x1 + x0; \
              y = y1 + y0; \
              z = y - y0; \
              y = y0 + (j - i); \
            } \
            else \
            { \
              float x0 = (j - 1) + (0.5 * (j + 1) - 1) + j * (j - 1) / 2; \
              float y0 = x0; \
              z = y - y0; \
              y = y0 + (j - i); \
            } \
            *p++ = (x / 52.0 * 2 - 1) * 0.8; \
            *p++ = (y / 52.0 * 2 - 1) * 0.8; \
            *p++ = (z / 52.0 * 2    ) * 0.8 + 1; \
            *p++ = 1;
            PERMUTED(inner)
#undef inner
            break;
          }

        case 7:
          {
            float *p = &anchor[w][0][0];
#define inner \
            float x = (i - 1) + (ii - 1) + i * (i - 1) / 2; \
            float y = (j - 1) + (jj - 1) + j * (j - 1) / 2; \
            float z = 0; \
            if  (i > j) \
            { \
              float x0 = (i - 1) + (0.5 * (i + 1) - 1) + i * (i - 1) / 2; \
              float y0 = x0; \
              x -= x0; \
              y -= y0; \
              float y1 = x; \
              x = x0 - jj + (0.5 * (i + 1)); \
              y = y1 + y0; \
              z = y - y0; \
              y = y0 + (j - i); \
            } \
            else \
            { \
              float x0 = (j - 1) + (0.5 * (j + 1) - 1) + j * (j - 1) / 2; \
              float y0 = x0; \
              z = y - y0; \
              y = y0 + (j - i); \
              x = x0 + ii - (0.5 * (j + 1)); \
            } \
            *p++ = (x / 52.0 * 2 - 1) * 0.8; \
            *p++ = (y / 52.0 * 2 - 1) * 0.8; \
            *p++ = (z / 52.0 * 2    ) * 0.8 + 1; \
            *p++ = 1;
            PERMUTED(inner)
#undef inner
            break;
          }

        case 8:
          {
            float *p = &anchor[w][0][0];
#define inner \
            float x = (i - 1) + (ii - 1) + i * (i - 1) / 2; \
            float y = (j - 1) + (jj - 1) + j * (j - 1) / 2; \
            float z = 0; \
            if  (i > j) \
            { \
              float x0 = (i - 1) + (0.5 * (i + 1) - 1) + i * (i - 1) / 2; \
              float y0 = x0; \
              x -= x0; \
              y -= y0; \
              float y1 = x; \
              x = x0 - jj + (0.5 * (i + 1)); \
              y = y1 + y0; \
              z = y - y0; \
              y = y0 + (j - i) + (i - 1 - 2 * (jj - 1)) * 0.5; \
            } \
            else \
            { \
              float x0 = (j - 1) + (0.5 * (j + 1) - 1) + j * (j - 1) / 2; \
              float y0 = x0; \
              z = y - y0; \
              y = y0 + (j - i) - (j - 1 - 2 * (ii - 1)) * 0.5; \
              x = x0 + ii - (0.5 * (j + 1)); \
            } \
            *p++ = (x / 52.0 * 2 - 1) * 0.8; \
            *p++ = (y / 52.0 * 2 - 1) * 0.8; \
            *p++ = (z / 52.0 * 2    ) * 0.8 + 1; \
            *p++ = 1;
            PERMUTED(inner)
#undef inner
            break;
          }

        case 9:
          {
            float *p = &anchor[w][0][0];
#define inner \
            float x00 = (((k - 1) + k * (k - 1) / 2) - 52.0 / 2.0) * 1.5 + 52.0 / 2.0; \
            float y00 = 52.0 / 2.0; \
            float x = (i - 1) + (ii - 1) + i * (i - 1) / 2; \
            float y = (j - 1) + (jj - 1) + j * (j - 1) / 2; \
            float z = 0; \
            if  (i > j) \
            { \
              float x0 = (i - 1) + (0.5 * (i + 1) - 1) + i * (i - 1) / 2; \
              float y0 = x0; \
              x00 -= x0; \
              y00 -= y0; \
              x -= x0; \
              y -= y0; \
              float y1 = x; \
              x = x0 - jj + (0.5 * (i + 1)); \
              y = y1 + y0; \
              z = y - y0; \
              y = y0 + (j - i) + (i - 1 - 2 * (jj - 1)) * 0.5; \
            } \
            else \
            { \
              float x0 = (j - 1) + (0.5 * (j + 1) - 1) + j * (j - 1) / 2; \
              float y0 = x0; \
              x00 -= x0; \
              y00 -= y0; \
              z = y - y0; \
              y = y0 + (j - i) - (j - 1 - 2 * (ii - 1)) * 0.5; \
              x = x0 + ii - (0.5 * (j + 1)); \
            } \
            x += x00; \
            y += y00; \
            *p++ = (x / 52.0 * 2 - 1) * 0.8; \
            *p++ = (y / 52.0 * 2 - 1) * 0.8; \
            *p++ = (z / 52.0 * 2    ) * 0.8 + 1; \
            *p++ = 1;
            PERMUTED(inner)
#undef inner
            break;
          }

        case 10:
          {
            float *p = &anchor[w][0][0];
            for (int k = 0; k < 2025; ++k)
            {
              *p++ = 0;
              *p++ = 0;
              *p++ = 1;
              *p++ = 1;
            }
            break;
          }

        default:
          return 0;
      }

      float cx = t < 4 ? 0 : t < 5 ? t - 4 : t < 7 ? 1 : 0;
      float cy = t < 6 ? 0 : t < 9 ? 1 : 0;
      float t = 0.3 - 2 * M_PI * k / 2025;
      unsigned char rgb[3] = { 128 + 127 * cosf(t), 128 + 127 * cosf(t + 2), 128 + 127 * cosf(t + 4) };
      float dx = position[k][0] - (anchor[w][k][0] - 0.6 * (1 - cos(M_PI * cx)) / 2);
      float dy = position[k][1] - (anchor[w][k][1] - 0.1 * (1 - cos(M_PI * cy)) / 2);
      float dz = position[k][2] - anchor[w][k][2];
      float d2 = dx * dx + dy * dy + dz * dz;
      float d = sqrtf(d2); if (! (d > 0)) d = 1;
      float F = 100 * d;
      float f = 0.8;
      velocity[k][0] *= f;
      velocity[k][1] *= f;
      velocity[k][2] *= f;
      velocity[k][0] -= F * dx / d * dt;
      velocity[k][1] -= F * dy / d * dt;
      velocity[k][2] -= F * dz / d * dt;
      position[k][0] += velocity[k][0] * dt;
      position[k][1] += velocity[k][1] * dt;
      position[k][2] += velocity[k][2] * dt;

      int x0 = (position[k][0] * (2 - position[k][2]) * 0.9 / 2 * H + W / 2) * aa;
      int y0 = (position[k][1] * (2 - position[k][2]) * 0.9 / 2 * H + H / 2) * aa;
      float r0 = r * (2 - position[k][2]);
      for (int j = -floor(r0 * aa); j <= ceil(r0 * aa); ++j)
      {
        int y = y0 + j;
        for (int i = -floor(r0 * aa); i <= ceil(r0 * aa); ++i)
        {
          int x = x0 + i;
          if (0 <= x && x < W * aa && 0 <= y && y < H * aa && i * i + j * j <= r0 * aa * r0 * aa)
          {
            ppm[y][x][0] = rgb[0];
            ppm[y][x][1] = rgb[1];
            ppm[y][x][2] = rgb[2];
          }
        }
      }
    }
    printf("P6\n%d %d\n255\n", W * aa, H * aa);
    fwrite(ppm, sizeof(ppm), 1, stdout);
  }
  return 0;
}
