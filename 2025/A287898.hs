{-

A287898 a(8)
- Number of ways to go up and down n stairs, with fewer than 4 stairs at a
time, stepping on each stair at least once.
- Also the number of words using 0, 1 and 2 which have n-1 length and don’t
appear 0000 or 1111.

Horizontally: (9 + 8) * 7 + 65 = 135 = 127 + 8
Vertically: 9 * 127 + 7 * 126 = 2025
2025 = 121 * 9 + 117 * 8

x
x y z
: : :
x y z
    z

-}

import Control.Monad (replicateM)
import Data.List (replicate, tails, transpose)

a287898 :: Int -> [[Int]]
a287898 n = filter (all f . tails) (replicateM (n - 1) [0,1,2])
  where
     f (0:0:0:0:_) = False
     f (2:2:2:2:_) = False
     f _ = True

offsets :: [Int]
offsets = map (4 *) [0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8]

lengths :: [Int]
lengths = [121,117,121,117, 121,117,121,117, 121,117,121,117, 121,117,121,117, 121]

splits :: [Int] -> [a] -> [[a]]
splits [] [] = []
splits (l:ls) xs = case splitAt l xs of (ys, zs) -> ys : splits ls zs

layout :: [[Int]] -> [[[Int]]]
layout = transpose . zipWith3 (\o p ys -> replicate o pad ++ ys ++ replicate p pad) offsets (reverse offsets) . splits lengths
  where pad = replicate 7 (-1)

draw :: [[[Int]]] -> String
draw = unlines . transpose . map (unwords . map (map c))
  where
    c 0 = '×'
    c 1 = '•'
    c 2 = '+'
    c _ = ' '

main :: IO ()
main = putStr . draw . layout . a287898 $ 8
