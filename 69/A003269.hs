-- oeis-diagrams -- unofficial diagrams of OEIS sequences
-- Copyright (C) 2016-2017 Claude Heiland-Allen
-- License CC-BY-NC <https://creativecommons.org/licenses/by-nc/3.0/>

-- https://oeis.org/A003269
-- a(n+1) is the number of compositions of n into parts 1 and 4. - Joerg Arndt,
-- Jun 25 2011

{-# LANGUAGE TypeFamilies #-}

import Diagrams.Prelude hiding (parts)
import Diagrams.Backend.SVG.CmdLine (B, defaultMain)

import Data.List (sortBy)
import Data.Monoid ((<>))
import Data.Ord (comparing)

parts = [1, 4]

compositions' total
  | total < 0 = []
  | total == 0 = [[]]
  | otherwise = [ part : rest | part <- parts, rest <- compositions (total - part) ]

compositions total = filter ((total ==) . sum) (compositions' total)

diagram1 :: [Int] -> Diagram B
diagram1 is = mconcat [strokeLocTrail $ circle (0.125) `at` v | v <- vs] # lw thin # fc black `atop` head vs ~~ last vs `atop` strutY (1/3)
  where
    vs = map (\x -> p2 (fromIntegral x, 0)) (scanl (+) 0 is)

diagram :: Int -> Diagram B
diagram = bg white . frame 2 . centerXY . vcat . map diagram1 . compositions

main = defaultMain (diagram 15)
