-- oeis-diagrams -- unofficial diagrams of OEIS sequences
-- Copyright (C) 2016-2017 Claude Heiland-Allen
-- License CC-BY-NC <https://creativecommons.org/licenses/by-nc/3.0/>

-- https://oeis.org/A000332
-- The number of equilateral triangles with vertices in an equilateral
-- triangular array of points with n rows (offset 1), with any orientation.
-- - Ignacio Larrosa Cañestro, Apr 09 2002.

{-# LANGUAGE FlexibleContexts #-}
import Diagrams.Prelude
import Diagrams.Backend.SVG.CmdLine (B, defaultMain)

import Data.List (sort, sortOn, nub, transpose)
import Data.List.Split (chunksOf)

third :: (Int, Int) -> (Int, Int) -> (Int, Int)
third (p, q) (p', q') =
  let (s, t) = (p' - p, q' - q)
  in  (p - t, q + s + t)

inTriangle :: Int -> (Int, Int) -> Bool
inTriangle n (p, q) = 0 <= p && 0 <= q && p + q < n

sizeSquared :: [(Int, Int)] -> Int
sizeSquared [(p, q), (p', q'), _] =
  let (s, t) = (p' - p, q' - q)
  in  s * s + s * t + t * t

triangles :: Int -> [[(Int, Int)]]
triangles n = sortOn sizeSquared $
  nub
  [ sort [(a, b), (c, d), (e, f)]
  | a <- [0..n]
  , b <- [0..n]
  , inTriangle n (a, b)
  , c <- [0..n]
  , d <- [0..n]
  , inTriangle n (c, d)
  , (a, b) /= (c, d)
  , (e, f) <- [ third (a, b) (c, d)
              , third (c, d) (a, b)
              ]
  , inTriangle n (e, f)
  ]

t2 :: (Int, Int) -> V2 Double
t2 (p, q)
  = V2
    (fromIntegral p + fromIntegral q / 2)
    (sqrt 3 * fromIntegral q / 2)

t2' = P . t2

draw n t@[ab,cd,ef]
  = frame 0.75
  . scale 1.25
  . rotate (15 @@ deg)
  $ mconcat
  [ circle 0.25
  # fc (if (p, q) `elem` t then grey else white)
  # translate (t2 (p, q))
  # lw thin
  | p <- [0..n]
  , q <- [0..n]
  , inTriangle n (p, q)
  ] `atop` mconcat
  [ t2' ab ~~ t2' cd
  , t2' cd ~~ t2' ef
  , t2' ef ~~ t2' ab
  ]

grid = vcat . map hcat

diagram n m
  = bg white
  . grid
  . chunksOf m
  . map (draw n)
  $ triangles n

main = defaultMain (diagram 6 7)
