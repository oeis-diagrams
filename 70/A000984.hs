-- oeis-diagrams -- unofficial diagrams of OEIS sequences
-- Copyright (C) 2016-2017 Claude Heiland-Allen
-- License CC-BY-NC <https://creativecommons.org/licenses/by-nc/3.0/>

-- https://oeis.org/A000984
-- The number of direct routes from my home to Granny's when Granny lives n
-- blocks south and n blocks east of my home in Grid City. To obtain a direct
-- route, from the 2n blocks, choose n blocks on which one travels south. For
-- example, a(2)=6 because there are 6 direct routes: SSEE, SESE, SEES, EESS,
-- ESES and ESSE. - Dennis P. Walsh, Oct 27 2006

{-# LANGUAGE FlexibleContexts #-}
import Diagrams.Prelude
import Diagrams.Backend.SVG.CmdLine (B, defaultMain)

import Control.Monad (replicateM)
import Data.List.Split (chunksOf)

u, d, z :: (Int, Int)
u = (1, 0)
d = (0, 1)
z = (0, 0)

add (a, b) (c, d) = (a + c, b + d)

v :: (Int, Int) -> V2 Double
v (a, b) = V2 (fromIntegral a) (fromIntegral b)

vs = map v . scanl add z
l = foldr add z

paths n =
  [ q
  | q <- replicateM (2 * n) [u,d]
  , l q == (n, n)
  ]

draw n q
  = frame 0.5
  . (`atop` centerXY (strutY (fromIntegral n)))
  . centerXY
  $ mconcat
  [ circle 0.25
  # fc white
  # translate pq
  # lw thin
  | pq <- vs q
  ] `atop` strokeT (trailFromOffsets (map v q))

grid = vcat . map hcat

diagram n m
  = bg white
  . centerXY
  . grid
  . chunksOf m
  . map (draw n)
  $ paths n

main = defaultMain (diagram 4 7)
