-- oeis-diagrams -- unofficial diagrams of OEIS sequences
-- Copyright (C) 2016-2017 Claude Heiland-Allen
-- License CC-BY-NC <https://creativecommons.org/licenses/by-nc/3.0/>

-- https://oeis.org/A002623
-- Number of nondegenerate triangles that can be made from rods of length
-- 1,2,3,4,...,n. - Alfred Bruckstein

{-# LANGUAGE FlexibleContexts #-}
import Diagrams.Prelude
import Diagrams.Backend.SVG.CmdLine (B, defaultMain)

import Data.List (sort, sortOn, nub, transpose)
import Data.List.Split (chunksOf)

nondegenerate :: [Int] -> Bool
nondegenerate [a,b,c] = a + b > c

corners :: [Int] -> [V2 Double]
corners [a',b',c']
  = [V2 0 0, V2 c 0, V2 x y]
  where
    a = fromIntegral a'
    b = fromIntegral b'
    c = fromIntegral c'
    x = (c^2 - a^2 + b^2) / (2 * c)
    y = sqrt $ b^2 - x^2

sizeSquared :: [Int] -> Double
sizeSquared [a',b',c']
  = s * (s - a) * (s - b) * (s - c)
  where
    a = fromIntegral a'
    b = fromIntegral b'
    c = fromIntegral c'
    s = (a + b + c) / 2

triangles :: Int -> [([Int], [V2 Double])]
triangles n
  = map (\t -> (t, corners t))
  . sortOn sizeSquared $
  [ abc
  | a <- [1..n]
  , b <- [a..n]
  , c <- [b..n]
  , let abc = [a,b,c]
  , nondegenerate abc
  ]

edge k a b
  = mconcat
  [ circle 0.25
  # fc white
  # translate p
  # lw thin
  | p <- [ lerp t a b
         | i <- [0..k]
         , let t = fromIntegral i / fromIntegral k
         ]
  ] `atop`
  (P a ~~ P b)

draw n ([a,b,c], t@[ab,cd,ef])
  = frame 0.5
  . (`atop` centerXY (strut (fromIntegral n)))
  . centerXY
  . rotate (15 @@ deg)
  $ mconcat
  [ 
  ] `atop` mconcat
  [ edge c ab cd
  , edge a cd ef
  , edge b ef ab
  ]

grid = vcat . map hcat

diagram n m
  = bg white
  . grid
  . chunksOf m
  . map (draw n)
  $ triangles n

main = defaultMain (diagram 8 7)
