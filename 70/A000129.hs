-- oeis-diagrams -- unofficial diagrams of OEIS sequences
-- Copyright (C) 2016-2017 Claude Heiland-Allen
-- License CC-BY-NC <https://creativecommons.org/licenses/by-nc/3.0/>

-- https://oeis.org/A000129
-- a(n) is the number of compositions (ordered partitions) of n-1 into two sorts
-- of 1's and one sort of 2's.  Example: the a(3)=5 compositions of 3-1=2 are
-- 1+1, 1+1', 1'+1, 1'+1', and 2. - Bob Selcoe, Jun 21 2013

{-# LANGUAGE FlexibleContexts #-}
import Diagrams.Prelude
import Diagrams.Backend.SVG.CmdLine (B, defaultMain)

import Control.Monad (replicateM)
import Data.List (sort, transpose)
import Data.List.Split (chunksOf)

u, d, h, z :: (Int, Int)
u = (1, 1)
d = (1, -1)
h = (2, 0)
z = (0, 0)

add (a, b) (c, d) = (a + c, b + d)

v :: (Int, Int) -> V2 Double
v (a, b) = V2 (fromIntegral a) (fromIntegral b)

vs = map v . scanl add z
l = fst . foldr add z

paths n =
  [ q
  | m <- [0..n]
  , q <- replicateM m [u,d,h]
  , l q == n
  ]

draw n q
  = frame 0.5
  . (`atop` centerXY (strutY (fromIntegral n)))
  . centerXY
  $ mconcat
  [ circle 0.25
  # fc white
  # translate pq
  # lw thin
  | pq <- vs q
  ] `atop` strokeT (trailFromOffsets (map v q))

grid = vcat . map hcat

diagram n m
  = bg white
  . centerXY
  . grid
  . transpose
  . chunksOf m
  . map (draw n)
  . sort
  $ paths n

main = defaultMain (diagram 5 10)
