-- oeis-diagrams -- unofficial diagrams of OEIS sequences
-- Copyright (C) 2016-2017 Claude Heiland-Allen
-- License CC-BY-NC <https://creativecommons.org/licenses/by-nc/3.0/>

-- https://oeis.org/A001405
-- Number of meanders (walks starting at the origin and ending at any altitude
-- >= 0 that may touch but never go below the x-axis) with n steps from {-1,1}.
-- - David Nguyen, Dec 20 2016

{-# LANGUAGE FlexibleContexts #-}
import Diagrams.Prelude
import Diagrams.Backend.SVG.CmdLine (B, defaultMain)

import Control.Monad (replicateM)
import Data.List (sort, transpose)
import Data.List.Split (chunksOf)

u, d, z :: (Int, Int)
u = (1, 1)
d = (1, -1)
z = (0, 0)

add (a, b) (c, d) = (a + c, b + d)

boundedBelow = not . any ((< 0) . snd) . scanl add z

paths n =
  [ q
  | q <- replicateM n [u,d]
  , boundedBelow q
  ]

v :: (Int, Int) -> V2 Double
v (a, b) = V2 (fromIntegral a) (fromIntegral b)

vs = map v . scanl add z

draw n q
  = frame 0.5
  . (`atop` centerXY (strutY (fromIntegral n)))
  . centerXY
  $ mconcat
  [ circle 0.25
  # fc white
  # translate pq
  # lw thin
  | pq <- vs q
  ] `atop` strokeT (trailFromOffsets (map v q))

grid = vcat . map hcat

diagram n m
  = bg white
  . centerXY
  . grid
  . transpose
  . chunksOf m
  . map (draw n)
  . sort
  $ paths n

main = defaultMain (diagram 8 10)
